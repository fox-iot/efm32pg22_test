#include "timer.h"
#include "cortex.h"
#include "efm32pg22.h"
#include "mask.h"
#include "platform.h"

static volatile uint32_t timer_sec;
static volatile uint16_t timer_msec;
static volatile uint32_t timer_usec;
static volatile timer_t *timer_conf;


void timer_init(timer_t *conf)
{
	cortex_interrupt_disable(TIMER0_INT);

	timer_conf = conf;

	timer_sec = 0;
	timer_msec = 0;

	CMU->CLKEN0 |= CMU_CLKEN0_TIMER0;

	TIMER0->EN = 0;
	TIMER0->CFG = mask32(TIMER_CFG_PRESC, 7) | mask32(TIMER_CFG_CLKSEL, 0) | mask32(TIMER_CFG_MODE, 0);
	TIMER0->EN = TIMER_EN_EN;
	TIMER0->TOP = (PLATFORM_PCLK / 80000) - 1;
	TIMER0->IEN = TIMER_IEN_OF;
	TIMER0->CMD = TIMER_CMD_START;

	cortex_interrupt_set_priority(TIMER0_INT, 0);
	cortex_interrupt_clear(TIMER0_INT);
	cortex_interrupt_enable(TIMER0_INT);
}

uint32_t timer_get(uint16_t *msec)
{
	uint32_t sec;
	cortex_interrupt_disable(TIMER0_INT);
	sec = timer_sec;
	*msec = timer_msec;
	cortex_interrupt_enable(TIMER0_INT);
	return sec;
}

CORTEX_ISR(TIMER0_INT)
{
	timer_usec += 100;
	if (timer_conf->timer_100us_cb) {
		timer_conf->timer_100us_cb();
	}

	if (timer_usec == 1000) {
		timer_usec = 0;
		timer_msec++;
		if (timer_msec == 1000) {
			timer_sec++;
			timer_msec = 0;
		}
	}
	TIMER0_CLR->IF = TIMER_IF_OF;
	cortex_interrupt_clear(TIMER0_INT);
}

