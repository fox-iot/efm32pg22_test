
#ifndef _EFM32PG22_LFRCO_H_
#define _EFM32PG22_LFRCO_H_

#include <stdint.h>

typedef struct{
	volatile uint32_t IPVERSION;
	volatile uint32_t CTRL;
	volatile uint32_t STATUS;
	uint32_t RESERVED1[2];
	volatile uint32_t IF;
	volatile uint32_t IEN;
	uint32_t RESERVED2[1];
	volatile uint32_t LOCK;
	volatile uint32_t CFG;
	uint32_t RESERVED3[1];
	volatile uint32_t NOMCAL;
	volatile uint32_t NOMCALINV;
	volatile uint32_t CMD;
}__attribute__((packed)) LFRCO_TypeDef;

#define LFRCO                          ((LFRCO_TypeDef *)0x40024000)
#define LFRCO_SET                      ((LFRCO_TypeDef *)0x40025000)
#define LFRCO_CLR                      ((LFRCO_TypeDef *)0x40026000)
#define LFRCO_TGL                      ((LFRCO_TypeDef *)0x40027000)

#define LFRCO_IPVERSION_IPVERSION      (((uint32_t)0xFFFFFFFF) << 0)

#define LFRCO_CTRL_FORCEEN             (((uint32_t)0x01) << 0)
#define LFRCO_CTRL_DISONDEMAND         (((uint32_t)0x01) << 1)

#define LFRCO_STATUS_RDY               (((uint32_t)0x01) << 0)
#define LFRCO_STATUS_ENS               (((uint32_t)0x01) << 16)
#define LFRCO_STATUS_LOCK              (((uint32_t)0x01) << 31)

#define LFRCO_IF_RDY                   (((uint32_t)0x01) << 0)
#define LFRCO_IF_POSEDGE               (((uint32_t)0x01) << 1)
#define LFRCO_IF_NEGEDGE               (((uint32_t)0x01) << 2)
#define LFRCO_IF_TCDONE                (((uint32_t)0x01) << 8)
#define LFRCO_IF_CALDONE               (((uint32_t)0x01) << 9)
#define LFRCO_IF_TEMPCHANGE            (((uint32_t)0x01) << 10)
#define LFRCO_IF_SCHEDERR              (((uint32_t)0x01) << 16)
#define LFRCO_IF_TCOOR                 (((uint32_t)0x01) << 17)
#define LFRCO_IF_CALOOR                (((uint32_t)0x01) << 18)

#define LFRCO_IEN_RDY                  (((uint32_t)0x01) << 0)
#define LFRCO_IEN_POSEDGE              (((uint32_t)0x01) << 1)
#define LFRCO_IEN_NEGEDGE              (((uint32_t)0x01) << 2)
#define LFRCO_IEN_TCDONE               (((uint32_t)0x01) << 8)
#define LFRCO_IEN_CALDONE              (((uint32_t)0x01) << 9)
#define LFRCO_IEN_TEMPCHANGE           (((uint32_t)0x01) << 10)
#define LFRCO_IEN_SCHEDERR             (((uint32_t)0x01) << 16)
#define LFRCO_IEN_TCOOR                (((uint32_t)0x01) << 17)
#define LFRCO_IEN_CALOOR               (((uint32_t)0x01) << 18)

#define LFRCO_LOCK_LOCKKEY             (((uint32_t)0xFFFF) << 0)

#define LFRCO_CFG_LOCKKEY              (((uint32_t)0x01) << 0)

#define LFRCO_NOMCAL_NOMCALCNT         (((uint32_t)0x1FFFFF) << 0)

#define LFRCO_NOMCALINV_NOMCALCNTINV   (((uint32_t)0x01FFFF) << 0)

#define LFRCO_CMD_REDUCETCINT          (((uint32_t)0x01) << 0)

#endif
