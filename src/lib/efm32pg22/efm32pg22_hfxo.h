#ifndef _EFM32PG22_HFXO_H_
#define _EFM32PG22_HFXO_H_

#include <stdint.h>

typedef struct{
	volatile uint32_t IPVERSION;
	uint32_t RESERVED1[3];
	volatile uint32_t XTALCFG;
	uint32_t RESERVED2[1];
	volatile uint32_t XTALCTRL;
	uint32_t RESERVED3[1];
	volatile uint32_t CFG;
	uint32_t RESERVED4[1];
	volatile uint32_t CTRL;
	uint32_t RESERVED5[9];
	volatile uint32_t CMD;
	uint32_t RESERVED6[1];
	volatile uint32_t STATUS;
	uint32_t RESERVED7[5];
	volatile uint32_t IF;
	volatile uint32_t IEN;
	uint32_t RESERVED8[2];
	volatile uint32_t LOCK;
}__attribute__((packed)) HFXO_TypeDef;

#define HFXO0                          ((HFXO_TypeDef *)0x4000C000)
#define HFXO0_SET                      ((HFXO_TypeDef *)0x4000D000)
#define HFXO0CLR                       ((HFXO_TypeDef *)0x4000E000)
#define HFXO0_TGL                      ((HFXO_TypeDef *)0x4000F000)

#define HFXO_IPVERSION_IPVERSION       (((uint32_t)0xFFFFFFFF) << 0)

#define HFXO_XTALCFG_COREBIASSTARTUPI  (((uint32_t)0x3F) << 0)
#define HFXO_XTALCFG_COREBIASSTARTUP   (((uint32_t)0x3F) << 6)
#define HFXO_XTALCFG_CTUNEXISTARTUP    (((uint32_t)0x0F) << 12)
#define HFXO_XTALCFG_CTUNEXOSTARTUP    (((uint32_t)0x0F) << 16)
#define HFXO_XTALCFG_TIMEOUTSTEADY     (((uint32_t)0x0F) << 20)
#define HFXO_XTALCFG_TIMEOUTCBLSB      (((uint32_t)0x0F) << 24)

#define HFXO_XTALCTRL_COREBIASANA      (((uint32_t)0xFF) << 0)
#define HFXO_XTALCTRL_CTUNEXIANA       (((uint32_t)0xFF) << 8)
#define HFXO_XTALCTRL_CTUNEXOANA       (((uint32_t)0xFF) << 16)
#define HFXO_XTALCTRL_CTUNEFIXANA      (((uint32_t)0x03) << 24)
#define HFXO_XTALCTRL_COREDGENANA      (((uint32_t)0x03) << 26)
#define HFXO_XTALCTRL_SKIPCOREBIASOPT  (((uint32_t)0x01) << 31)

#define HFXO_CFG_MODE                  (((uint32_t)0x01) << 0)
#define HFXO_CFG_ENXIDCBIASANA         (((uint32_t)0x01) << 2)
#define HFXO_CFG_SQBUFSCHTRGANA        (((uint32_t)0x01) << 3)

#define HFXO_CTRL_FORCEEN              (((uint32_t)0x01) << 0)
#define HFXO_CTRL_DISONDEMAND          (((uint32_t)0x01) << 1)
#define HFXO_CTRL_KEEPWARM             (((uint32_t)0x01) << 2)
#define HFXO_CTRL_FORCEXI2GNDANA       (((uint32_t)0x01) << 4)
#define HFXO_CTRL_FORCEXO2GNDANA       (((uint32_t)0x01) << 5)

#define HFXO_CMD_COREBIASOPT           (((uint32_t)0x01) << 0)
#define HFXO_CMD_MANUALOVERRIDE        (((uint32_t)0x01) << 1)

#define HFXO_STATUS_RDY                (((uint32_t)0x01) << 0)
#define HFXO_STATUS_COREBIASOPTRDY     (((uint32_t)0x01) << 1)               
#define HFXO_STATUS_ENS                (((uint32_t)0x01) << 16)
#define HFXO_STATUS_HWREQ              (((uint32_t)0x01) << 17)
#define HFXO_STATUS_ISWARM             (((uint32_t)0x01) << 19)
#define HFXO_STATUS_FSMLOCK            (((uint32_t)0x01) << 30)
#define HFXO_STATUS_LOCK               (((uint32_t)0x01) << 31)

#define HFXO_IF_RDY                    (((uint32_t)0x01) << 0)
#define HFXO_IF_COREBIASOPTRDY         (((uint32_t)0x01) << 1)
#define HFXO_IF_DNSERR                 (((uint32_t)0x01) << 29)
#define HFXO_IF_COREBIASOPTERR         (((uint32_t)0x01) << 31)

#define HFXO_IEN_RDY                   (((uint32_t)0x01) << 0)
#define HFXO_IEN_COREBIASOPTRDY        (((uint32_t)0x01) << 1)
#define HFXO_IEN_DNSERR                (((uint32_t)0x01) << 29)
#define HFXO_IEN_COREBIASOPTERR        (((uint32_t)0x01) << 31)

#define HFXO_LOCK_LOCKKEY              (((uint32_t)0xFFFF) << 0)

#endif
