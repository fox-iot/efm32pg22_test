#ifndef _EFM32PG22_PDM_H_
#define _EFM32PG22_PDM_H_

#include <stdint.h>

typedef struct{
	volatile uint32_t IPVERSION;
	volatile uint32_t EN;
	volatile uint32_t CTRL;
	volatile uint32_t CMD;
	volatile uint32_t STATUS;
	volatile uint32_t CFG0;
	volatile uint32_t CFG1;
	uint32_t RESERVED1[1];
	volatile uint32_t RXDATA;
	uint32_t RESERVED2[7];
	volatile uint32_t IF;
	volatile uint32_t IEN;
	uint32_t RESERVED3[6];
	volatile uint32_t SYNCBUSY;
}__attribute__((packed)) PDM_TypeDef;

#define PDM                            ((PDM_TypeDef *)0x40098000)
#define PDM_SET                        ((PDM_TypeDef *)0x40099000)
#define PDM_CLR                        ((PDM_TypeDef *)0x4009A000)
#define PDM_TGL                        ((PDM_TypeDef *)0x4009B000)

#define PDM_IPVERSION_IPVERSION        (((uint32_t)0xFFFFFFFF) << 0)

#define PDM_EN_EN                      (((uint32_t)0x01) << 0)

#define PDM_CTRL_GAIN                  (((uint32_t)0x1F) << 0)
#define PDM_CTRL_DSR                   (((uint32_t)0x0FFFFF) << 8)

#define PDM_CMD_START                  (((uint32_t)0x01) << 0)
#define PDM_CMD_STOP                   (((uint32_t)0x01) << 4)
#define PDM_CMD_CLEAR                  (((uint32_t)0x01) << 8)
#define PDM_CMD_FIFOFL                 (((uint32_t)0x01) << 16)

#define PDM_STATUS_ACT                 (((uint32_t)0x01) << 0)
#define PDM_STATUS_FULL                (((uint32_t)0x01) << 4)
#define PDM_STATUS_EMPTY               (((uint32_t)0x01) << 5)
#define PDM_STATUS_FIFOCNT             (((uint32_t)0x07) << 8)

#define PDM_CFG0_FORDER                (((uint32_t)0x03) << 0)
#define PDM_CFG0_NUMCH                 (((uint32_t)0x01) << 4)
#define PDM_CFG0_DATAFORMAT            (((uint32_t)0x07) << 8)
#define PDM_CFG0_FIFODVL               (((uint32_t)0x03) << 12)
#define PDM_CFG0_STEREOMODECH01        (((uint32_t)0x01) << 16)
#define PDM_CFG0_CH0CLKPOL             (((uint32_t)0x01) << 24)
#define PDM_CFG0_CH1CLKPOL             (((uint32_t)0x01) << 25)

#define PDM_CFG1_PRESC                 (((uint32_t)0x03FF) << 0)
#define PDM_CFG1_DLYMUXSEL             (((uint32_t)0x03) << 24)

#define PDM_RXDATA_RXDATA              (((uint32_t)0xFFFFFFFF) << 0)

#define PDM_IF_DV                      (((uint32_t)0x01) << 0)
#define PDM_IF_DVL                     (((uint32_t)0x01) << 1)
#define PDM_IF_OF                      (((uint32_t)0x01) << 2)
#define PDM_IF_UF                      (((uint32_t)0x01) << 3)

#define PDM_IEN_DV                     (((uint32_t)0x01) << 0)
#define PDM_IEN_DVL                    (((uint32_t)0x01) << 1)
#define PDM_IEN_OF                     (((uint32_t)0x01) << 2)
#define PDM_IEN_UF                     (((uint32_t)0x01) << 3)

#define PDM_SYNCBUSY_SYNCBUSY          (((uint32_t)0x01) << 0)
#define PDM_SYNCBUSY_FIFOFLBUSY        (((uint32_t)0x01) << 3)

#endif
