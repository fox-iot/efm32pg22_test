#ifndef _EFM32PG22_LDMAXBAR_H_
#define _EFM32PG22_LDMAXBAR_H_

#include <stdint.h>

typedef struct{
    volatile uint32_t CH_REQSEL[8];
}__attribute__((packed)) LDMAXBAR_TypeDef;

#define LDMAXBAR                      ((LDMAXBAR_TypeDef *)0x40044000)
#define LDMAXBAR_SET                  ((LDMAXBAR_TypeDef *)0x40045000)
#define LDMAXBAR_CLR                  ((LDMAXBAR_TypeDef *)0x40046000)
#define LDMAXBAR_TGL                  ((LDMAXBAR_TypeDef *)0x40047000)

#define LDMAXBAR_CH_REQSEL_SIGSEL     (((uint32_t)0x0F) << 0)
#define LDMAXBAR_CH_REQSEL_SOURCESEL  (((uint32_t)0x3F) << 16)

#endif
