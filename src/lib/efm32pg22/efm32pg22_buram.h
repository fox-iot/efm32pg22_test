#ifndef _EFM32PG22_BURAM_H_
#define _EFM32PG22_BURAM_H_

#include <stdint.h>

typedef struct{
	volatile uint32_t RET_REG[32];
}__attribute__((packed)) BURAM_TypeDef;

#define BURAM                          ((BURAM_TypeDef *)0x40080000)
#define BURAM_SET                      ((BURAM_TypeDef *)0x40081000)
#define BURAM_CLR                      ((BURAM_TypeDef *)0x40082000)
#define BURAM_TGL                      ((BURAM_TypeDef *)0x40083000)

#define BURAM_RET_REG_RETREG          (((uint32_t)0xFFFFFFFF) << 0)

#endif
