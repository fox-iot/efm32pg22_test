#ifndef _EFM32PG22_WDOG_H_
#define _EFM32PG22_WDOG_H_

#include <stdint.h>

typedef struct{
        volatile uint32_t IPVERSION;
        volatile uint32_t EN;
        volatile uint32_t CFG;
        volatile uint32_t CMD;
        uint32_t RESERVED1[1];
        volatile uint32_t STATUS;
        volatile uint32_t IF;
        volatile uint32_t IEN;
        volatile uint32_t LOCK;
        volatile uint32_t SYNCBUSY;
}__attribute__((packed)) WDOG_TypeDef;

#define WDOG0                           ((WDOG_TypeDef *)0x4A018000)
#define WDOG0_SET                       ((WDOG_TypeDef *)0x4A019000)
#define WDOG0_CLR                       ((WDOG_TypeDef *)0x4A01A000)
#define WDOG0_TGL                       ((WDOG_TypeDef *)0x4A01B000)

#define WDOG_IPVERSION_IPVERSION        (((uint32_t)0xFFFFFFFF) << 0)

#define WDOG_EN_EN                      (((uint32_t)0x01) << 0)

#define WDOG_CFG_CLRSRC                 (((uint32_t)0x01) << 0)
#define WDOG_CFG_EM2RUN                 (((uint32_t)0x01) << 1)
#define WDOG_CFG_EM3RUN                 (((uint32_t)0x01) << 2)
#define WDOG_CFG_EM4BLOCK               (((uint32_t)0x01) << 3)
#define WDOG_CFG_DEBUGRUN               (((uint32_t)0x01) << 4)
#define WDOG_CFG_WDOGRSTDIS             (((uint32_t)0x01) << 8)
#define WDOG_CFG_PRS0MISSRSTEN          (((uint32_t)0x01) << 9)
#define WDOG_CFG_PRS1MISSRSTEN          (((uint32_t)0x01) << 10)
#define WDOG_CFG_PERSEL                 (((uint32_t)0x0F) << 16)
#define WDOG_CFG_WARNSEL                (((uint32_t)0x03) << 24)
#define WDOG_CFG_WINSEL                 (((uint32_t)0x07) << 28)

#define WDOG_CMD_CLEAR                  (((uint32_t)0x01) << 0)

#define WDOG_STATUS_LOCK                (((uint32_t)0x01) << 31)

#define WDOG_IF_TOUT                    (((uint32_t)0x01) << 0)
#define WDOG_IF_WARN                    (((uint32_t)0x01) << 1)
#define WDOG_IF_WIN                     (((uint32_t)0x01) << 2)
#define WDOG_IF_PEM0                    (((uint32_t)0x01) << 3)
#define WDOG_IF_PEM1                    (((uint32_t)0x01) << 4)

#define WDOG_IEN_TOUT                   (((uint32_t)0x01) << 0)
#define WDOG_IEN_WARN                   (((uint32_t)0x01) << 1)
#define WDOG_IEN_WIN                    (((uint32_t)0x01) << 2)
#define WDOG_IEN_PEM0                   (((uint32_t)0x01) << 3)
#define WDOG_IEN_PEM1                   (((uint32_t)0x01) << 4)

#define WDOG_LOCK_LOCKKEY               (((uint32_t)0xFFFF) << 0)

#define WDOG_SYNCBUSY_CMD               (((uint32_t)0x01) << 0)

#define WDOG0_CMU_CLKSEL_LFRCO          0x01 /**LFRCO is clocking WDOG0CLK.         */
#define WDOG0_CMU_CLKSEL_LFXO           0x02 /**LFXO is clocking WDOG0CLK.          */
#define WDOG0_CMU_CLKSEL_ULFRCO         0x03 /**ULFRCO is clocking WDOG0CLK.        */
#define WDOG0_CMU_CLKSEL_HCLKDIV1024    0x04 /**HCLKDIV1024 is clocking WDOG0CLK.   */

#define WDOG0_CFG_WINDOW_DISABLE        0x0 /**< Disable */
#define WDOG0_CFG_WINDOW_SEL1_25        0x1 /**<window  timeout is 12.5% of the Timeout. */
#define WDOG0_CFG_WINDOW_SEL2_50        0x2 /**<window  timeout is 25% of the Timeout. */
#define WDOG0_CFG_WINDOW_SEL3_75        0x3 /**<window  timeout is 37.5% of the Timeout. */
#define WDOG0_CFG_WINDOW_SEL4_25        0x4 /**<window  timeout is 50% of the Timeout. */
#define WDOG0_CFG_WINDOW_SEL5_50        0x5 /**<window  timeout is 62.5% of the Timeout. */
#define WDOG0_CFG_WINDOW_SEL6_75        0x6 /**<window  timeout is 75.5% of the Timeout. */
#define WDOG0_CFG_WINDOW_SEL7_75        0x7 /**<window  timeout is 87.5% of the Timeout. */

#define WDOG0_CFG_WARNSEL_DISABLE       0x0 /**< Disable */
#define WDOG0_CFG_WARNSEL_SEL1_25       0x1 /**< Warning timeout is 25% of the Timeout. */
#define WDOG0_CFG_WARNSEL_SEL2_50       0x2 /**< Warning timeout is 50% of the Timeout. */
#define WDOG0_CFG_WARNSEL_SEL3_75       0x3 /**< Warning timeout is 75% of the Timeout. */

#define WDOG0_CFG_PERSEL_SEL0_9         0x0 /**< 9 clock periods */
#define WDOG0_CFG_PERSEL_SEL1_17        0x1 /**< 17 clock periods */
#define WDOG0_CFG_PERSEL_SEL2_33        0x2 /**< 33 clock periods */
#define WDOG0_CFG_PERSEL_SEL3_65        0x3 /**< 65 clock periods */
#define WDOG0_CFG_PERSEL_SEL4_129       0x4 /**< 129 clock periods */
#define WDOG0_CFG_PERSEL_SEL5_257       0x5 /**< 257 clock periods */
#define WDOG0_CFG_PERSEL_SEL6_513       0x6 /**< 513 clock periods */
#define WDOG0_CFG_PERSEL_SEL7_1K        0x7 /**< 1025 clock periods */
#define WDOG0_CFG_PERSEL_SEL8_2K        0x8 /**< 2049 clock periods */
#define WDOG0_CFG_PERSEL_SEL9_4K        0x9 /**< 4097 clock periods */
#define WDOG0_CFG_PERSEL_SEL10_8K       0xA /**< 8193 clock periods */
#define WDOG0_CFG_PERSEL_SEL11_16K      0xB /**< 16385 clock periods */
#define WDOG0_CFG_PERSEL_SEL12_32K      0xC /**< 32769 clock periods */
#define WDOG0_CFG_PERSEL_SEL13_64K      0xD /**< 65537 clock periods */
#define WDOG0_CFG_PERSEL_SEL14_128K     0xE /**< 131073 clock periods */
#define WDOG0_CFG_PERSEL_SEL15_256K     0xF /**< 262145 clock periods */

#endif
