#ifndef _EFM32PG22_IADC_H_
#define _EFM32PG22_IADC_H_

#include <stdint.h>

typedef struct {
	volatile uint32_t CFG;
	uint32_t RESERVED;
	volatile uint32_t SCALE;
	volatile uint32_t SCHED;
}__attribute__((packed)) IADC_CFG_TypeDef;

typedef struct{
    volatile uint32_t IPVERSION;
    volatile uint32_t EN;
    volatile uint32_t CTRL;
    volatile uint32_t CMD;
    volatile uint32_t TIMER;
    volatile uint32_t STATUS;
    volatile uint32_t MASKREQ;
    volatile uint32_t STMASK;
    volatile uint32_t CMPTHR;
    volatile uint32_t IF;
    volatile uint32_t IEN;
    volatile uint32_t TRIGGER;
    uint32_t RESERVED1[6];
	IADC_CFG_TypeDef CFG[2];
    uint32_t RESERVED3[2];
    volatile uint32_t SINGLEFIFOCFG;
    volatile uint32_t SINGLEFIFODATA;
    volatile uint32_t SINGLEFIFOSTAT;
    volatile uint32_t SINGLEDATA;
    volatile uint32_t SCANFIFOCFG;
    volatile uint32_t SCANFIFODATA;
    volatile uint32_t SCANFIFOSTAT;
    volatile uint32_t SCANDATA;
    uint32_t RESERVED4[2];
    volatile uint32_t SINGLE;
    uint32_t RESERVED5[1];
    volatile uint32_t SCAN[16];
}__attribute__((packed)) IADC_TypeDef;

#define IADC0                              ((IADC_TypeDef *)0x4A004000)  
#define IADC0_SET                          ((IADC_TypeDef *)0x4A005000)
#define IADC0_CLR                          ((IADC_TypeDef *)0x4A006000)
#define IADC0_TGL                          ((IADC_TypeDef *)0x4A007000)

#define IADC_IPVERSION_IPVERSION           (((uint32_t)0x01) << 0)

#define IADC_EN_EN                         (((uint32_t)0x01) << 0)

#define IADC_CTRL_EM23WUCONVERT            (((uint32_t)0x01) << 0)
#define IADC_CTRL_ADCCLKSUSPEND0           (((uint32_t)0x01) << 1)
#define IADC_CTRL_ADCCLKSUSPEND1           (((uint32_t)0x01) << 2)
#define IADC_CTRL_DBGHALT                  (((uint32_t)0x01) << 3)
#define IADC_CTRL_WARMUPMODE               (((uint32_t)0x03) << 4)
#define IADC_CTRL_TIMEBASE                 (((uint32_t)0x7F) << 16)
#define IADC_CTRL_HSCLKRATE                (((uint32_t)0x07) << 28)

#define IADC_CMD_SINGLESTART               (((uint32_t)0x01) << 0) 
#define IADC_CMD_SINGLESTOP                (((uint32_t)0x01) << 1)
#define IADC_CMD_SCANSTART                 (((uint32_t)0x01) << 3)
#define IADC_CMD_SCANSTOP                  (((uint32_t)0x01) << 4)
#define IADC_CMD_TIMEREN                   (((uint32_t)0x01) << 16)
#define IADC_CMD_TIMERDIS                  (((uint32_t)0x01) << 17)
#define IADC_CMD_SINGLEFIFOFLUSH           (((uint32_t)0x01) << 24)
#define IADC_CMD_SCANFIFOFLUSH             (((uint32_t)0x01) << 25)

#define IADC_TIMER_TIMER                   (((uint32_t)0xFFFF) << 0)

#define IADC_STATUS_SINGLEQEN              (((uint32_t)0x01) << 0)  
#define IADC_STATUS_SINGLEQUEUEPENDING     (((uint32_t)0x01) << 1)
#define IADC_STATUS_SCANQEN                (((uint32_t)0x01) << 3)
#define IADC_STATUS_SCANQUEUEPENDING       (((uint32_t)0x01) << 4)
#define IADC_STATUS_CONVERTING             (((uint32_t)0x01) << 6)
#define IADC_STATUS_SINGLEFIFODV           (((uint32_t)0x01) << 8)
#define IADC_STATUS_SCANFIFODV             (((uint32_t)0x01) << 9)
#define IADC_STATUS_SINGLEFIFOFLUSHING     (((uint32_t)0x01) << 14)
#define IADC_STATUS_SCANFIFOFLUSHING       (((uint32_t)0x01) << 15)
#define IADC_STATUS_TIMERACTIVE            (((uint32_t)0x01) << 16)
#define IADC_STATUS_SINGLEWRITEPENDING     (((uint32_t)0x01) << 20)
#define IADC_STATUS_MASKREQWRITEPENDING    (((uint32_t)0x01) << 21)
#define IADC_STATUS_SYNCBUSY               (((uint32_t)0x01) << 24)
#define IADC_STATUS_ADCWARM                (((uint32_t)0x01) << 30)

#define IADC_MASKREQ_MASKREQ               (((uint32_t)0xFFFF) << 0)

#define IADC_STMASK_STMASK                 (((uint32_t)0xFFFF) << 0)

#define IADC_CMPTHR_ADLT                   (((uint32_t)0xFFFF) << 0)
#define IADC_CMPTHR_ADGT                   (((uint32_t)0xFFFF) << 16)

#define IADC_IF_SINGLEFIFODVL              (((uint32_t)0x01) << 0)
#define IADC_IF_SCANFIFODVL                (((uint32_t)0x01) << 1)
#define IADC_IF_SINGLECMP                  (((uint32_t)0x01) << 2)
#define IADC_IF_SCANCMP                    (((uint32_t)0x01) << 3)
#define IADC_IF_SCANENTRYDONE              (((uint32_t)0x01) << 7)
#define IADC_IF_SCANTABLEDONE              (((uint32_t)0x01) << 8)
#define IADC_IF_SINGLEDONE                 (((uint32_t)0x01) << 9)
#define IADC_IF_POLARITYERR                (((uint32_t)0x01) << 12)
#define IADC_IF_PORTALLOCERR               (((uint32_t)0x01) << 13)
#define IADC_IF_SINGLEFIFOOF               (((uint32_t)0x01) << 16)
#define IADC_IF_SCANFIFOOF                 (((uint32_t)0x01) << 17)
#define IADC_IF_SINGLEFIFOUF               (((uint32_t)0x01) << 18)
#define IADC_IF_SCANFIFOUF                 (((uint32_t)0x01) << 19)
#define IADC_IF_EM23ABORTERROR             (((uint32_t)0x01) << 31)   

#define IADC_IEN_SINGLEFIFODVL             (((uint32_t)0x01) << 0)
#define IADC_IEN_SCANFIFODVL               (((uint32_t)0x01) << 1)
#define IADC_IEN_SINGLECMP                 (((uint32_t)0x01) << 2)
#define IADC_IEN_SCANCMP                   (((uint32_t)0x01) << 3)
#define IADC_IEN_SCANENTRYDONE             (((uint32_t)0x01) << 7)
#define IADC_IEN_SCANTABLEDONE             (((uint32_t)0x01) << 8)
#define IADC_IEN_SINGLEDONE                (((uint32_t)0x01) << 9)
#define IADC_IEN_POLARITYERR               (((uint32_t)0x01) << 12)
#define IADC_IEN_PORTALLOCERR              (((uint32_t)0x01) << 13)
#define IADC_IEN_SINGLEFIFOOF              (((uint32_t)0x01) << 16)
#define IADC_IEN_SCANFIFOOF                (((uint32_t)0x01) << 17)
#define IADC_IEN_SINGLEFIFOUF              (((uint32_t)0x01) << 18)
#define IADC_IEN_SCANFIFOUF                (((uint32_t)0x01) << 19)
#define IADC_IEN_EM23ABORTERROR            (((uint32_t)0x01) << 31)

#define IADC_TRIGGER_SCANTRIGSEL           (((uint32_t)0x07) << 0)
#define IADC_TRIGGER_SCANTRIGACTION        (((uint32_t)0x01) << 4)
#define IADC_TRIGGER_SINGLETRIGSEL         (((uint32_t)0x07) << 8)
#define IADC_TRIGGER_SINGLETRIGACTION      (((uint32_t)0x01) << 12)
#define IADC_TRIGGER_SINGLETAILGATE        (((uint32_t)0x01) << 16)

#define IADC_CFG_ADCMODE                  (((uint32_t)0x03) << 0)
#define IADC_CFG_OSRHS                    (((uint32_t)0x07) << 2)
#define IADC_CFG_ANALOGGAIN               (((uint32_t)0x07) << 12)
#define IADC_CFG_REFSEL                   (((uint32_t)0x07) << 16)
#define IADC_CFG_DIGAVG                   (((uint32_t)0x07) << 21)
#define IADC_CFG_TWOSCOMPL                (((uint32_t)0x03) << 28)

#define IADC_SCALE_OFFSET                 (((uint32_t)0x03FFFF) << 0)
#define IADC_SCALE_GAIN13LSB              (((uint32_t)0x1FFF) << 18)
#define IADC_SCALE_GAIN3MSB               (((uint32_t)0x01) << 31)

#define IADC_SCHED_PRESCALE               (((uint32_t)0x03FF) << 0)

#define IADC_SINGLEFIFOCFG_ALIGNMENT       (((uint32_t)0x07) << 0)
#define IADC_SINGLEFIFOCFG_SHOWID          (((uint32_t)0x01) << 3)
#define IADC_SINGLEFIFOCFG_DVL             (((uint32_t)0x03) << 4)
#define IADC_SINGLEFIFOCFG_DMAWUFIFOSINGLE (((uint32_t)0x01) << 8)

#define IADC_SINGLEFIFODATA_DATA           (((uint32_t)0xFFFFFFFF) << 0)

#define IADC_SINGLEFIFOSTAT_FIFOREADCNT    (((uint32_t)0x07) << 0)

#define IADC_SINGLEDATA_DATA               (((uint32_t)0xFFFFFFFF) << 0)

#define IADC_SCANFIFOCFG_ALIGNMENT         (((uint32_t)0x07) << 0)
#define IADC_SCANFIFOCFG_SHOWID            (((uint32_t)0x01) << 3)
#define IADC_SCANFIFOCFG_DVL               (((uint32_t)0x03) << 4)
#define IADC_SCANFIFOCFG_DMAWUFIFOSCAN     (((uint32_t)0x01) << 8)

#define IADC_SCANFIFODATA_DATA             (((uint32_t)0xFFFFFFFF) << 0)

#define IADC_SCANFIFOSTAT_FIFOREADCNT      (((uint32_t)0x07) << 0)

#define IADC_SCANDATA_DATA                 (((uint32_t)0xFFFFFFFF) << 0)

#define IADC_SINGLE_PINNEG                 (((uint32_t)0x0F) << 0)
#define IADC_SINGLE_PORTNEG                (((uint32_t)0x0F) << 4)
#define IADC_SINGLE_PINPOS                 (((uint32_t)0x0F) << 8)
#define IADC_SINGLE_PORTPOS                (((uint32_t)0x0F) << 12)
#define IADC_SINGLE_CFG                    (((uint32_t)0x01) << 16)
#define IADC_SINGLE_CMP                    (((uint32_t)0x01) << 17)

#define IADC_SCAN_PINNEG                  (((uint32_t)0x0F) << 0)
#define IADC_SCAN_PORTNEG                 (((uint32_t)0x0F) << 4)
#define IADC_SCAN_PORTNEG_GND             0
#define IADC_SCAN_PORTNEG_PORTA           8
#define IADC_SCAN_PORTNEG_PORTB           9
#define IADC_SCAN_PORTNEG_PORTC           10
#define IADC_SCAN_PORTNEG_PORTD           11
#define IADC_SCAN_PINPOS                  (((uint32_t)0x0F) << 8)
#define IADC_SCAN_PORTPOS                 (((uint32_t)0x0F) << 12)
#define IADC_SCAN_PORTPOS_GND             0
#define IADC_SCAN_PORTPOS_SUPPLY          1
#define IADC_SCAN_PORTPOS_PORTA           8
#define IADC_SCAN_PORTPOS_PORTB           9
#define IADC_SCAN_PORTPOS_PORTC           10
#define IADC_SCAN_PORTPOS_PORTD           11
#define IADC_SCAN_CFG                     (((uint32_t)0x01) << 16)
#define IADC_SCAN_CMP                     (((uint32_t)0x01) << 17)

#endif
