
#ifndef _EFM32PG22_CMU_H_
#define _EFM32PG22_CMU_H_

#include <stdint.h>

typedef struct{
	volatile uint32_t IPVERSION;
	uint32_t RESERVED1[1];
	volatile uint32_t STATUS;
	uint32_t  RESERVED2[1];
	volatile uint32_t LOCK;
	volatile uint32_t WDOGLOCK;
	uint32_t  RESERVED3[2];
	volatile uint32_t IF;
	volatile uint32_t IEN;
	uint32_t RESERVED4[10];
	volatile uint32_t CALCMD;
	volatile uint32_t CALCTRL;
	volatile uint32_t CALCNT;
	uint32_t RESERVED5[2];
	volatile uint32_t CLKEN0;
	volatile uint32_t CLKEN1;
	uint32_t RESERVED6[1];
	volatile uint32_t SYSCLKCTRL;
	uint32_t RESERVED7[3];
	volatile uint32_t TRACECLKCTRL;
	uint32_t RESERVED8[3];
	volatile uint32_t EXPORTCLKCTRL;
	uint32_t RESERVED9[27];
	volatile uint32_t DPLLREFCLKCTRL;
	uint32_t RESERVED10[7];
	volatile uint32_t EM01GRPACLKCTRL;
	volatile uint32_t EM01GRPBCLKCTRL;
	uint32_t RESERVED11[6];
	volatile uint32_t EM23GRPACLKCTRL;
	uint32_t RESERVED12[7];
	volatile uint32_t EM4GRPACLKCTRL;
	uint32_t RESERVED13[7];
	volatile uint32_t IADCCLKCTRL;
	uint32_t RESERVED14[31];
	volatile uint32_t WDOG0CLKCTRL;
	uint32_t RESERVED15[7];
	volatile uint32_t EUART0CLKCTRL;
	uint32_t RESERVED16[7];
	volatile uint32_t RTCCCLKCTRL;
	uint32_t RESERVED17[7];
	volatile uint32_t CRYPTOACCCLKCTRL;
}__attribute__((packed)) CMU_TypeDef;


#define CMU                            ((CMU_TypeDef *)0x40008000)
#define CMU_SET                        ((CMU_TypeDef *)0x40009000)
#define CMU_CLR                        ((CMU_TypeDef *)0x4000A000)
#define CMU_TGL                        ((CMU_TypeDef *)0x4000B000)


#define CMU_IPVERSION_IPVERSION        (((uint32_t)0xFFFFFFFF) << 0)

#define CMU_STATUS_CALRDY              (((uint32_t)0x01) << 0)
#define CMU_STATUS_WDOGLOCK            (((uint32_t)0x01) << 30)
#define CMU_STATUS_LOCK                (((uint32_t)0x01) << 31)

#define CMU_LOCK_LOCKKEY               (((uint32_t)0xFFFF) << 0)

#define CMU_WDOGLOCK_LOCKKEY           (((uint32_t)0xFFFF) << 0)

#define CMU_IF_CALRDY                  (((uint32_t)0x01) << 0)
#define CMU_IF_CALOF                   (((uint32_t)0x01) << 1)

#define CMU_IEN_CALRDY                  (((uint32_t)0x01) << 0)
#define CMU_IEN_CALOF                   (((uint32_t)0x01) << 1)

#define CMU_CALCMD_CALSTART            (((uint32_t)0x01) << 0)
#define CMU_CALCMD_CALSTOP             (((uint32_t)0x01) << 1)

#define CMU_CALCTRL_CALTOP             (((uint32_t)0x0FFFFF) << 0)
#define CMU_CALCTRL_CONT               (((uint32_t)0x01) << 23)
#define CMU_CALCTRL_UPSEL              (((uint32_t)0x0F) << 24)
#define CMU_CALCTRL_DOWNSEL            (((uint32_t)0x0F) << 28)

#define CMU_CALCNT_CALCNT              (((uint32_t)0x0FFFFF) << 0)

#define CMU_CLKEN0_LDMA                (((uint32_t)0x01) << 0)
#define CMU_CLKEN0_LDMAXBAR            (((uint32_t)0x01) << 1)
#define CMU_CLKEN0_GPCRC               (((uint32_t)0x01) << 3)
#define CMU_CLKEN0_TIMER0              (((uint32_t)0x01) << 4)
#define CMU_CLKEN0_TIMER1              (((uint32_t)0x01) << 5)
#define CMU_CLKEN0_TIMER2              (((uint32_t)0x01) << 6)
#define CMU_CLKEN0_TIMER3              (((uint32_t)0x01) << 7)
#define CMU_CLKEN0_USART0              (((uint32_t)0x01) << 8)
#define CMU_CLKEN0_USART1              (((uint32_t)0x01) << 9)
#define CMU_CLKEN0_IADC0               (((uint32_t)0x01) << 10)
#define CMU_CLKEN0_AMUXCP0             (((uint32_t)0x01) << 11)
#define CMU_CLKEN0_LETIMER0            (((uint32_t)0x01) << 12)
#define CMU_CLKEN0_WDOG0               (((uint32_t)0x01) << 13)
#define CMU_CLKEN0_I2C0                (((uint32_t)0x01) << 14)
#define CMU_CLKEN0_I2C1                (((uint32_t)0x01) << 15)
#define CMU_CLKEN0_SYSCFG              (((uint32_t)0x01) << 16)
#define CMU_CLKEN0_DPLL0               (((uint32_t)0x01) << 17)
#define CMU_CLKEN0_HFRCO0              (((uint32_t)0x01) << 18)
#define CMU_CLKEN0_HFXO0               (((uint32_t)0x01) << 19)
#define CMU_CLKEN0_FSRCO               (((uint32_t)0x01) << 20)
#define CMU_CLKEN0_LFRCO               (((uint32_t)0x01) << 21)
#define CMU_CLKEN0_LFXO                (((uint32_t)0x01) << 22)
#define CMU_CLKEN0_ULFRCO              (((uint32_t)0x01) << 23)
#define CMU_CLKEN0_EUART0              (((uint32_t)0x01) << 24)
#define CMU_CLKEN0_PDM                 (((uint32_t)0x01) << 25)
#define CMU_CLKEN0_GPIO                (((uint32_t)0x01) << 26)
#define CMU_CLKEN0_PRS                 (((uint32_t)0x01) << 27)
#define CMU_CLKEN0_BURAM               (((uint32_t)0x01) << 28)
#define CMU_CLKEN0_BURTC               (((uint32_t)0x01) << 29)
#define CMU_CLKEN0_RTCC                (((uint32_t)0x01) << 30)
#define CMU_CLKEN0_DCDC                (((uint32_t)0x01) << 31)

#define CMU_CLKEN1_CRYPTOACC           (((uint32_t)0x01) << 13)
#define CMU_CLKEN1_SMU                 (((uint32_t)0x01) << 15)
#define CMU_CLKEN1_ICACHE0             (((uint32_t)0x01) << 16)
#define CMU_CLKEN1_MSC                 (((uint32_t)0x01) << 17)
#define CMU_CLKEN1_TIMER4              (((uint32_t)0x01) << 18)

#define CMU_SYSCLKCTRL_CLKSEL          (((uint32_t)0x07) << 0)
#define CMU_SYSCLKCTRL_PCLKPRESC       (((uint32_t)0x01) << 10)
#define CMU_SYSCLKCTRL_HCLKPRESC       (((uint32_t)0x0F) << 12)

#define CMU_TRACECLKCTRL_PRESC         (((uint32_t)0x03) << 4)

#define CMU_EXPORTCLKCTRL_CLKOUTSEL0   (((uint32_t)0x0F) << 0)
#define CMU_EXPORTCLKCTRL_CLKOUTSEL1   (((uint32_t)0x0F) << 8)
#define CMU_EXPORTCLKCTRL_CLKOUTSEL2   (((uint32_t)0x0F) << 16)
#define CMU_EXPORTCLKCTRL_PRESC        (((uint32_t)0x1F) << 24)

#define CMU_DPLLREFCLKCTRL_CLKSEL      (((uint32_t)0x03) << 0)

#define CMU_EM01GRPACLKCTRL_CLKSEL     (((uint32_t)0x03) << 0)

#define CMU_EM01GRPBCLKCTRL_CLKSEL     (((uint32_t)0x07) << 0)

#define CMU_EM23GRPACLKCTRL_CLKSEL     (((uint32_t)0x03) << 0)

#define CMU_EM4GRPACLKCTRL_CLKSEL      (((uint32_t)0x03) << 0)

#define CMU_IADCCLKCTRL_CLKSEL         (((uint32_t)0x03) << 0)

#define CMU_WDOG0CLKCTRL_CLKSEL        (((uint32_t)0x07) << 0)

#define CMU_EUART0CLKCTRL_CLKSEL       (((uint32_t)0x03) << 0)

#define CMU_RTCCCLKCTRL_CLKSEL         (((uint32_t)0x03) << 0)

#define CMU_CRYPTOACCCLKCTRL_PKEN      (((uint32_t)0x01) << 0)
#define CMU_CRYPTOACCCLKCTRL_AESEN     (((uint32_t)0x01) << 1)

#endif
