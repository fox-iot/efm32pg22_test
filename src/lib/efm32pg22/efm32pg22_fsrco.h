
#ifndef _EFM32PG22_FSRCO_H_
#define _EFM32PG22_FSRCO_H_

#include <stdint.h>

typedef struct{
	volatile uint32_t IPVERSION;
}__attribute__((packed)) FSRCO_TypeDef;

#define FSRCO                          ((FSRCO_TypeDef *)0x40018000)
#define FSRCO_SET                      ((FSRCO_TypeDef *)0x40019000)
#define FSRCO_CLR                      ((FSRCO_TypeDef *)0x4001A000)
#define FSRCO_TGL                      ((FSRCO_TypeDef *)0x4001B000)

#define FSRCO_IPVERSION_IPVERSION      (((uint32_t)0xFFFFFFFF) << 0)

#endif
