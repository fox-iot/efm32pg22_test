#ifndef _EFM32PG22_ICACHE_H_
#define _EFM32PG22_ICACHE_H_

#include <stdint.h>

typedef struct{
	volatile uint32_t IPVERSION;
	volatile uint32_t CTRL;
	volatile uint32_t PCHITS;
	volatile uint32_t PCMISSES;
	volatile uint32_t PCAHITS;
	volatile uint32_t STATUS;
	volatile uint32_t CMD;
	volatile uint32_t LPMODE;
	volatile uint32_t IF;
	volatile uint32_t IEN;
}__attribute__((packed)) ICACHE_TypeDef;

#define ICACHE0                        ((ICACHE_TypeDef *)0x40034000)
#define ICACHE0_SET                    ((ICACHE_TypeDef *)0x40035000)
#define ICACHE0_CLR                    ((ICACHE_TypeDef *)0x40036000)
#define ICACHE0_TGL                    ((ICACHE_TypeDef *)0x40037000)

#define ICACHE_IPVERSION_IPVERSION     (((uint32_t)0xFFFFFFFF) << 0)

#define ICACHE_CTRL_CACHEDIS           (((uint32_t)0x01) << 0)
#define ICACHE_CTRL_USEMPU             (((uint32_t)0x01) << 1)
#define ICACHE_CTRL_AUTOFLUSHDIS       (((uint32_t)0x01) << 2)

#define ICACHE_PCHITS_PCHITS           (((uint32_t)0xFFFFFFFF) << 0)

#define ICACHE_PCMISSES_PCMISSES       (((uint32_t)0xFFFFFFFF) << 0)

#define ICACHE_PCAHITS_PCAHITS         (((uint32_t)0xFFFFFFFF) << 0)

#define ICACHE_STATUS_PCRUNNING        (((uint32_t)0x01) << 0)

#define ICACHE_CMD_FLUSH               (((uint32_t)0x01) << 0)
#define ICACHE_CMD_STARTPC             (((uint32_t)0x01) << 1)
#define ICACHE_CMD_STOPPC              (((uint32_t)0x01) << 2)

#define ICACHE_LPMODE_LPLEVEL          (((uint32_t)0x03) << 0)
#define ICACHE_LPMODE_NESTFACTOR       (((uint32_t)0x0F) << 4)

#define ICACHE_IF_HITOF                (((uint32_t)0x01) << 0)
#define ICACHE_IF_MISSOF               (((uint32_t)0x01) << 1)
#define ICACHE_IF_AHITOF               (((uint32_t)0x01) << 2)
#define ICACHE_IF_RAMERROR             (((uint32_t)0x01) << 8)

#define ICACHE_IF_HITOF                (((uint32_t)0x01) << 0)
#define ICACHE_IF_MISSOF               (((uint32_t)0x01) << 1)
#define ICACHE_IF_AHITOF               (((uint32_t)0x01) << 2)
#define ICACHE_IF_RAMERROR             (((uint32_t)0x01) << 8)

#endif
