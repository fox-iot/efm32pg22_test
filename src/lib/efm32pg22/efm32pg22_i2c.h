#ifndef _EFM32PG22_I2C_H_
#define _EFM32PG22_I2C_H_

#include <stdint.h>

typedef struct{
	volatile uint32_t IPVERSION;
	volatile uint32_t EN;
	volatile uint32_t CTRL;
	volatile uint32_t CMD;
	volatile uint32_t STATE;
	volatile uint32_t STATUS;
	volatile uint32_t CLKDIV;
	volatile uint32_t SADDR;
	volatile uint32_t SADDRMASK;
	volatile uint32_t RXDATA;
	volatile uint32_t RXDOUBLE;
	volatile uint32_t RXDATAP;
	volatile uint32_t RXDOUBLEP;
	volatile uint32_t TXDATA;
	volatile uint32_t TXDOUBLE;
	volatile uint32_t IF;
}__attribute__((packed)) I2C_TypeDef;

#define I2C0                    ((I2C_TypeDef *)0x4A010000)
#define I2C0_SET                ((I2C_TypeDef *)0x4A011000)
#define I2C0_CLR                ((I2C_TypeDef *)0x4A012000)
#define I2C0_TGL                ((I2C_TypeDef *)0x4A013000)

#define I2C1                    ((I2C_TypeDef *)0x40068000)
#define I2C1_SET                ((I2C_TypeDef *)0x40069000)
#define I2C1_CLR                ((I2C_TypeDef *)0x4006A000)
#define I2C1_TGL                ((I2C_TypeDef *)0x4006B000)

#define I2C_EN_EN               (((uint32_t)0x01) << 0)

#define I2C_CTRL_CORERST        (((uint32_t)0x01) << 0)
#define I2C_CTRL_SLAVE          (((uint32_t)0x01) << 1)
#define I2C_CTRL_AUTOACK        (((uint32_t)0x01) << 2)
#define I2C_CTRL_AUTOSE         (((uint32_t)0x01) << 3)
#define I2C_CTRL_AUTOSN         (((uint32_t)0x01) << 4)
#define I2C_CTRL_ARBDIS         (((uint32_t)0x01) << 5)
#define I2C_CTRL_GCAMEN         (((uint32_t)0x01) << 6)
#define I2C_CTRL_TXBIL          (((uint32_t)0x01) << 7)
#define I2C_CTRL_CLHR           (((uint32_t)0x03) << 8)
#define I2C_CTRL_BITO           (((uint32_t)0x03) << 12)
#define I2C_CTRL_GIBITO         (((uint32_t)0x01) << 15)
#define I2C_CTRL_CLTO           (((uint32_t)0x07) << 16)
#define I2C_CTRL_SCLMONEN       (((uint32_t)0x01) << 20)
#define I2C_CTRL_SDAMONEN       (((uint32_t)0x01) << 21)

#define I2C_CMD_START           (((uint32_t)0x01) << 0)
#define I2C_CMD_STOP            (((uint32_t)0x01) << 1)
#define I2C_CMD_ACK             (((uint32_t)0x01) << 2)
#define I2C_CMD_NACK            (((uint32_t)0x01) << 3)
#define I2C_CMD_CONT            (((uint32_t)0x01) << 4)
#define I2C_CMD_ABORT           (((uint32_t)0x01) << 5)
#define I2C_CMD_CLEARTX         (((uint32_t)0x01) << 6)
#define I2C_CMD_CLEARPC         (((uint32_t)0x01) << 7)

#define I2C_STATE_BUSY          (((uint32_t)0x01) << 0)
#define I2C_STATE_MASTER        (((uint32_t)0x01) << 1)
#define I2C_STATE_TRANSMITTER   (((uint32_t)0x01) << 2)
#define I2C_STATE_NACKED        (((uint32_t)0x01) << 3)
#define I2C_STATE_BUSHOLD       (((uint32_t)0x01) << 4)
#define I2C_STATE_STATE         (((uint32_t)0x07) << 5)

#define I2C_STATUS_PSTART       (((uint32_t)0x01) << 0)
#define I2C_STATUS_PSTOP        (((uint32_t)0x01) << 1)
#define I2C_STATUS_PACK         (((uint32_t)0x01) << 2)
#define I2C_STATUS_PNACK        (((uint32_t)0x01) << 3)
#define I2C_STATUS_PCONT        (((uint32_t)0x01) << 4)
#define I2C_STATUS_PABORT       (((uint32_t)0x01) << 5)
#define I2C_STATUS_TXC          (((uint32_t)0x01) << 6)
#define I2C_STATUS_TXBL         (((uint32_t)0x01) << 7)
#define I2C_STATUS_RXDATAV      (((uint32_t)0x01) << 8)
#define I2C_STATUS_RXFULL       (((uint32_t)0x01) << 9)
#define I2C_STATUS_TXBUFCNT     (((uint32_t)0x03) << 10)

#define I2C_CLKDIV_DIV          (((uint32_t)0x01FF) << 0)

#define I2C_SADDR_ADDR          (((uint32_t)0x7F) << 1)

#define I2C_SADDRMASK_SADDRMASK (((uint32_t)0x7F) << 1)

#define I2C_RXDATA_RXDATA       (((uint32_t)0xFF) << 0)

#define I2C_RXDOUBLE_RXDATA0    (((uint32_t)0xFF) << 0)
#define I2C_RXDOUBLE_RXDATA1    (((uint32_t)0xFF) << 8)

#define I2C_RXDATAP_RXDATAP     (((uint32_t)0xFF) << 0)

#define I2C_RXDOUBLEP_RXDATAP0  (((uint32_t)0xFF) << 0)
#define I2C_RXDOUBLEP_RXDATAP1  (((uint32_t)0xFF) << 8)

#define I2C_TXDATA_TXDATA       (((uint32_t)0xFF) << 0)

#define I2C_TXDOUBLE_TXDATA0    (((uint32_t)0xFF) << 0)
#define I2C_TXDOUBLE_TXDATA1    (((uint32_t)0xFF) << 8)

#define I2C_IF_START            (((uint32_t)0x01) << 0)
#define I2C_IF_RSTART           (((uint32_t)0x01) << 1)
#define I2C_IF_ADDR             (((uint32_t)0x01) << 2)
#define I2C_IF_TXC              (((uint32_t)0x01) << 3)
#define I2C_IF_TXBL             (((uint32_t)0x01) << 4)
#define I2C_IF_RXDATAV          (((uint32_t)0x01) << 5)
#define I2C_IF_ACK              (((uint32_t)0x01) << 6)
#define I2C_IF_NACK             (((uint32_t)0x01) << 7)
#define I2C_IF_MSTOP            (((uint32_t)0x01) << 8)
#define I2C_IF_ARBLOST          (((uint32_t)0x01) << 9)
#define I2C_IF_BUSERR           (((uint32_t)0x01) << 10)
#define I2C_IF_BUSHOLD          (((uint32_t)0x01) << 11)
#define I2C_IF_TXOF             (((uint32_t)0x01) << 12)
#define I2C_IF_RXUF             (((uint32_t)0x01) << 13)
#define I2C_IF_BITO             (((uint32_t)0x01) << 14)
#define I2C_IF_CLTO             (((uint32_t)0x01) << 15)
#define I2C_IF_SSTOP            (((uint32_t)0x01) << 16)
#define I2C_IF_RXFULL           (((uint32_t)0x01) << 17)
#define I2C_IF_CLERR            (((uint32_t)0x01) << 18)
#define I2C_IF_SCLERR           (((uint32_t)0x01) << 19)
#define I2C_IF_SDAERR           (((uint32_t)0x01) << 20)

#define I2C_IEN_START           (((uint32_t)0x01) << 0)
#define I2C_IEN_RSTART          (((uint32_t)0x01) << 1)
#define I2C_IEN_ADDR            (((uint32_t)0x01) << 2)
#define I2C_IEN_TXC             (((uint32_t)0x01) << 3)
#define I2C_IEN_TXBL            (((uint32_t)0x01) << 4)
#define I2C_IEN_RXDATAV         (((uint32_t)0x01) << 5)
#define I2C_IEN_ACK             (((uint32_t)0x01) << 6)
#define I2C_IEN_NACK            (((uint32_t)0x01) << 7)
#define I2C_IEN_MSTOP           (((uint32_t)0x01) << 8)
#define I2C_IEN_ARBLOST         (((uint32_t)0x01) << 9)
#define I2C_IEN_BUSERR          (((uint32_t)0x01) << 10)
#define I2C_IEN_BUSHOLD         (((uint32_t)0x01) << 11)
#define I2C_IEN_TXOF            (((uint32_t)0x01) << 12)
#define I2C_IEN_RXUF            (((uint32_t)0x01) << 13)
#define I2C_IEN_BITO            (((uint32_t)0x01) << 14)
#define I2C_IEN_CLTO            (((uint32_t)0x01) << 15)
#define I2C_IEN_SSTOP           (((uint32_t)0x01) << 16)
#define I2C_IEN_RXFULL          (((uint32_t)0x01) << 17)
#define I2C_IEN_CLERR           (((uint32_t)0x01) << 18)
#define I2C_IEN_SCLERR          (((uint32_t)0x01) << 19)
#define I2C_IEN_SDAERR          (((uint32_t)0x01) << 20)

#endif
