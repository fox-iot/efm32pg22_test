#ifndef _EFM32PG22_INT_H_
#define _EFM32PG22_INT_H_

#define CRYPTOACC_INT         0
#define TRNG_INT              1
#define PKE_INT               2
#define SMU_SECURE_INT        3
#define SMU_PRIVILEGED_INT    4
#define SMU_NS_PRIVILEGED_INT 5
#define EMU_INT               6
#define TIMER0_INT            7
#define TIMER1_INT            8
#define TIMER2_INT            9
#define TIMER3_INT            10
#define TIMER4_INT            11
#define RTCC_INT              12
#define USART0_RX_INT         13
#define USART0_TX_INT         14
#define USART1_RX_INT         15
#define USART1_TX_INT         16
#define ICACHE0_INT           17
#define BURTC_INT             18
#define LETIMER0_INT          19
#define SYSCFG_INT            20
#define LDMA_INT              21
#define LFXO_INT              22
#define LFRCO_INT             23
#define ULFRCO_INT            24
#define GPIO_ODD_INT          25
#define GPIO_EVEN_INT         26
#define I2C0_INT              27
#define I2C1_INT              28
#define EMUDG_INT             29
#define EMUSE_INT             30
#define WDOG0_INT             43
#define HFXO0_INT             44
#define HFRCO0_INT            45
#define CMU_INT               46
#define IADC_INT              48
#define MSC_INT               49
#define DPLL0_INT             50
#define PDM_INT               51
#define SW0_INT               52
#define SW1_INT               53
#define SW2_INT               54
#define SW3_INT               55
#define KERNEL0_INT           56
#define KERNEL1_INT           57
#define M33CTI0_INT           58
#define M33CTI1_INT           59
#define EMUEFP_INT            60
#define DCDC_INT              61
#define EUART0_RX_INT         62
#define EUART0_TX_INT         63

#endif

