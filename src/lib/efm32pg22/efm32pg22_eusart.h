#ifndef _EFM32PG22_EUART_H_
#define _EFM32PG22_EUART_H_

#include <stdint.h>

typedef struct{
        volatile uint32_t IPVERSION;
        volatile uint32_t EN;
        volatile uint32_t CFG0;
        volatile uint32_t CFG1;
        volatile uint32_t FRAMECFG;
        volatile uint32_t IRHFCFG;
        volatile uint32_t IRLFCFG;
        volatile uint32_t TIMINGCFG;
        volatile uint32_t STARTFRAMECFG;
        volatile uint32_t SIGFRAMECFG;
        volatile uint32_t CLKDIV;
        volatile uint32_t TRIGCTRL;
        volatile uint32_t CMD;
        volatile uint32_t RXDATA;
        volatile uint32_t RXDATAP;
        volatile uint32_t TXDATA;
        volatile uint32_t STATUS;
        volatile uint32_t IF;
        volatile uint32_t IEN;
        volatile uint32_t SYNCBUSY;
}__attribute__((packed)) EUSART_TypeDef;

#define EUART0                               ((EUSART_TypeDef *)0x4A030000)
#define EUART0_SET                           ((EUSART_TypeDef *)0x4A031000)
#define EUART0_CLR                           ((EUSART_TypeDef *)0x4A032000)
#define EUART0_TGL                           ((EUSART_TypeDef *)0x4A033000)

#define EUSART_IPVERSION_IPVERSION           (((uint32_t)0xFFFFFFFF) << 0)

#define EUSART_EN_EN                         (((uint32_t)0x01) << 0)

#define EUSART_CFG0_LOOPBK                   (((uint32_t)0x01) << 1)
#define EUSART_CFG0_CCEN                     (((uint32_t)0x01) << 2)
#define EUSART_CFG0_MPM                      (((uint32_t)0x01) << 3)
#define EUSART_CFG0_MPAB                     (((uint32_t)0x01) << 4)
#define EUSART_CFG0_OVS                      (((uint32_t)0x07) << 5)
#define EUSART_CFG0_MSBF                     (((uint32_t)0x01) << 10)
#define EUSART_CFG0_RXINV                    (((uint32_t)0x01) << 13)
#define EUSART_CFG0_TXINV                    (((uint32_t)0x01) << 14)
#define EUSART_CFG0_AUTOTRI                  (((uint32_t)0x01) << 17)
#define EUSART_CFG0_SKIPPERRF                (((uint32_t)0x01) << 20)
#define EUSART_CFG0_ERRSDMA                  (((uint32_t)0x01) << 22)
#define EUSART_CFG0_ERRSRX                   (((uint32_t)0x01) << 23)
#define EUSART_CFG0_ERRSTX                   (((uint32_t)0x01) << 24)
#define EUSART_CFG0_MVDIS                    (((uint32_t)0x01) << 30)
#define EUSART_CFG0_AUTOBAUDEN               (((uint32_t)0x01) << 31)

#define EUSART_CFG1_DBGHALT                  (((uint32_t)0x01) << 0)
#define EUSART_CFG1_CTSINV                   (((uint32_t)0x01) << 1)
#define EUSART_CFG1_CTSEN                    (((uint32_t)0x01) << 2)
#define EUSART_CFG1_RTSINV                   (((uint32_t)0x01) << 3)
#define EUSART_CFG1_TXDMAWU                  (((uint32_t)0x01) << 9)
#define EUSART_CFG1_RXDMAWU                  (((uint32_t)0x01) << 10)
#define EUSART_CFG1_SFUBRX                   (((uint32_t)0x01) << 11)
#define EUSART_CFG1_RXPRSEN                  (((uint32_t)0x01) << 15)
#define EUSART_CFG1_TXFIW                    (((uint32_t)0x03) << 16)
#define EUSART_CFG1_RXFIW                    (((uint32_t)0x03) << 19)
#define EUSART_CFG1_RTSRXFW                  (((uint32_t)0x03) << 22)

#define EUSART_FRAMECFG_DATABITS             (((uint32_t)0x03) << 0)
#define EUSART_FRAMECFG_PARITY               (((uint32_t)0x03) << 8)
#define EUSART_FRAMECFG_STOPBITS             (((uint32_t)0x03) << 12)

#define EUSART_IRHFCFG_IRHFEN                (((uint32_t)0x01) << 0)
#define EUSART_IRHFCFG_IRHFPW                (((uint32_t)0x03) << 1)
#define EUSART_IRHFCFG_IRHFFILT              (((uint32_t)0x01) << 3)

#define EUSART_IRLFCFG_IRLFEN                (((uint32_t)0x01) << 0)

#define EUSART_TIMINGCFG_TXDELAY             (((uint32_t)0x03) << 0)

#define EUSART_STARTFRAMECFG_STARTFRAME      (((uint32_t)0x01FF) << 0)

#define EUSART_SIGFRAMECFG_SIGFRAME          (((uint32_t)0x01FF) << 0)

#define EUSART_CLKDIV_DIV                    (((uint32_t)0x03FF) << 3)

#define EUSART_TRIGCTRL_RXTEN                (((uint32_t)0x01) << 0)
#define EUSART_TRIGCTRL_TXTEN                (((uint32_t)0x01) << 1)
 
#define EUSART_CMD_RXEN                      (((uint32_t)0x01) << 0)
#define EUSART_CMD_RXDIS                     (((uint32_t)0x01) << 1)
#define EUSART_CMD_TXEN                      (((uint32_t)0x01) << 2)
#define EUSART_CMD_TXDIS                     (((uint32_t)0x01) << 3)
#define EUSART_CMD_RXBLOCKEN                 (((uint32_t)0x01) << 4)
#define EUSART_CMD_RXBLOCKDIS                (((uint32_t)0x01) << 5)
#define EUSART_CMD_TXTRIEN                   (((uint32_t)0x01) << 6)
#define EUSART_CMD_TXTRIDIS                  (((uint32_t)0x01) << 7)
#define EUSART_CMD_CLEARTX                   (((uint32_t)0x01) << 8)

#define EUSART_RXDATA_RXDATA                 (((uint32_t)0x01FF) << 0)
#define EUSART_RXDATA_PERR                   (((uint32_t)0x01) << 9)
#define EUSART_RXDATA_FERR                   (((uint32_t)0x01) << 10)

#define EUSART_RXDATAP_RXDATAP               (((uint32_t)0x01FF) << 0)
#define EUSART_RXDATAP_PERRP                 (((uint32_t)0x01) << 9)
#define EUSART_RXDATAP_FERRP                 (((uint32_t)0x01) << 10)

#define EUSART_TXDATA_TXDATA                 (((uint32_t)0x01FF) << 0)
#define EUSART_TXDATA_UBRXAT                 (((uint32_t)0x01) << 9)
#define EUSART_TXDATA_TXTRIAT                (((uint32_t)0x01) << 10)
#define EUSART_TXDATA_TXBREAK                (((uint32_t)0x01) << 11)
#define EUSART_TXDATA_TXDISAT                (((uint32_t)0x01) << 12)
#define EUSART_TXDATA_RXENAT                 (((uint32_t)0x01) << 13)

#define EUSART_STATUS_RXENS                  (((uint32_t)0x01) << 0)
#define EUSART_STATUS_TXENS                  (((uint32_t)0x01) << 1)
#define EUSART_STATUS_RXBLOCK                (((uint32_t)0x01) << 3)
#define EUSART_STATUS_TXTRI                  (((uint32_t)0x01) << 4)
#define EUSART_STATUS_TXC                    (((uint32_t)0x01) << 5)
#define EUSART_STATUS_TXFL                   (((uint32_t)0x01) << 6)
#define EUSART_STATUS_RXFL                   (((uint32_t)0x01) << 7)
#define EUSART_STATUS_RXFULL                 (((uint32_t)0x01) << 8)
#define EUSART_STATUS_RXIDLE                 (((uint32_t)0x01) << 12)
#define EUSART_STATUS_TXIDLE                 (((uint32_t)0x01) << 13)
#define EUSART_STATUS_TXFCNT                 (((uint32_t)0x07) << 16)
#define EUSART_STATUS_CLEARTXBUSY            (((uint32_t)0x01) << 19)
#define EUSART_STATUS_AUTOBAUDDONE           (((uint32_t)0x01) << 24)

#define EUSART_IF_TXC                        (((uint32_t)0x01) << 0)
#define EUSART_IF_TXFL                       (((uint32_t)0x01) << 1)
#define EUSART_IF_RXFL                       (((uint32_t)0x01) << 2)
#define EUSART_IF_RXFULL                     (((uint32_t)0x01) << 3)
#define EUSART_IF_RXOF                       (((uint32_t)0x01) << 4)
#define EUSART_IF_RXUF                       (((uint32_t)0x01) << 5)
#define EUSART_IF_TXOF                       (((uint32_t)0x01) << 6)
#define EUSART_IF_PERR                       (((uint32_t)0x01) << 8)
#define EUSART_IF_FERR                       (((uint32_t)0x01) << 9)
#define EUSART_IF_MPAF                       (((uint32_t)0x01) << 10)
#define EUSART_IF_CCF                        (((uint32_t)0x01) << 12)
#define EUSART_IF_TXIDLE                     (((uint32_t)0x01) << 13)
#define EUSART_IF_STARTF                     (((uint32_t)0x01) << 18)
#define EUSART_IF_SIGF                       (((uint32_t)0x01) << 19)
#define EUSART_IF_AUTOBAUDDONE               (((uint32_t)0x01) << 24)

#define EUSART_IEN_TXC                       (((uint32_t)0x01) << 0)
#define EUSART_IEN_TXFL                      (((uint32_t)0x01) << 1)
#define EUSART_IEN_RXFL                      (((uint32_t)0x01) << 2)
#define EUSART_IEN_RXFULL                    (((uint32_t)0x01) << 3)
#define EUSART_IEN_RXOF                      (((uint32_t)0x01) << 4)
#define EUSART_IEN_RXUF                      (((uint32_t)0x01) << 5)
#define EUSART_IEN_TXOF                      (((uint32_t)0x01) << 6)
#define EUSART_IEN_PERR                      (((uint32_t)0x01) << 8)
#define EUSART_IEN_FERR                      (((uint32_t)0x01) << 9)
#define EUSART_IEN_MPAF                      (((uint32_t)0x01) << 10)
#define EUSART_IEN_CCF                       (((uint32_t)0x01) << 12)
#define EUSART_IEN_TXIDLE                    (((uint32_t)0x01) << 13)
#define EUSART_IEN_STARTF                    (((uint32_t)0x01) << 18)
#define EUSART_IEN_SIGF                      (((uint32_t)0x01) << 19)
#define EUSART_IEN_AUTOBAUDDONE              (((uint32_t)0x01) << 24)
 
#define EUSART_SYNCBUSY_DIV                  (((uint32_t)0x01) << 0)
#define EUSART_SYNCBUSY_RXTEN                (((uint32_t)0x01) << 1)
#define EUSART_SYNCBUSY_TXTEN                (((uint32_t)0x01) << 2)
#define EUSART_SYNCBUSY_RXEN                 (((uint32_t)0x01) << 3)
#define EUSART_SYNCBUSY_RXDIS                (((uint32_t)0x01) << 4)
#define EUSART_SYNCBUSY_TXEN                 (((uint32_t)0x01) << 5)
#define EUSART_SYNCBUSY_TXDIS                (((uint32_t)0x01) << 6)
#define EUSART_SYNCBUSY_RXBLOCKEN            (((uint32_t)0x01) << 7)
#define EUSART_SYNCBUSY_RXBLOCKDIS           (((uint32_t)0x01) << 8) 
#define EUSART_SYNCBUSY_TXTRIEN              (((uint32_t)0x01) << 9)
#define EUSART_SYNCBUSY_TXTRIDIS             (((uint32_t)0x01) << 10)

#endif
