#ifndef _EFM32PG22_LETTIMER_H_
#define _EFM32PG22_LETTIMER_H_

#include <stdint.h>

typedef struct{
	volatile uint32_t IPVERSION;
	volatile uint32_t EN;
	volatile uint32_t CTRL;
	volatile uint32_t CMD;
	volatile uint32_t STATUS;
	uint32_t RESERVED1[1];
	volatile uint32_t CNT;
	volatile uint32_t COMP0;
	volatile uint32_t COMP1;
	volatile uint32_t TOP;
	volatile uint32_t TOPBUFF;
	volatile uint32_t REP0;
	volatile uint32_t REP1;
	volatile uint32_t IF;
	volatile uint32_t IEN;
	uint32_t RESERVED2[1];
	volatile uint32_t SYNCBUSY;
	uint32_t RESERVED3[3];
	volatile uint32_t PRSMODE;
}__attribute__((packed)) LETTIMER_TypeDef;

#define LETIMER0                       ((LETTIMER_TypeDef *)0x4A000000)
#define LETIMER0_SET                   ((LETTIMER_TypeDef *)0x4A001000)
#define LETIMER0_CLR                   ((LETTIMER_TypeDef *)0x4A002000)
#define LETIMER0_TGL                   ((LETTIMER_TypeDef *)0x4A003000)

#define LETIMER_IPVERSION_IPVERSION    (((uint32_t)0xFFFFFFFF) << 0)

#define LETIMER_EN_EN                  (((uint32_t)0x01) << 0)

#define LETIMER_CTRL_REPMODE           (((uint32_t)0x03) << 0)
#define LETIMER_CTRL_UFOA0             (((uint32_t)0x03) << 2)
#define LETIMER_CTRL_UFOA1             (((uint32_t)0x03) << 4)
#define LETIMER_CTRL_OPOL0             (((uint32_t)0x01) << 6)
#define LETIMER_CTRL_OPOL1             (((uint32_t)0x01) << 7)
#define LETIMER_CTRL_BUFTOP            (((uint32_t)0x01) << 8)
#define LETIMER_CTRL_CNTTOPEN          (((uint32_t)0x01) << 9)
#define LETIMER_CTRL_DEBUGRUN          (((uint32_t)0x01) << 12)
#define LETIMER_CTRL_CNTPRESC          (((uint32_t)0x0F) << 16)

#define LETIMER_CMD_START              (((uint32_t)0x01) << 0)
#define LETIMER_CMD_STOP               (((uint32_t)0x01) << 1)
#define LETIMER_CMD_CLEAR              (((uint32_t)0x01) << 2)
#define LETIMER_CMD_CTO0               (((uint32_t)0x01) << 3)
#define LETIMER_CMD_CTO1               (((uint32_t)0x01) << 4)

#define LETIMER_STATUS_RUNNING         (((uint32_t)0x01) << 0)

#define LETIMER_CNT_CNT                (((uint32_t)0xFFFFFF) << 0)

#define LETIMER_COMP0_COMP0            (((uint32_t)0xFFFFFF) << 0)

#define LETIMER_COMP1_COMP1            (((uint32_t)0xFFFFFF) << 0)

#define LETIMER_TOP_TOP                (((uint32_t)0xFFFFFF) << 0)

#define LETIMER_TOPBUFF_TOPBUFF        (((uint32_t)0xFFFFFF) << 0)

#define LETIMER_REP0_REP0              (((uint32_t)0xFF) << 0)

#define LETIMER_REP1_REP1              (((uint32_t)0xFF) << 0)

#define LETIMER_IF_COMP0               (((uint32_t)0x01) << 0)
#define LETIMER_IF_COMP1               (((uint32_t)0x01) << 1)
#define LETIMER_IF_UF                  (((uint32_t)0x01) << 2)
#define LETIMER_IF_REP0                (((uint32_t)0x01) << 3)
#define LETIMER_IF_REP1                (((uint32_t)0x01) << 4)

#define LETIMER_IEN_COMP0              (((uint32_t)0x01) << 0)
#define LETIMER_IEN_COMP1              (((uint32_t)0x01) << 1)
#define LETIMER_IEN_UF                 (((uint32_t)0x01) << 2)
#define LETIMER_IEN_REP0               (((uint32_t)0x01) << 3)
#define LETIMER_IEN_REP1               (((uint32_t)0x01) << 4)

#define LETIMER_SYNCBUSY_CNT           (((uint32_t)0x01) << 0)
#define LETIMER_SYNCBUSY_TOP           (((uint32_t)0x01) << 2)
#define LETIMER_SYNCBUSY_REP0          (((uint32_t)0x01) << 3)
#define LETIMER_SYNCBUSY_REP1          (((uint32_t)0x01) << 4)
#define LETIMER_SYNCBUSY_START         (((uint32_t)0x01) << 5)
#define LETIMER_SYNCBUSY_STOP          (((uint32_t)0x01) << 6)
#define LETIMER_SYNCBUSY_CLEAR         (((uint32_t)0x01) << 7)
#define LETIMER_SYNCBUSY_CTO0          (((uint32_t)0x01) << 8)
#define LETIMER_SYNCBUSY_CTO1          (((uint32_t)0x01) << 9)

#define LETIMER_PRSMODE_PRSSTARTMODE   (((uint32_t)0x03) << 18)
#define LETIMER_PRSMODE_PRSSTOPMODE    (((uint32_t)0x03) << 22)
#define LETIMER_PRSMODE_PRSCLEARMODE   (((uint32_t)0x03) << 26)

#endif
