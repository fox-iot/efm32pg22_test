#ifndef _EFM32PG22_RTCC_H_
#define _EFM32PG22_RTCC_H_

#include <stdint.h>

typedef struct {
	volatile uint32_t CTRL;
	volatile uint32_t OCVALUE;
	volatile uint32_t ICVALUE;
}__attribute__((packed)) RTCC_CC_TypeDef;

typedef struct{
	volatile uint32_t IPVERSION;
	volatile uint32_t EN;
	volatile uint32_t CFG;
	volatile uint32_t CMD;
	volatile uint32_t STATUS;
	volatile uint32_t IF;
	volatile uint32_t IEN;
	volatile uint32_t PRECNT;
	volatile uint32_t CNT;
	volatile uint32_t COMBCNT;
	volatile uint32_t SYNCBUSY;
	volatile uint32_t LOCK;
	RTCC_CC_TypeDef CC[3];
}__attribute__((packed)) RTCC_TypeDef;

#define RTCC                           ((RTCC_TypeDef *)0x48000000)
#define RTCC_SET                       ((RTCC_TypeDef *)0x48001000)
#define RTTC_CLR                       ((RTCC_TypeDef *)0x48002000)
#define RTRTC_TGL                      ((RTCC_TypeDef *)0x48030000)

#define RTCC_IPVERSION_IPVERSION       (((uint32_t)0xFFFFFFFF) << 0)

#define RTCC_EN_EN                     (((uint32_t)0x01) << 0)

#define RTCC_CFG_DEBUGRUN              (((uint32_t)0x01) << 0)
#define RTCC_CFG_PRECNTCCV0TOP         (((uint32_t)0x01) << 1)
#define RTCC_CFG_CNTCCV1TOP            (((uint32_t)0x01) << 2)
#define RTCC_CFG_CNTTICK               (((uint32_t)0x01) << 3)
#define RTCC_CFG_CNTPRESC              (((uint32_t)0x0F) << 4)

#define RTCC_CMD_START                 (((uint32_t)0x01) << 0)
#define RTCC_CMD_STOP                  (((uint32_t)0x01) << 1)

#define RTCC_STATUS_RUNNING            (((uint32_t)0x01) << 0)
#define RTCC_STATUS_RTCCLOCKSTATUS     (((uint32_t)0x01) << 1)

#define RTCC_IF_OF                     (((uint32_t)0x01) << 0)
#define RTCC_IF_CNTTICK                (((uint32_t)0x01) << 1)
#define RTCC_IF_CC0                    (((uint32_t)0x01) << 4)
#define RTCC_IF_CC1                    (((uint32_t)0x01) << 6)
#define RTCC_IF_CC2                    (((uint32_t)0x01) << 8)

#define RTCC_IEN_OF                    (((uint32_t)0x01) << 0)
#define RTCC_IEN_CNTTICK               (((uint32_t)0x01) << 1)
#define RTCC_IEN_CC0                   (((uint32_t)0x01) << 4)
#define RTCC_IEN_CC1                   (((uint32_t)0x01) << 6)
#define RTCC_IEN_CC2                   (((uint32_t)0x01) << 8)

#define RTCC_PRECNT_PRECNT             (((uint32_t)0x7FFF) << 0)

#define RTCC_CNT_CNT                   (((uint32_t)0xFFFFFFFF) << 0)

#define RTCC_COMBCNT_PRECNT            (((uint32_t)0x7FFF) << 0)
#define RTCC_COMBCNT_CNTLSB            (((uint32_t)0x01FFFF) << 15)

#define RTCC_SYNCBUSY_START            (((uint32_t)0x01) << 0)
#define RTCC_SYNCBUSY_STOP             (((uint32_t)0x01) << 1)
#define RTCC_SYNCBUSY_PRECNT           (((uint32_t)0x01) << 2)
#define RTCC_SYNCBUSY_CNT              (((uint32_t)0x01) << 3)

#define RTCC_LOCK_LOCKKEY              (((uint32_t)0xFFFF) << 0)

#define RTCC_CTRL_MODE             (((uint32_t)0x03) << 0)
#define RTCC_CTRL_CMOA             (((uint32_t)0x03) << 2)
#define RTCC_CTRL_COMPBASE         (((uint32_t)0x01) << 4)
#define RTCC_CTRL_ICEDGE           (((uint32_t)0x03) << 5)

#define RTCC_OCVALUE_OC            (((uint32_t)0xFFFFFFFF) << 0)

#define RTCC_ICVALUE_IC            (((uint32_t)0xFFFFFFFF) << 0)

#endif
