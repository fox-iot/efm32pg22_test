#ifndef _EFM32PG22_DEVINFO_H_
#define _EFM32PG22_DEVINFO_H_

#include <stdint.h>

typedef struct{
	volatile uint32_t INFO;
	volatile uint32_t PART;
	volatile uint32_t MEMINFO;
	volatile uint32_t MSIZE;
	volatile uint32_t PKGINFO;
	volatile uint32_t CUSTOMINFO;
	volatile uint32_t SWFIX;
	volatile uint32_t SWCAPA0;
	volatile uint32_t SWCAPA1;
	uint32_t RESERVED1[1];
	volatile uint32_t EXTINFO;
	uint32_t RESERVED2[5];
	volatile uint32_t EUI48L;
	volatile uint32_t EUI48H;
	volatile uint32_t EUI64L;
	volatile uint32_t EUI64H;
	volatile uint32_t CALTEMP;
	volatile uint32_t EMUTEMP;
	volatile uint32_t HFRCODPLLCAL[18];
	volatile uint32_t HFRCOEM23CAL[18];
	volatile uint32_t HFRCOSECAL[18];
	volatile uint32_t MODULENAME0;
	volatile uint32_t MODULENAME1;
	volatile uint32_t MODULENAME2;
	volatile uint32_t MODULENAME3;
	volatile uint32_t MODULENAME4;
	volatile uint32_t MODULENAME5;
	volatile uint32_t MODULENAME6;
	volatile uint32_t MODULEINFO;
	volatile uint32_t MODXOCAL;
	uint32_t RESERVED4[11];
	volatile uint32_t IADC0GAIN0;
	volatile uint32_t IADC0GAIN1;
	volatile uint32_t IADC0OFFSETCAL0;
	volatile uint32_t IADC0NORMALOFFSETCAL0;
	volatile uint32_t IADC0NORMALOFFSETCAL1;
	volatile uint32_t IADC0HISPDOFFSETCAL0;
	volatile uint32_t IADC0HISPDOFFSETCAL1;
	uint32_t RESERVED5[24];
	volatile uint32_t LEGACY;
	uint32_t RESERVED6[23];
	volatile uint32_t RTHERM;
}__attribute__((packed)) DEVINFO_TypeDef;

#define DEVINFO                        ((DEVINFO_TypeDef *)0x0FE08000)

#define DEVINFO_INFO_CRC               (((uint32_t)0xFFFF) << 0)
#define DEVINFO_INFO_PRODREV           (((uint32_t)0xFF) << 16)
#define DEVINFO_INFO_DEVINFOREV        (((uint32_t)0xFF) << 24)

#define DEVINFO_PART_DEVICENUM         (((uint32_t)0xFFFF) << 0)
#define DEVINFO_PART_FAMILYNUM         (((uint32_t)0x3F) << 16)
#define DEVINFO_PART_FAMILY            (((uint32_t)0x3F) << 24)

#define DEVINFO_MEMINFO_FLASHPAGESIZE  (((uint32_t)0xFF) << 0)
#define DEVINFO_MEMINFO_UDPAGESIZE     (((uint32_t)0xFF) << 8)
#define DEVINFO_MEMINFO_DILEN          (((uint32_t)0xFFFF) << 16)

#define DEVINFO_MSIZE_FLASH            (((uint32_t)0xFFFF) << 0)
#define DEVINFO_MSIZE_SRAM             (((uint32_t)0x07FF) << 16)

#define DEVINFO_PKGINFO_TEMPGRADE      (((uint32_t)0xFF) << 0)
#define DEVINFO_PKGINFO_PKGTYPE        (((uint32_t)0xFF) << 8)
#define DEVINFO_PKGINFO_PINCOUNT       (((uint32_t)0xFF) << 16)

#define DEVINFO_CUSTOMINFO_PARTNO      (((uint32_t)0xFFFFFFFF) << 16)

#define DEVINFO_SWFIX_RSV              (((uint32_t)0xFFFFFFFF) << 0)

#define DEVINFO_SWCAPA0_ZIGBEE         (((uint32_t)0x03) << 0)
#define DEVINFO_SWCAPA0_THREAD         (((uint32_t)0x03) << 4)
#define DEVINFO_SWCAPA0_RF4CE          (((uint32_t)0x03) << 8)
#define DEVINFO_SWCAPA0_BTSMART        (((uint32_t)0x03) << 12)
#define DEVINFO_SWCAPA0_CONNECT        (((uint32_t)0x03) << 16)
#define DEVINFO_SWCAPA0_SRI            (((uint32_t)0x03) << 20)

#define DEVINFO_SWCAPA1_RFMCUEN        (((uint32_t)0x01) << 0)
#define DEVINFO_SWCAPA1_NCPEN          (((uint32_t)0x01) << 1)
#define DEVINFO_SWCAPA1_GWEN           (((uint32_t)0x01) << 2)

#define DEVINFO_EXTINFO_TYPE           (((uint32_t)0xFF) << 0)
#define DEVINFO_EXTINFO_CONNECTION     (((uint32_t)0xFF) << 8)
#define DEVINFO_EXTINFO_REV            (((uint32_t)0xFF) << 16)

#define DEVINFO_EUI48L_UNIQUEID        (((uint32_t)0xFFFFFF) << 0)
#define DEVINFO_EUI48L_OUI48L          (((uint32_t)0xFF) << 24)

#define DEVINFO_EUI48H_OUI48H          (((uint32_t)0xFFFF) << 0)
#define DEVINFO_EUI48H_RESERVED        (((uint32_t)0xFFFFFFFF) << 16)

#define DEVINFO_EUI64L_UNIQUEL         (((uint32_t)0xFFFFFFFF) << 0)

#define DEVINFO_EUI64H_UNIQUEH         (((uint32_t)0xFF) << 0)
#define DEVINFO_EUI64H_OUI64           (((uint32_t)0xFFFFFFFF) << 8)

#define DEVINFO_CALTEMP_TEMP           (((uint32_t)0xFF) << 0)

#define DEVINFO_EMUTEMP_EMUTEMPROOM    (((uint32_t)0x01FF) << 2)

#define DEVINFO_HFRCODPLLCAL_TUNING    (((uint32_t)0x7F) << 0)
#define DEVINFO_HFRCODPLLCAL_FINETUNING (((uint32_t)0x3F) << 8)
#define DEVINFO_HFRCODPLLCAL_LDOHP     (((uint32_t)0x01) << 15)
#define DEVINFO_HFRCODPLLCAL_FREQRANGE (((uint32_t)0x1F) << 16)
#define DEVINFO_HFRCODPLLCAL_CMPBIAS   (((uint32_t)0x07) << 21)
#define DEVINFO_HFRCODPLLCAL_CLKDIV    (((uint32_t)0x03) << 24)
#define DEVINFO_HFRCODPLLCAL_CMPSEL    (((uint32_t)0x03) << 26)
#define DEVINFO_HFRCODPLLCAL_IREFTC    (((uint32_t)0x0F) << 28)

#define DEVINFO_MODULENAME0_MODCHAR1   (((uint32_t)0xFF) << 0)
#define DEVINFO_MODULENAME0_MODCHAR2   (((uint32_t)0xFF) << 8)
#define DEVINFO_MODULENAME0_MODCHAR3   (((uint32_t)0xFF) << 16)
#define DEVINFO_MODULENAME0_MODCHAR4   (((uint32_t)0xFF) << 24)

#define DEVINFO_MODULENAME1_MODCHAR5   (((uint32_t)0xFF) << 0)
#define DEVINFO_MODULENAME1_MODCHAR6   (((uint32_t)0xFF) << 8)
#define DEVINFO_MODULENAME1_MODCHAR7   (((uint32_t)0xFF) << 16)
#define DEVINFO_MODULENAME1_MODCHAR8   (((uint32_t)0xFF) << 24)

#define DEVINFO_MODULENAME2_MODCHAR9   (((uint32_t)0xFF) << 0)
#define DEVINFO_MODULENAME2_MODCHAR10  (((uint32_t)0xFF) << 8)
#define DEVINFO_MODULENAME2_MODCHAR11  (((uint32_t)0xFF) << 16)
#define DEVINFO_MODULENAME2_MODCHAR12  (((uint32_t)0xFF) << 24)

#define DEVINFO_MODULENAME3_MODCHAR13  (((uint32_t)0xFF) << 0)
#define DEVINFO_MODULENAME3_MODCHAR14  (((uint32_t)0xFF) << 8)
#define DEVINFO_MODULENAME3_MODCHAR15  (((uint32_t)0xFF) << 16)
#define DEVINFO_MODULENAME3_MODCHAR16  (((uint32_t)0xFF) << 24)

#define DEVINFO_MODULENAME4_MODCHAR17  (((uint32_t)0xFF) << 0)
#define DEVINFO_MODULENAME4_MODCHAR18  (((uint32_t)0xFF) << 8)
#define DEVINFO_MODULENAME4_MODCHAR19  (((uint32_t)0xFF) << 16)
#define DEVINFO_MODULENAME4_MODCHAR20  (((uint32_t)0xFF) << 24)

#define DEVINFO_MODULENAME5_MODCHAR21  (((uint32_t)0xFF) << 0)
#define DEVINFO_MODULENAME5_MODCHAR22  (((uint32_t)0xFF) << 8)
#define DEVINFO_MODULENAME5_MODCHAR23  (((uint32_t)0xFF) << 16)
#define DEVINFO_MODULENAME5_MODCHAR24  (((uint32_t)0xFF) << 24)

#define DEVINFO_MODULENAME6_MODCHAR25  (((uint32_t)0xFF) << 0)
#define DEVINFO_MODULENAME6_MODCHAR26  (((uint32_t)0xFF) << 8)
#define DEVINFO_MODULENAME6_RSV        (((uint32_t)0xFFFF) << 16)

#define DEVINFO_MODULEINFO_HWREV       (((uint32_t)0x1F) << 0)
#define DEVINFO_MODULEINFO_ANTENNA     (((uint32_t)0x07) << 5)
#define DEVINFO_MODULEINFO_MODNUMBER   (((uint32_t)0x7F) << 8)
#define DEVINFO_MODULEINFO_TYPE        (((uint32_t)0x01) << 15)
#define DEVINFO_MODULEINFO_LFXO        (((uint32_t)0x01) << 16)
#define DEVINFO_MODULEINFO_EXPRESS     (((uint32_t)0x01) << 17)
#define DEVINFO_MODULEINFO_LFXOCALVAL  (((uint32_t)0x01) << 18)
#define DEVINFO_MODULEINFO_HFXOCALVAL  (((uint32_t)0x01) << 19)
#define DEVINFO_MODULEINFO_MODNUMBERMSB (((uint32_t)0x01FF) << 20)
#define DEVINFO_MODULEINFO_PADCDC      (((uint32_t)0x01) << 29)
#define DEVINFO_MODULEINFO_PHYLIMITED  (((uint32_t)0x01) << 30)
#define DEVINFO_MODULEINFO_EXTVALID    (((uint32_t)0x01) << 31)

#define DEVINFO_MODXOCAL_HFXOCTUNEXIANA (((uint32_t)0xFF) << 0)
#define DEVINFO_MODXOCAL_HFXOCTUNEXOANA (((uint32_t)0xFF) << 8)
#define DEVINFO_MODXOCAL_LFXOCAPTUNE   (((uint32_t)0x7F) << 16)

#define DEVINFO_IADC0GAIN0_GAINCANA1   (((uint32_t)0xFFFF) << 0)
#define DEVINFO_IADC0GAIN0_GAINCANA2   (((uint32_t)0xFFFF) << 16)

#define DEVINFO_IADC0_GAINCANA3        (((uint32_t)0xFFFF) << 0)
#define DEVINFO_IADC0_GAINCANA4        (((uint32_t)0xFFFF) << 16)

#define DEVINFO_IADC0OFFSETCAL0_OFFSETANABASE (((uint32_t)0xFFFF) << 0)
#define DEVINFO_IADC0OFFSETCAL0_OFFSETANA1HIACC (((uint32_t)0xFFFF) << 16)

#define DEVINFO_IADC0NORMALOFFSETCAL0_OFFSETANA1NORM (((uint32_t)0xFFFF) << 0)
#define DEVINFO_IADC0NORMALOFFSETCAL0_OFFSETANA2NORM (((uint32_t)0xFFFF) << 16)

#define DEVINFO_IADC0NORMALOFFSETCAL1_OFFSETANA3NORM (((uint32_t)0xFFFF) << 0)

#define DEVINFO_IADC0HISPDOFFSETCAL0_OFFSETANA1HISPD (((uint32_t)0xFFFF) << 0)
#define DEVINFO_IADC0HISPDOFFSETCAL0_OFFSETANA2HISPD (((uint32_t)0xFFFF) << 16)

#define DEVINFO_IADC0HISPDOFFSETCAL1_OFFSETANA3HISPD (((uint32_t)0xFFFF) << 0)

#define DEVINFO_LEGACY_DEVICEFAMILY    (((uint32_t)0xFF) << 16)

#define DEVINFO_RTHERM_RTHERM          (((uint32_t)0xFFFF) << 0)

#endif
