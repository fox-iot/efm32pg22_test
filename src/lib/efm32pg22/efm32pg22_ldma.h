#ifndef _EFM32PG22_LDMA_H_
#define _EFM32PG22_LDMA_H_
#include <stdint.h>

typedef struct {
	uint32_t RESERVED1[1];
	volatile uint32_t CFG;
	volatile uint32_t LOOP;
	volatile uint32_t CTRL;
	volatile uint32_t SRC;
	volatile uint32_t DST;
	volatile uint32_t LINK;
	uint32_t RESERVED2[5];
}__attribute__((packed)) LDMA_CH_TypedDef;

typedef struct{
        volatile uint32_t IPVERSION;
        volatile uint32_t EN;
        volatile uint32_t CTRL;
        volatile uint32_t STATUS;
        volatile uint32_t SYNCSWSET;
        volatile uint32_t SYNCSWCLR;
        volatile uint32_t SYNCHWEN;
        volatile uint32_t SYNCHWSEL;
        volatile uint32_t SYNCSTATUS;
        volatile uint32_t CHEN;
        volatile uint32_t CHDIS;
        volatile uint32_t CHSTATUS;
        volatile uint32_t CHBUSY;
        volatile uint32_t CHDONE;
        volatile uint32_t DBGHALT;
        volatile uint32_t SWREQ;
        volatile uint32_t REQDIS;
        volatile uint32_t REQPEND;
        volatile uint32_t LINKLOAD;
        volatile uint32_t REQCLEAR;
        volatile uint32_t IF;
        volatile uint32_t IEN;
        uint32_t RESERVED1[1];
	LDMA_CH_TypedDef CH[8];
}__attribute__((packed)) LDMA_TypeDef;

#define LDMA                       ((LDMA_TypeDef *)0x40040000)
#define LDMA_SET                   ((LDMA_TypeDef *)0x40041000)
#define LDMA_CLR                   ((LDMA_TypeDef *)0x40042000)
#define LDMA_TGL                   ((LDMA_TypeDef *)0x40043000)

#define LDMA_IPVERSION_IPVERSION   (((uint32_t)0xFF) << 0)

#define LDMA_EN_EN                 (((uint32_t)0x01) << 0) 

#define LDMA_CTRL_NUMFIXED         (((uint32_t)0x1F) << 24)
#define LDMA_CTRL_CORERST          (((uint32_t)0x01) << 31)

#define LDMA_STATUS_ANYBUSY        (((uint32_t)0x01) << 0)
#define LDMA_STATUS_ANYREQ         (((uint32_t)0x01) << 1)
#define LDMA_STATUS_CHGRANT        (((uint32_t)0x1F) << 3)
#define LDMA_STATUS_CHERROR        (((uint32_t)0x1F) << 8)
#define LDMA_STATUS_FIFOLEVEL      (((uint32_t)0x1F) << 16)
#define LDMA_STATUS_CHNUM          (((uint32_t)0x1F) << 24)

#define LDMA_SYNCSWSET_SYNCSWSET   (((uint32_t)0xFF) << 0)

#define LDMA_SYNCSWCLR_SYNCSWCLR   (((uint32_t)0xFF) << 0)
 
#define LDMA_SYNCHWEN_SYNCSETEN    (((uint32_t)0xFF) << 0)
#define LDMA_SYNCHWEN_SYNCCLREN    (((uint32_t)0xFF) << 16)

#define LDMA_SYNCHWSEL_SYNCSETEDGE (((uint32_t)0x0FF) << 0)
#define LDMA_SYNCHWSEL_SYNCCLREDGE (((uint32_t)0xFF) << 16)

#define LDMA_SYNCSTATUS_SYNCTRIG   (((uint32_t)0xFF) << 0)

#define LDMA_CHEN_CHEN             (((uint32_t)0xFF) << 0)

#define LDMA_CHDIS_CHDIS           (((uint32_t)0xFF) << 0)

#define LDMA_CHSTATUS_CHSTATUS     (((uint32_t)0xFF) << 0)

#define LDMA_CHBUSY_BUSY           (((uint32_t)0xFF) << 0)

#define LDMA_CHDONE_CHDONE0        (((uint32_t)0x01) << 0)
#define LDMA_CHDONE_CHDONE1        (((uint32_t)0x01) << 1)
#define LDMA_CHDONE_CHDONE2        (((uint32_t)0x01) << 2)
#define LDMA_CHDONE_CHDONE3        (((uint32_t)0x01) << 3)
#define LDMA_CHDONE_CHDONE4        (((uint32_t)0x01) << 4)
#define LDMA_CHDONE_CHDONE5        (((uint32_t)0x01) << 5)
#define LDMA_CHDONE_CHDONE6        (((uint32_t)0x01) << 6)
#define LDMA_CHDONE_CHDONE7        (((uint32_t)0x01) << 7)

#define LDMA_DBGHALT_DBGHALT       (((uint32_t)0xFF) << 0)

#define LDMA_SWREQ_SWREQ           (((uint32_t)0xFF) << 0)

#define LDMA_REQDIS_REQDIS         (((uint32_t)0xFF) << 0)

#define LDMA_REQPEND_REQPEND       (((uint32_t)0xFF) << 0)

#define LDMA_LINKLOAD_LINKLOAD     (((uint32_t)0xFF) << 0)

#define LDMA_REQCLEAR_REQCLEAR     (((uint32_t)0xFF) << 0)

#define LDMA_IF_DONE0              (((uint32_t)0x01) << 0)
#define LDMA_IF_DONE1              (((uint32_t)0x01) << 1)
#define LDMA_IF_DONE2              (((uint32_t)0x01) << 2)
#define LDMA_IF_DONE3              (((uint32_t)0x01) << 3)
#define LDMA_IF_DONE4              (((uint32_t)0x01) << 4)
#define LDMA_IF_DONE5              (((uint32_t)0x01) << 5)
#define LDMA_IF_DONE6              (((uint32_t)0x01) << 6)
#define LDMA_IF_DONE7              (((uint32_t)0x01) << 7)
#define LDMA_IF_ERROR              (((uint32_t)0x01) << 31)

#define LDMA_IEN_CHDONE            (((uint32_t)0xFF) << 0)
#define LDMA_IEN_ERROR             (((uint32_t)0x01) << 31)

#define LDMA_CH_CFG_ARBSLOTS      (((uint32_t)0x03) << 16)
#define LDMA_CH_CFG_SRCINCSIGN    (((uint32_t)0x01) << 20)
#define LDMA_CH_CFG_DSTINCSIGN    (((uint32_t)0x01) << 21)

#define LDMA_CH_LOOP_LOOPCNT      (((uint32_t)0xFF) << 0)
 
#define LDMA_CH_CTRL_STRUCTTYPE   (((uint32_t)0x03) << 0)
#define LDMA_CH_CTRL_STRUCTREQ    (((uint32_t)0x01) << 3)
#define LDMA_CH_CTRL_XFERCNT      (((uint32_t)0x07FF) << 4)
#define LDMA_CH_CTRL_BYTESWAP     (((uint32_t)0x01) << 15)
#define LDMA_CH_CTRL_BLOCKSIZE    (((uint32_t)0x0F) << 16)
#define LDMA_CH_CTRL_DONEIEN      (((uint32_t)0x0F) << 20)
#define LDMA_CH_CTRL_REQMODE      (((uint32_t)0x01) << 21)
#define LDMA_CH_CTRL_DECLOOPCNT   (((uint32_t)0x01) << 22)
#define LDMA_CH_CTRL_IGNORESREQ   (((uint32_t)0x01) << 23)
#define LDMA_CH_CTRL_SRCINC       (((uint32_t)0x03) << 24)
#define LDMA_CH_CTRL_SIZE         (((uint32_t)0x03) << 26)
#define LDMA_CH_CTRL_DSTINC       (((uint32_t)0x03) << 28)
#define LDMA_CH_CTRL_SRCMODE      (((uint32_t)0x01) << 30)
#define LDMA_CH_CTRL_DSTMODE      (((uint32_t)0x01) << 31)

#define LDMA_CH_SRC_SRCADDR       (((uint32_t)0xFFFFFFFF) << 0)

#define LDMA_CH_DST_DSTADDR       (((uint32_t)0xFFFFFFFF) << 0) 

#define LDMA_CH_LINK_LINKMODE     (((uint32_t)0x01) << 0)
#define LDMA_CH_LINK_LINK         (((uint32_t)0x01) << 1)
#define LDMA_CH_LINK_LINKADDR     (((uint32_t)0x3FFFFFFF) < 2)

#endif
