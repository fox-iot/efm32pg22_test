
#ifndef _EFM32PG22_SYSCFG_H_
#define _EFM32PG22_SYSCFG_H_

#include <stdint.h>

/* NB !!! SYSCFG_CFGNS Register and bits Declaration is missing */

typedef struct{
	volatile uint32_t IF;
	volatile uint32_t IEN;
	uint32_t RESERVED1[2];
	volatile uint32_t CHIPREVHW;
	volatile uint32_t CHIPREV;
	uint32_t RESERVED2[2];
	volatile uint32_t CFGSYSTIC;
	uint32_t RESERVED3[119];
	volatile uint32_t CTRL;
	uint32_t RESERVED4[1];
	volatile uint32_t DMEM0RETNCTRL;
	uint32_t RESERVED5[1];
	volatile uint32_t DMEM0ECCADDR;
	volatile uint32_t DMEM0ECCCTRL;
	uint32_t RESERVED6[250];
	volatile uint32_t ROOTDATA0;
	volatile uint32_t ROOTDATA1;
	volatile uint32_t ROOTLOCKSTATUS;
}__attribute__((packed)) SYSCFG_TypeDef;

typedef struct {
        uint32_t UNKNOWN;
}__attribute__((packed)) SYSCFG_CFGNS_TypeDef;

#define SYSCFG                                  ((SYSCFG_TypeDef *)0x4007C000)
#define SYSCFG_SET                              ((SYSCFG_TypeDef *)0x4007D000)
#define SYSCFG_CLR                              ((SYSCFG_TypeDef *)0x4007E000)
#define SYSCFG_TGL                              ((SYSCFG_TypeDef *)0x4007F000)

#define SYSCFG_CFGNS                            ((SYSCFG_CFGNS_TypeDef)0x40078000)

#define SYSCFG_IF_SW0                           (((uint32_t)0x01) << 0)
#define SYSCFG_IF_SW1                           (((uint32_t)0x01) << 1)
#define SYSCFG_IF_SW2                           (((uint32_t)0x01) << 2)
#define SYSCFG_IF_SW3                           (((uint32_t)0x01) << 3)
#define SYSCFG_IF_RAMERR1B                      (((uint32_t)0x01) << 16)
#define SYSCFG_IF_RAMERR2B                      (((uint32_t)0x01) << 17)

#define SYSCFG_IF_SW0                           (((uint32_t)0x01) << 0)
#define SYSCFG_IF_SW1                           (((uint32_t)0x01) << 1)
#define SYSCFG_IF_SW2                           (((uint32_t)0x01) << 2)
#define SYSCFG_IF_SW3                           (((uint32_t)0x01) << 3)
#define SYSCFG_IF_RAMERR1B                      (((uint32_t)0x01) << 16)
#define SYSCFG_IF_RAMERR2B                      (((uint32_t)0x01) << 17)

#define SYSCFG_CHIPREVHW_MAJOR                  (((uint32_t)0x3F) << 0)
#define SYSCFG_CHIPREVHW_FAMILY                 (((uint32_t)0x3F) << 6)
#define SYSCFG_CHIPREVHW_MINOR                  (((uint32_t)0xFF) << 12)

#define SYSCFG_CHIPREV_MAJOR                    (((uint32_t)0x3F) << 0)
#define SYSCFG_CHIPREV_FAMILY                   (((uint32_t)0x3F) << 6)
#define SYSCFG_CHIPREV_MINOR                    (((uint32_t)0xFF) << 12)

#define SYSCFG_CFGSYSTIC_SYSTICEXTCLKEN         (((uint32_t)0x01) << 0)

#define SYSCFG_CTRL_ADDRFAULTEN                 (((uint32_t)0x01) << 0)
#define SYSCFG_CTRL_RAMECCERRFAULTEN            (((uint32_t)0x01) << 5)

#define SYSCFG_DMEM0RETNCTRL_RAMRETNCTRL        (((uint32_t)0x03) << 0)

#define SYSCFG_DMEM0ECCADDR_DMEM0ECCADDR        (((uint32_t)0xFFFFFFFF) << 0)

#define SYSCFG_DMEM0ECCCTRL_RAMECCEN            (((uint32_t)0x01) << 0)
#define SYSCFG_DMEM0ECCCTRL_RAMECCEWEN          (((uint32_t)0x01) << 1)

#define SYSCFG_ROOTDATA0_DATA                   (((uint32_t)0xFFFFFFFF) << 0)

#define SYSCFG_ROOTDATA1_DATA                   (((uint32_t)0xFFFFFFFF) << 0)

#define SYSCFG_ROOTLOCKSTATUS_BUSLOCK           (((uint32_t)0x01) << 0)
#define SYSCFG_ROOTLOCKSTATUS_REGLOCK           (((uint32_t)0x01) << 1)
#define SYSCFG_ROOTLOCKSTATUS_MFRLOCK           (((uint32_t)0x01) << 2)
#define SYSCFG_ROOTLOCKSTATUS_ROOTMODELOCK      (((uint32_t)0x01) << 4)
#define SYSCFG_ROOTLOCKSTATUS_ROOTDBGLOCK       (((uint32_t)0x01) << 8)
#define SYSCFG_ROOTLOCKSTATUS_USERDBGLOCK       (((uint32_t)0x01) << 16)
#define SYSCFG_ROOTLOCKSTATUS_USERNIDLOCK       (((uint32_t)0x01) << 17)
#define SYSCFG_ROOTLOCKSTATUS_USERSPIDLOCK      (((uint32_t)0x01) << 18)
#define SYSCFG_ROOTLOCKSTATUS_USERSPNIDLOCK     (((uint32_t)0x01) << 19)
#define SYSCFG_ROOTLOCKSTATUS_USERDBGAPLOCK     (((uint32_t)0x01) << 20)

#endif
