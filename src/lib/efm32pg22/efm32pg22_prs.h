#ifndef _EFM32PG22_PRS_H_
#define _EFM32PG22_PRS_H_

#include <stdint.h>

typedef struct{
	volatile uint32_t IPVERSION;
	uint32_t RESERVED1[1];
	volatile uint32_t ASYNC_SWPULSE;
	volatile uint32_t ASYNC_SWLEVEL;
	volatile uint32_t ASYNC_PEEK;
	volatile uint32_t SYNC_PEEK;
	volatile uint32_t ASYNC_CH_CTRL[12];
	volatile uint32_t SYNC_CH_CTRL[4];
	volatile uint32_t CONSUMER_CMU_CALDN;
	volatile uint32_t CONSUMER_CMU_CALUP;
	uint32_t RESERVED4[1];
	volatile uint32_t CONSUMER_IADC0_SCANTRIGGER;
	volatile uint32_t CONSUMER_IADC0_SINGLETRIGGER;
	volatile uint32_t CONSUMER_LDMAXBAR_DMAREQ0;
	volatile uint32_t CONSUMER_LDMAXBAR_DMAREQ1;
	volatile uint32_t CONSUMER_LETIMER0_CLEAR;
	volatile uint32_t CONSUMER_LETIMER0_START;
	volatile uint32_t CONSUMER_LETIMER0_STOP;
	volatile uint32_t CONSUMER_EUART0_RX;
	volatile uint32_t CONSUMER_EUART0_TRIGGER;
	uint32_t  RESERVED5[24];
	volatile uint32_t CONSUMER_RTCC_CC0;
	volatile uint32_t CONSUMER_RTCC_CC1;
	volatile uint32_t CONSUMER_RTCC_CC2;
	uint32_t  RESERVED6[1];
	volatile uint32_t CONSUMER_CORE_CTIIN0;
	volatile uint32_t CONSUMER_CORE_CTIIN1;
	volatile uint32_t CONSUMER_CORE_CTIIN2;
	volatile uint32_t CONSUMER_CORE_CTIIN3;
	volatile uint32_t CONSUMER_CORE_M33RXEV;
	volatile uint32_t CONSUMER_TIMER0_CC0;
	volatile uint32_t CONSUMER_TIMER0_CC1;
	volatile uint32_t CONSUMER_TIMER0_CC2;
	volatile uint32_t CONSUMER_TIMER0_DTI;
	volatile uint32_t CONSUMER_TIMER0_DTIFS1;
	volatile uint32_t CONSUMER_TIMER0_DTIFS2;
	volatile uint32_t CONSUMER_TIMER1_CC0;
	volatile uint32_t CONSUMER_TIMER1_CC1;
	volatile uint32_t CONSUMER_TIMER1_CC2;
	volatile uint32_t CONSUMER_TIMER1_DTI;
	volatile uint32_t CONSUMER_TIMER1_DTIFS1;
	volatile uint32_t CONSUMER_TIMER1_DTIFS2;
	volatile uint32_t CONSUMER_TIMER2_CC0;
	volatile uint32_t CONSUMER_TIMER2_CC1;
	volatile uint32_t CONSUMER_TIMER2_CC2;
	volatile uint32_t CONSUMER_TIMER2_DTI;
	volatile uint32_t CONSUMER_TIMER2_DTIFS1;
	volatile uint32_t CONSUMER_TIMER2_DTIFS2;
	volatile uint32_t CONSUMER_TIMER3_CC0;
	volatile uint32_t CONSUMER_TIMER3_CC1;
	volatile uint32_t CONSUMER_TIMER3_CC2;
	volatile uint32_t CONSUMER_TIMER3_DTI;
	volatile uint32_t CONSUMER_TIMER3_DTIFS1;
	volatile uint32_t CONSUMER_TIMER3_DTIFS2;
	volatile uint32_t CONSUMER_TIMER4_CC0;
	volatile uint32_t CONSUMER_TIMER4_CC1;
	volatile uint32_t CONSUMER_TIMER4_CC2;
	volatile uint32_t CONSUMER_TIMER4_DTI;
	volatile uint32_t CONSUMER_TIMER4_DTIFS1;
	volatile uint32_t CONSUMER_TIMER4_DTIFS2;
	volatile uint32_t CONSUMER_USART0_CLK;
	volatile uint32_t CONSUMER_USART0_IR;
	volatile uint32_t CONSUMER_USART0_RX;
	volatile uint32_t CONSUMER_USART0_TRIGGER;
	volatile uint32_t CONSUMER_USART1_CLK;
	volatile uint32_t CONSUMER_USART1_IR;
	volatile uint32_t CONSUMER_USART1_RX;
	volatile uint32_t CONSUMER_USART1_TRIGGER;
	volatile uint32_t CONSUMER_WDOG0_SRC0;
	volatile uint32_t CONSUMER_WDOG0_SRC1;

}__attribute__((packed)) PRS_TypeDef;

#define PRS                            ((PRS_TypeDef *)0x40038000)
#define PRS_SET                        ((PRS_TypeDef *)0x40039000)
#define PRS_CLR                        ((PRS_TypeDef *)0x4003A000)
#define PRS_TGL                        ((PRS_TypeDef *)0x4003B000)

#define PRS_IPVERSION_IPVERSION        (((uint32_t)0xFFFFFFFF) << 0)

#define PRS_ASYNC_SWPULSE_CH0PULSE     (((uint32_t)0x01) << 0)
#define PRS_ASYNC_SWPULSE_CH1PULSE     (((uint32_t)0x01) << 1)
#define PRS_ASYNC_SWPULSE_CH2PULSE     (((uint32_t)0x01) << 2)
#define PRS_ASYNC_SWPULSE_CH3PULSE     (((uint32_t)0x01) << 3)
#define PRS_ASYNC_SWPULSE_CH4PULSE     (((uint32_t)0x01) << 4)
#define PRS_ASYNC_SWPULSE_CH5PULSE     (((uint32_t)0x01) << 5)
#define PRS_ASYNC_SWPULSE_CH6PULSE     (((uint32_t)0x01) << 6)
#define PRS_ASYNC_SWPULSE_CH7PULSE     (((uint32_t)0x01) << 7)
#define PRS_ASYNC_SWPULSE_CH8PULSE     (((uint32_t)0x01) << 8)
#define PRS_ASYNC_SWPULSE_CH9PULSE     (((uint32_t)0x01) << 9)
#define PRS_ASYNC_SWPULSE_CH10PULSE    (((uint32_t)0x01) << 10)
#define PRS_ASYNC_SWPULSE_CH11PULSE    (((uint32_t)0x01) << 11)

#define PRS_ASYNC_SWLEVEL_CH0LEVEL     (((uint32_t)0x01) << 0)
#define PRS_ASYNC_SWLEVEL_CH1LEVEL     (((uint32_t)0x01) << 1)
#define PRS_ASYNC_SWLEVEL_CH2LEVEL     (((uint32_t)0x01) << 2)
#define PRS_ASYNC_SWLEVEL_CH3LEVEL     (((uint32_t)0x01) << 3)
#define PRS_ASYNC_SWLEVEL_CH4LEVEL     (((uint32_t)0x01) << 4)
#define PRS_ASYNC_SWLEVEL_CH5LEVEL     (((uint32_t)0x01) << 5)
#define PRS_ASYNC_SWLEVEL_CH6LEVEL     (((uint32_t)0x01) << 6)
#define PRS_ASYNC_SWLEVEL_CH7LEVEL     (((uint32_t)0x01) << 7)
#define PRS_ASYNC_SWLEVEL_CH8LEVEL     (((uint32_t)0x01) << 8)
#define PRS_ASYNC_SWLEVEL_CH9LEVEL     (((uint32_t)0x01) << 9)
#define PRS_ASYNC_SWLEVEL_CH10LEVEL    (((uint32_t)0x01) << 10)
#define PRS_ASYNC_SWLEVEL_CH11LEVEL    (((uint32_t)0x01) << 11)

#define PRS_ASYNC_PEEK_CH0VAL          (((uint32_t)0x01) << 0)
#define PRS_ASYNC_PEEK_CH1VAL          (((uint32_t)0x01) << 1)
#define PRS_ASYNC_PEEK_CH2VAL          (((uint32_t)0x01) << 2)
#define PRS_ASYNC_PEEK_CH3VAL          (((uint32_t)0x01) << 3)
#define PRS_ASYNC_PEEK_CH4VAL          (((uint32_t)0x01) << 4)
#define PRS_ASYNC_PEEK_CH5VAL          (((uint32_t)0x01) << 5)
#define PRS_ASYNC_PEEK_CH6VAL          (((uint32_t)0x01) << 6)
#define PRS_ASYNC_PEEK_CH7VAL          (((uint32_t)0x01) << 7)
#define PRS_ASYNC_PEEK_CH8VAL          (((uint32_t)0x01) << 8)
#define PRS_ASYNC_PEEK_CH9VAL          (((uint32_t)0x01) << 9)
#define PRS_ASYNC_PEEK_CH10VAL         (((uint32_t)0x01) << 10)
#define PRS_ASYNC_PEEK_CH11VAL         (((uint32_t)0x01) << 11)

#define PRS_SYNC_PEEK_CH0VAL           (((uint32_t)0x01) << 0)
#define PRS_SYNC_PEEK_CH1VAL           (((uint32_t)0x01) << 1)
#define PRS_SYNC_PEEK_CH2VAL           (((uint32_t)0x01) << 2)
#define PRS_SYNC_PEEK_CH3VAL           (((uint32_t)0x01) << 3)

#define PRS_ASYNC_CH_CTRL_SIGSEL       (((uint32_t)0x07) << 0)
#define PRS_ASYNC_CH_CTRL_SOURCESEL    (((uint32_t)0x7F) << 8)
#define PRS_ASYNC_CH_CTRL_FNSEL        (((uint32_t)0x0F) << 16)
#define PRS_ASYNC_CH_CTRL_AUXSEL       (((uint32_t)0x0F) << 24)

#define PRS_SYNC_CH_CTRL_SIGSEL        (((uint32_t)0x07) << 0)
#define PRS_SYNC_CH_CTRL_SOURCESEL     (((uint32_t)0x7F) << 8)

#define PRS_CONSUMER_CMU_CALDN_PRSSEL  (((uint32_t)0x0F) << 0)

#define PRS_CONSUMER_CMU_CALUP_PRSSEL  (((uint32_t)0x0F) << 0)

#define PRS_CONSUMER_IADC0_SCANTRIGGER_PRSSEL (((uint32_t)0x0F) << 0)
#define PRS_CONSUMER_IADC0_SCANTRIGGER_SPRSSEL (((uint32_t)0x03) << 8)

#define PRS_CONSUMER_IADC0_SINGLETRIGGER_PRSSEL (((uint32_t)0x0F) << 0)
#define PRS_CONSUMER_IADC0_SINGLETRIGGER_SPRSSEL (((uint32_t)0x03) << 8)

#define PRS_CONSUMER_LDMAXBAR_DMAREQ0_PRSSEL (((uint32_t)0x0F) << 0)

#define PRS_CONSUMER_LDMAXBAR_DMAREQ1_PRSSEL (((uint32_t)0x0F) << 0)

#define PRS_CONSUMER_LETIMER0_CLEAR_PRSSEL (((uint32_t)0x0F) << 0)

#define PRS_CONSUMER_LETIMER0_START_PRSSEL (((uint32_t)0x0F) << 0)

#define PRS_CONSUMER_LETIMER0_STOP_PRSSEL (((uint32_t)0x0F) << 0)

#define PRS_CONSUMER_EUART0_RX_PRSSEL  (((uint32_t)0x0F) << 0)

#define PRS_CONSUMER_EUART0_TRIGGER_PRSSEL (((uint32_t)0x0F) << 0)

#define PRS_CONSUMER_RTCC_CC0_PRSSEL   (((uint32_t)0x0F) << 0)

#define PRS_CONSUMER_RTCC_CC1_PRSSEL   (((uint32_t)0x0F) << 0)

#define PRS_CONSUMER_RTCC_CC2_PRSSEL   (((uint32_t)0x0F) << 0)

#define PRS_CONSUMER_CORE_CTIIN0_PRSSEL (((uint32_t)0x0F) << 0)

#define PRS_CONSUMER_CORE_CTIIN1_PRSSEL (((uint32_t)0x0F) << 0)

#define PRS_CONSUMER_CORE_CTIIN2_PRSSEL (((uint32_t)0x0F) << 0)

#define PRS_CONSUMER_CORE_CTIIN3_PRSSEL (((uint32_t)0x0F) << 0)

#define PRS_CONSUMER_CORE_M33RXEV_PRSSEL (((uint32_t)0x0F) << 0)

#define PRS_CONSUMER_TIMER0_CC0_PRSSEL (((uint32_t)0x0F) << 0)
#define PRS_CONSUMER_TIMER0_CC0_SPRSSEL (((uint32_t)0x03) << 8)

#define PRS_CONSUMER_TIMER0_CC1_PRSSEL (((uint32_t)0x0F) << 0)
#define PRS_CONSUMER_TIMER0_CC1_SPRSSEL (((uint32_t)0x03) << 8)

#define PRS_CONSUMER_TIMER0_CC2_PRSSEL (((uint32_t)0x0F) << 0)
#define PRS_CONSUMER_TIMER0_CC2_SPRSSEL (((uint32_t)0x03) << 8)

#define PRS_CONSUMER_TIMER0_DTI_PRSSEL (((uint32_t)0x0F) << 0)

#define PRS_CONSUMER_TIMER0_DTIFS1_PRSSEL (((uint32_t)0x0F) << 0)

#define PRS_CONSUMER_TIMER0_DTIFS2_PRSSEL (((uint32_t)0x0F) << 0)

#define PRS_CONSUMER_TIMER1_CC0_PRSSEL (((uint32_t)0x0F) << 0)
#define PRS_CONSUMER_TIMER1_CC0_SPRSSEL (((uint32_t)0x03) << 8)

#define PRS_CONSUMER_TIMER1_CC1_PRSSEL (((uint32_t)0x0F) << 0)
#define PRS_CONSUMER_TIMER1_CC1_SPRSSEL (((uint32_t)0x03) << 8)

#define PRS_CONSUMER_TIMER1_CC2_PRSSEL (((uint32_t)0x0F) << 0)
#define PRS_CONSUMER_TIMER1_CC2_SPRSSEL (((uint32_t)0x03) << 8)

#define PRS_CONSUMER_TIMER1_DTI_PRSSEL (((uint32_t)0x0F) << 0)

#define PRS_CONSUMER_TIMER1_DTIFS1_PRSSEL (((uint32_t)0x0F) << 0)

#define PRS_CONSUMER_TIMER1_DTIFS2_PRSSEL (((uint32_t)0x0F) << 0)

#define PRS_CONSUMER_TIMER2_CC0_PRSSEL (((uint32_t)0x0F) << 0)
#define PRS_CONSUMER_TIMER2_CC0_SPRSSEL (((uint32_t)0x03) << 8)

#define PRS_CONSUMER_TIMER2_CC1_PRSSEL (((uint32_t)0x0F) << 0)
#define PRS_CONSUMER_TIMER2_CC1_SPRSSEL (((uint32_t)0x03) << 8)

#define PRS_CONSUMER_TIMER2_CC2_PRSSEL (((uint32_t)0x0F) << 0)
#define PRS_CONSUMER_TIMER2_CC2_SPRSSEL (((uint32_t)0x03) << 8)

#define PRS_CONSUMER_TIMER2_DTI_PRSSEL (((uint32_t)0x0F) << 0)

#define PRS_CONSUMER_TIMER2_DTIFS1_PRSSEL (((uint32_t)0x0F) << 0)

#define PRS_CONSUMER_TIMER2_DTIFS2_PRSSEL (((uint32_t)0x0F) << 0)

#define PRS_CONSUMER_TIMER3_CC0_PRSSEL (((uint32_t)0x0F) << 0)
#define PRS_CONSUMER_TIMER3_CC0_SPRSSEL (((uint32_t)0x03) << 8)

#define PRS_CONSUMER_TIMER3_CC1_PRSSEL (((uint32_t)0x0F) << 0)
#define PRS_CONSUMER_TIMER3_CC1_SPRSSEL (((uint32_t)0x03) << 8)

#define PRS_CONSUMER_TIMER3_CC2_PRSSEL (((uint32_t)0x0F) << 0)
#define PRS_CONSUMER_TIMER3_CC2_SPRSSEL (((uint32_t)0x03) << 8)

#define PRS_CONSUMER_TIMER3_DTI_PRSSEL (((uint32_t)0x0F) << 0)

#define PRS_CONSUMER_TIMER3_DTIFS1_PRSSEL (((uint32_t)0x0F) << 0)

#define PRS_CONSUMER_TIMER3_DTIFS2_PRSSEL (((uint32_t)0x0F) << 0)

#define PRS_CONSUMER_TIMER4_CC0_PRSSEL (((uint32_t)0x0F) << 0)
#define PRS_CONSUMER_TIMER4_CC0_SPRSSEL (((uint32_t)0x03) << 8)

#define PRS_CONSUMER_TIMER4_CC1_PRSSEL (((uint32_t)0x0F) << 0)
#define PRS_CONSUMER_TIMER4_CC1_SPRSSEL (((uint32_t)0x03) << 8)

#define PRS_CONSUMER_TIMER4_CC2_PRSSEL (((uint32_t)0x0F) << 0)
#define PRS_CONSUMER_TIMER4_CC2_SPRSSEL (((uint32_t)0x03) << 8)

#define PRS_CONSUMER_TIMER4_DTI_PRSSEL (((uint32_t)0x0F) << 0)

#define PRS_CONSUMER_TIMER4_DTIFS1_PRSSEL (((uint32_t)0x0F) << 0)

#define PRS_CONSUMER_TIMER4_DTIFS2_PRSSEL (((uint32_t)0x0F) << 0)

#define PRS_CONSUMER_USART0_CLK_PRSSEL (((uint32_t)0x0F) << 0)

#define PRS_CONSUMER_USART0_IR_PRSSEL  (((uint32_t)0x0F) << 0)

#define PRS_CONSUMER_USART0_RX_PRSSEL  (((uint32_t)0x0F) << 0)

#define PRS_CONSUMER_USART0_TRIGGER_PRSSEL (((uint32_t)0x0F) << 0)

#define PRS_CONSUMER_USART1_CLK_PRSSEL (((uint32_t)0x0F) << 0)

#define PRS_CONSUMER_USART1_IR_PRSSEL  (((uint32_t)0x0F) << 0)

#define PRS_CONSUMER_USART1_RX_PRSSEL  (((uint32_t)0x0F) << 0)

#define PRS_CONSUMER_USART1_TRIGGER_PRSSEL (((uint32_t)0x0F) << 0)

#define PRS_CONSUMER_WDOG0_SRC0_PRSSEL (((uint32_t)0x0F) << 0)

#define PRS_CONSUMER_WDOG0_SRC1_PRSSEL (((uint32_t)0x0F) << 0)

#endif
