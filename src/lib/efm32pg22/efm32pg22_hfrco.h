#ifndef _EFM32PG22_HFRCO_H_
#define _EFM32PG22_HFRCO_H_

#include <stdint.h>

typedef struct{
	volatile uint32_t IPVERSION;
	volatile uint32_t CTRL;
	volatile uint32_t CAL;
	volatile uint32_t STATUS;
	volatile uint32_t IF;
	volatile uint32_t IEN;
	uint32_t RESERVED1[1];
	volatile uint32_t LOCK;
}__attribute__((packed)) HFRCO_TypeDef;

#define HFRCO0                    ((HFRCO_TypeDef *)0x40010000)
#define HFRCO0_SET                ((HFRCO_TypeDef *)0x40011000)
#define HFRCO0_CLR                ((HFRCO_TypeDef *)0x40012000)
#define HFRCO0_TGL                ((HFRCO_TypeDef *)0x40013000)

#define HFRCO_IPVERSION_IPVERSION (((uint32_t)0xFFFFFFFF) << 0)

#define HFRCO_CTRL_FORCEEN        (((uint32_t)0x01) << 0)
#define HFRCO_CTRL_DISONDEMAND    (((uint32_t)0x01) << 1)

#define HFRCO_FREQRANGE_4MHz      0
#define HFRCO_FREQRANGE_5MHz      1
#define HFRCO_FREQRANGE_7MHz      3
#define HFRCO_FREQRANGE_10MHz     4
#define HFRCO_FREQRANGE_13MHz     6
#define HFRCO_FREQRANGE_16MHz     7
#define HFRCO_FREQRANGE_19MHz     8
#define HFRCO_FREQRANGE_20MHz     9
#define HFRCO_FREQRANGE_26MHz     10
#define HFRCO_FREQRANGE_32MHz     11
#define HFRCO_FREQRANGE_38MHz     12
#define HFRCO_FREQRANGE_48MHz     13
#define HFRCO_FREQRANGE_56MHz     14
#define HFRCO_FREQRANGE_64MHz     15
#define HFRCO_FREQRANGE_80MHz     16

#define HFRCO_CAL_TUNING          (((uint32_t)0x7F) << 1)
#define HFRCO_CAL_FINETUNING      (((uint32_t)0x3F) << 8)
#define HFRCO_CAL_LDOHP           (((uint32_t)0x01) << 15)
#define HFRCO_CAL_FREQRANGE       (((uint32_t)0x1F) << 16)
#define HFRCO_CAL_CMPBIAS         (((uint32_t)0x07) << 21)
#define HFRCO_CAL_CLKDIV          (((uint32_t)0x03) << 24)
#define HFRCO_CAL_CMPSEL          (((uint32_t)0x03) << 26)
#define HFRCO_CAL_IREFTC          (((uint32_t)0x0F) << 28)

#define HFRCO_STATUS_RDY          (((uint32_t)0x01) << 0)
#define HFRCO_STATUS_FREQBSY      (((uint32_t)0x01) << 1)
#define HFRCO_STATUS_SYNCBUSY     (((uint32_t)0x01) << 2)
#define HFRCO_STATUS_ENS          (((uint32_t)0x01) << 16)
#define HFRCO_STATUS_LOCK         (((uint32_t)0x01) << 31)

#define HFRCO_IF_RDY              (((uint32_t)0x01) << 0)

#define HFRCO_IEN_RDY             (((uint32_t)0x01) << 0)

#define HFRCO_LOCK_LOCKKEY        (((uint32_t)0xFFFF) << 0)

#endif
