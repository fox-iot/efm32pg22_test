#ifndef _EFM32PG22_MSC_H_
#define _EFM32PG22_MSC_H_

#include <stdint.h>

typedef struct{
	volatile uint32_t IPVERSION;
	uint32_t RESERVED1[1];
	volatile uint32_t READCTRL;
	volatile uint32_t WRITECTRL;
	volatile uint32_t WRITECMD;
	volatile uint32_t ADDRB;
	volatile uint32_t WDATA;
	volatile uint32_t STATUS;
	volatile uint32_t IF;
	volatile uint32_t IEN;
	uint32_t RESERVED2[3];
	volatile uint32_t USERDATASIZE;
	volatile uint32_t CMD;
	volatile uint32_t LOCK;
	volatile uint32_t MISCLOCKWORD;
	uint32_t RESERVED3[3];
	volatile uint32_t PWRCTRL;
	uint32_t RESERVED4[51];
	volatile uint32_t PAGELOCK0;
	volatile uint32_t PAGELOCK1;
}__attribute__((packed)) MSC_TypeDef;

#define MSC                            ((MSC_TypeDef *)0x40030000)
#define MSC_SET                        ((MSC_TypeDef *)0x40031000)
#define MSC_CLR                        ((MSC_TypeDef *)0x40032000)
#define MSC_TGL                        ((MSC_TypeDef *)0x40033000)

#define MSC_IPVERSION_IPVERSION        (((uint32_t)0xFFFFFFFF) << 0)

#define MSC_READCTRL_DOUTBUFEN         (((uint32_t)0x01) << 12)
#define MSC_READCTRL_MODE              (((uint32_t)0x03) << 20)

#define MSC_WRITECTRL_WREN             (((uint32_t)0x01) << 0)
#define MSC_WRITECTRL_IRQERASEABORT    (((uint32_t)0x01) << 1)
#define MSC_WRITECTRL_LPWRITE          (((uint32_t)0x01) << 3)

#define MSC_WRITECMD_ERASEPAGE         (((uint32_t)0x01) << 1)
#define MSC_WRITECMD_WRITEEND          (((uint32_t)0x01) << 2)
#define MSC_WRITECMD_ERASEABORT        (((uint32_t)0x01) << 5)
#define MSC_WRITECMD_ERASEMAIN0        (((uint32_t)0x01) << 8)
#define MSC_WRITECMD_CLEARWDATA        (((uint32_t)0x01) << 12)

#define MSC_ADDRB_ADDRB                (((uint32_t)0xFFFFFFFF) << 0)

#define MSC_WDATA_DATAW                (((uint32_t)0xFFFFFFFF) << 0)

#define MSC_STATUS_BUSY                (((uint32_t)0x01) << 0)
#define MSC_STATUS_LOCKED              (((uint32_t)0x01) << 1)
#define MSC_STATUS_INVADDR             (((uint32_t)0x01) << 2)
#define MSC_STATUS_WDATAREADY          (((uint32_t)0x01) << 3)
#define MSC_STATUS_ERASEABORTED        (((uint32_t)0x01) << 4)
#define MSC_STATUS_PENDING             (((uint32_t)0x01) << 5)
#define MSC_STATUS_TIMEOUT             (((uint32_t)0x01) << 6)
#define MSC_STATUS_REGLOCK             (((uint32_t)0x01) << 16)
#define MSC_STATUS_PWRON               (((uint32_t)0x01) << 24)
#define MSC_STATUS_WREADY              (((uint32_t)0x01) << 27)
#define MSC_STATUS_PWRUPCKBDFAILCOUNT  (((uint32_t)0x0F) << 28)

#define MSC_IF_ERASE                   (((uint32_t)0x01) << 0)
#define MSC_IF_WRITE                   (((uint32_t)0x01) << 1)
#define MSC_IF_WDATAOV                 (((uint32_t)0x01) << 2)
#define MSC_IF_PWRUPF                  (((uint32_t)0x01) << 8)
#define MSC_IF_PWROFF                  (((uint32_t)0x01) << 9)

#define MSC_IEN_ERASE                  (((uint32_t)0x01) << 0)
#define MSC_IEN_WRITE                  (((uint32_t)0x01) << 1)
#define MSC_IEN_WDATAOV                (((uint32_t)0x01) << 2)
#define MSC_IEN_PWRUPF                 (((uint32_t)0x01) << 8)
#define MSC_IEN_PWROFF                 (((uint32_t)0x01) << 9)

#define MSC_USERDATASIZE_USERDATASIZE  (((uint32_t)0x3F) << 0)

#define MSC_CMD_PWRUP                  (((uint32_t)0x01) << 0)
#define MSC_CMD_PWROFF                 (((uint32_t)0x01) << 4)

#define MSC_LOCK_LOCKKEY               (((uint32_t)0xFFFF) << 0)

#define MSC_MISCLOCKWORD_MELOCKBIT     (((uint32_t)0x01) << 0)
#define MSC_MISCLOCKWORD_UDLOCKBIT     (((uint32_t)0x01) << 4)

#define MSC_PWRCTRL_PWROFFONEM1ENTRY   (((uint32_t)0x01) << 0)
#define MSC_PWRCTRL_PWROFFENTRYAGAIN   (((uint32_t)0x01) << 4)
#define MSC_PWRCTRL_PWROFFDLY          (((uint32_t)0xFF) << 16)

#define MSC_PAGELOCK0_LOCKBIT          (((uint32_t)0xFFFFFFFF) << 0)

#define MSC_PAGELOCK1_LOCKBIT          (((uint32_t)0xFFFFFFFF) << 0)

#endif
