#ifndef _EFM32PG22_SMU_H_
#define _EFM32PG22_SMU_H_

#include <stdint.h>

/** SMU_CFGNS Register and Bits Declaration. MISSING!!!!!!! **/
typedef struct{
	volatile uint32_t IPVERSION;
	volatile uint32_t STATUS;
	volatile uint32_t LOCK;
	volatile uint32_t IF;
	volatile uint32_t IEN;
	uint32_t RESERVED1[3];
	volatile uint32_t M33CTRL;
	uint32_t RESERVED2[7];
	volatile uint32_t PPUPATD0;
	volatile uint32_t PPUPATD1;
	uint32_t RESERVED3[6];
	volatile uint32_t PPUSATD0;
	volatile uint32_t PPUSATD1;
	uint32_t RESERVED4[54];
	volatile uint32_t PPUFS;
	uint32_t RESERVED5[3];
	volatile uint32_t BMPUPATD0;
	uint32_t RESERVED6[7];
	volatile uint32_t BMPUSATD0;
	uint32_t RESERVED7[55];
	volatile uint32_t BMPUFS;
	volatile uint32_t BMPUFSADDR;
	uint32_t RESERVED8[2];
	volatile uint32_t ESAURTYPES0;
	volatile uint32_t ESAURTYPES1;
	uint32_t RESERVED9[2];
	volatile uint32_t ESAUMRB01;
	volatile uint32_t ESAUMRB12;
	uint32_t RESERVED10[2];
	volatile uint32_t ESAUMRB45;
	volatile uint32_t ESAUMRB56;
}__attribute__((packed)) SMU_TypeDef;

typedef struct {
	uint32_t UNKNOWN;
}__attribute__((packed)) SMU_CFGNS_TypeDef;

#define SMU                            ((SMU_TypeDef *)0x44008000)
#define SMU_SET                        ((SMU_TypeDef *)0x44009000)
#define SMU_CLR                        ((SMU_TypeDef *)0x4400A000)
#define SMU_TGL                        ((SMU_TypeDef *)0x4400B000)

#define SMU_CFGNS                      ((SMU_CFGNS_TypeDef *)0x4400C000)

#define SMU_IPVERSION_IPVERSION        (((uint32_t)0xFFFFFFFF) << 0)

#define SMU_STATUS_SMULOCK             (((uint32_t)0x01) << 0)
#define SMU_STATUS_SMUPRGERR           (((uint32_t)0x01) << 1)

#define SMU_LOCK_SMULOCKKEY            (((uint32_t)0xFFFFFF) << 0)

#define SMU_IF_PPUPRIV                 (((uint32_t)0x01) << 0)
#define SMU_IF_PPUINST                 (((uint32_t)0x01) << 2)
#define SMU_IF_PPUSEC                  (((uint32_t)0x01) << 3)
#define SMU_IF_BMPUSEC                 (((uint32_t)0x01) << 4)

#define SMU_IEN_PPUPRIV                (((uint32_t)0x01) << 0)
#define SMU_IEN_PPUINST                (((uint32_t)0x01) << 2)
#define SMU_IEN_PPUSEC                 (((uint32_t)0x01) << 3)
#define SMU_IEN_BMPUSEC                (((uint32_t)0x01) << 4)

#define SMU_M33CTRL_LOCKSVTAIRCR       (((uint32_t)0x01) << 0)
#define SMU_M33CTRL_LOCKNSVTOR         (((uint32_t)0x01) << 1)
#define SMU_M33CTRL_LOCKSMPU           (((uint32_t)0x01) << 2)
#define SMU_M33CTRL_LOCKNSMPU          (((uint32_t)0x01) << 3)
#define SMU_M33CTRL_LOCKSAU            (((uint32_t)0x01) << 4)

#define SMU_PPUPATD0_EMU               (((uint32_t)0x01) << 1)
#define SMU_PPUPATD0_CMU               (((uint32_t)0x01) << 2)
#define SMU_PPUPATD0_HFXO0             (((uint32_t)0x01) << 3)
#define SMU_PPUPATD0_HFRCO0            (((uint32_t)0x01) << 4)
#define SMU_PPUPATD0_FSRCO             (((uint32_t)0x01) << 5)
#define SMU_PPUPATD0_DPLL0             (((uint32_t)0x01) << 6)
#define SMU_PPUPATD0_LFXO              (((uint32_t)0x01) << 7)
#define SMU_PPUPATD0_LFRCO             (((uint32_t)0x01) << 8)
#define SMU_PPUPATD0_ULFRCO            (((uint32_t)0x01) << 9)
#define SMU_PPUPATD0_MSC               (((uint32_t)0x01) << 10)
#define SMU_PPUPATD0_ICACHE0           (((uint32_t)0x01) << 11)
#define SMU_PPUPATD0_PRS               (((uint32_t)0x01) << 12)
#define SMU_PPUPATD0_GPIO              (((uint32_t)0x01) << 13)
#define SMU_PPUPATD0_LDMA              (((uint32_t)0x01) << 14)
#define SMU_PPUPATD0_LDMAXBAR          (((uint32_t)0x01) << 15)
#define SMU_PPUPATD0_TIMER0            (((uint32_t)0x01) << 16)
#define SMU_PPUPATD0_TIMER1            (((uint32_t)0x01) << 17)
#define SMU_PPUPATD0_TIMER2            (((uint32_t)0x01) << 18)
#define SMU_PPUPATD0_TIMER3            (((uint32_t)0x01) << 19)
#define SMU_PPUPATD0_TIMER4            (((uint32_t)0x01) << 20)
#define SMU_PPUPATD0_USART0            (((uint32_t)0x01) << 21)
#define SMU_PPUPATD0_USART1            (((uint32_t)0x01) << 22)
#define SMU_PPUPATD0_BURTC             (((uint32_t)0x01) << 23)
#define SMU_PPUPATD0_I2C1              (((uint32_t)0x01) << 24)
#define SMU_PPUPATD0_CHIPTESTCTRL      (((uint32_t)0x01) << 25)
#define SMU_PPUPATD0_SYSCFGCFGNS       (((uint32_t)0x01) << 26)
#define SMU_PPUPATD0_SYSCFG            (((uint32_t)0x01) << 27)
#define SMU_PPUPATD0_BURAM             (((uint32_t)0x01) << 28)
#define SMU_PPUPATD0_GPCRC             (((uint32_t)0x01) << 30)
#define SMU_PPUPATD0_DCI               (((uint32_t)0x01) << 31)

#define SMU_PPUPATD1_DCDC              (((uint32_t)0x00) << 1)
#define SMU_PPUPATD1_PDM               (((uint32_t)0x01) << 2)
#define SMU_PPUPATD1_SMU               (((uint32_t)0x01) << 5)
#define SMU_PPUPATD1_SMUCFGNS          (((uint32_t)0x01) << 6)
#define SMU_PPUPATD1_RTCC              (((uint32_t)0x01) << 7)
#define SMU_PPUPATD1_LETIMER0          (((uint32_t)0x01) << 8)
#define SMU_PPUPATD1_IADC0             (((uint32_t)0x01) << 9)
#define SMU_PPUPATD1_I2C0              (((uint32_t)0x01) << 10)
#define SMU_PPUPATD1_WDOG0             (((uint32_t)0x01) << 11)
#define SMU_PPUPATD1_AMUXCP0           (((uint32_t)0x01) << 12)
#define SMU_PPUPATD1_EUART0            (((uint32_t)0x01) << 13)
#define SMU_PPUPATD1_CRYPTOACC         (((uint32_t)0x01) << 14)

#define SMU_PPUSATD0_EMU               (((uint32_t)0x01) << 1)
#define SMU_PPUSATD0_CMU               (((uint32_t)0x01) << 2)
#define SMU_PPUSATD0_HFXO0             (((uint32_t)0x01) << 3)
#define SMU_PPUSATD0_HFRCO0            (((uint32_t)0x01) << 4)
#define SMU_PPUSATD0_FSRCO             (((uint32_t)0x01) << 5)
#define SMU_PPUSATD0_DPLL0             (((uint32_t)0x01) << 6)
#define SMU_PPUSATD0_LFXO              (((uint32_t)0x01) << 7)
#define SMU_PPUSATD0_LFRCO             (((uint32_t)0x01) << 8)
#define SMU_PPUSATD0_ULFRCO            (((uint32_t)0x01) << 9)
#define SMU_PPUSATD0_MSC               (((uint32_t)0x01) << 10)
#define SMU_PPUSATD0_ICACHE0           (((uint32_t)0x01) << 11)
#define SMU_PPUSATD0_PRS               (((uint32_t)0x01) << 12)
#define SMU_PPUSATD0_GPIO              (((uint32_t)0x01) << 13)
#define SMU_PPUSATD0_LDMA              (((uint32_t)0x01) << 14)
#define SMU_PPUSATD0_LDMAXBAR          (((uint32_t)0x01) << 15)
#define SMU_PPUSATD0_TIMER0            (((uint32_t)0x01) << 16)
#define SMU_PPUSATD0_TIMER1            (((uint32_t)0x01) << 17)
#define SMU_PPUSATD0_TIMER2            (((uint32_t)0x01) << 18)
#define SMU_PPUSATD0_TIMER3            (((uint32_t)0x01) << 19)
#define SMU_PPUSATD0_TIMER4            (((uint32_t)0x01) << 20)
#define SMU_PPUSATD0_USART0            (((uint32_t)0x01) << 21)
#define SMU_PPUSATD0_USART1            (((uint32_t)0x01) << 22)
#define SMU_PPUSATD0_BURTC             (((uint32_t)0x01) << 23)
#define SMU_PPUSATD0_I2C1              (((uint32_t)0x01) << 24)
#define SMU_PPUSATD0_CHIPTESTCTRL      (((uint32_t)0x01) << 25)
#define SMU_PPUSATD0_SYSCFGCFGNS       (((uint32_t)0x01) << 26)
#define SMU_PPUSATD0_SYSCFG            (((uint32_t)0x01) << 27)
#define SMU_PPUSATD0_BURAM             (((uint32_t)0x01) << 28)
#define SMU_PPUSATD0_GPCRC             (((uint32_t)0x01) << 30)
#define SMU_PPUSATD0_DCI               (((uint32_t)0x01) << 31)

#define SMU_PPUSATD1_DCDC              (((uint32_t)0x01) << 1)
#define SMU_PPUSATD1_PDM               (((uint32_t)0x01) << 2)
#define SMU_PPUSATD1_SMU               (((uint32_t)0x01) << 5)
#define SMU_PPUSATD1_SMUCFGNS          (((uint32_t)0x01) << 6)
#define SMU_PPUSATD1_RTCC              (((uint32_t)0x01) << 7)
#define SMU_PPUSATD1_LETIMER0          (((uint32_t)0x01) << 8)
#define SMU_PPUSATD1_IADC0             (((uint32_t)0x01) << 9)
#define SMU_PPUSATD1_I2C0              (((uint32_t)0x01) << 10)
#define SMU_PPUSATD1_WDOG0             (((uint32_t)0x01) << 11)
#define SMU_PPUSATD1_AMUXCP0           (((uint32_t)0x01) << 12)
#define SMU_PPUSATD1_EUART0            (((uint32_t)0x01) << 13)
#define SMU_PPUSATD1_CRYPTOACC         (((uint32_t)0x01) << 14)

#define SMU_PPUFS_PPUFSPERIPHID        (((uint32_t)0xFF) << 0)

#define SMU_BMPUPATD0_CRYPTOACC        (((uint32_t)0x01) << 1)
#define SMU_BMPUPATD0_LDMA             (((uint32_t)0x01) << 4)

#define SMU_BMPUSATD0_CRYPTOACC        (((uint32_t)0x01) << 1)
#define SMU_BMPUSATD0_LDMA             (((uint32_t)0x01) << 4)

#define SMU_BMPUFS_BMPUFSMASTERID      (((uint32_t)0xFF) << 0)

#define SMU_BMPUFSADDR_BMPUFSADDR      (((uint32_t)0xFFFFFFFF) << 0)

#define SMU_ESAURTYPES0_ESAUR3NS       (((uint32_t)0x01) << 12)

#define SMU_ESAURTYPES1_ESAUR11NS      (((uint32_t)0x01) << 12)

#define SMU_ESAUMRB01_ESAUMRB01        (((uint32_t)0xFFFF) << 12)

#define SMU_ESAUMRB12_ESAUMRB12        (((uint32_t)0xFFFF) << 12)

#define SMU_ESAUMRB45_ESAUMRB45        (((uint32_t)0xFFFF) << 12)

#define SMU_ESAUMRB56_ESAUMRB56        (((uint32_t)0xFFFF) << 12)

/** SMU_CFGNS BIT Declaration. MISSING!!!!!!! **/

#endif
