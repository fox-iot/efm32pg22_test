
#ifndef _EFM32PG22_ULFSRCO_H_
#define _EFM32PG22_ULFSRCO_H_

#include <stdint.h>

typedef struct {
	volatile uint32_t IPVERSION;
	uint32_t RESERVED1[1];
	volatile uint32_t STATUS;
	uint32_t RESERVED2[2];
	volatile uint32_t IF;
	volatile uint32_t IEN;
}__attribute__((packed)) ULFSRCO_TypeDef;

#define ULFSRCO                        ((ULFSRCO_TypeDef *)0x40028000)
#define ULFSRCO_SET                    ((ULFSRCO_TypeDef *)0x40029000)
#define ULFSRCO_CLR                    ((ULFSRCO_TypeDef *)0x4002A000)
#define ULFSRCO_TGL                    ((ULFSRCO_TypeDef *)0x4002B000)

#define ULFSRCO_IPVERSION_IPVERSION    (((uint32_t)0xFFFFFFFF) << 0)

#define ULFRCO_STATUS_RDY              (((uint32_t)0x01) << 0)
#define ULFRCO_STATUS_ENS              (((uint32_t)0x01) << 16)

#define ULFRCO_IF_RDY                  (((uint32_t)0x01) << 0)
#define ULFRCO_IF_POSEDGE              (((uint32_t)0x01) << 1)
#define ULFRCO_IF_NEGEDGE              (((uint32_t)0x01) << 2)

#define ULFRCO_IEN_RDY                 (((uint32_t)0x01) << 0)
#define ULFRCO_IEN_POSEDGE             (((uint32_t)0x01) << 1)
#define ULFRCO_IEN_NEGEDGE             (((uint32_t)0x01) << 2)

#endif
