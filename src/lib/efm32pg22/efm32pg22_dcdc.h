#ifndef _EFM32PG22_DCDC_H_
#define _EFM32PG22_DCDC_H_

#include <stdint.h>

typedef struct{
	volatile uint32_t IPVERSION;
	volatile uint32_t EN;
	volatile uint32_t CTRL;
	uint32_t RESERVED1[1];
	volatile uint32_t EM01CTRL0;
	volatile uint32_t EM23CTRL0;
	uint32_t RESERVED2[3];
	volatile uint32_t IF;
	volatile uint32_t IEN;
	volatile uint32_t STATUS;
	uint32_t RESERVED3[4];
	volatile uint32_t LOCK;
	volatile uint32_t LOCKSTATUS;
}__attribute__((packed)) DCDC_TypeDef;

#define DCDC                      ((DCDC_TypeDef *)0x40094000)
#define DCDC_SET                  ((DCDC_TypeDef *)0x40095000)
#define DCDC_CLR                  ((DCDC_TypeDef *)0x40096000)
#define DCDC_TGL                  ((DCDC_TypeDef *)0x40097000)

#define DCDC_IPVERSION_IPVERSION  (((uint32_t)0xFFFFFFFF) << 0)

#define DCDC_EN_EN                (((uint32_t)0x01) << 0)

#define DCDC_CTRL_MODE            (((uint32_t)0x01) << 0)
#define DCDC_CTRL_DCMONLYEN       (((uint32_t)0x01) << 2)
#define DCDC_CTRL_IPKTMAXCTRL     (((uint32_t)0x07) << 4)

#define DCDC_EM01CTRL0_IPKVAL     (((uint32_t)0x0F) << 0)
#define DCDC_EM01CTRL0_DRVSPEED   (((uint32_t)0x03) << 8)

#define DCDC_EM23CTRL0_IPKVAL     (((uint32_t)0x0F) << 0)
#define DCDC_EM23CTRL0_DRVSPEED   (((uint32_t)0x03) << 8)

#define DCDC_IF_BYPSW             (((uint32_t)0x01) << 0)
#define DCDC_IF_WARM              (((uint32_t)0x01) << 1)
#define DCDC_IF_RUNNING           (((uint32_t)0x01) << 2)
#define DCDC_IF_VREGINLOW         (((uint32_t)0x01) << 3)
#define DCDC_IF_VREGINHIGH        (((uint32_t)0x01) << 4)
#define DCDC_IF_REGULATION        (((uint32_t)0x01) << 5)
#define DCDC_IF_TMAX              (((uint32_t)0x01) << 6)
#define DCDC_IF_EM4ERR            (((uint32_t)0x01) << 7)

#define DCDC_IEN_BYPSW            (((uint32_t)0x01) << 0)
#define DCDC_IEN_WARM             (((uint32_t)0x01) << 1)
#define DCDC_IEN_RUNNING          (((uint32_t)0x01) << 2)
#define DCDC_IEN_VREGINLOW        (((uint32_t)0x01) << 3)
#define DCDC_IEN_VREGINHIGH       (((uint32_t)0x01) << 4)
#define DCDC_IEN_REGULATION       (((uint32_t)0x01) << 5)
#define DCDC_IEN_TMAX             (((uint32_t)0x01) << 6)
#define DCDC_IEN_EM4ERR           (((uint32_t)0x01) << 7)

#define DCDC_STATUS_BYPSW         (((uint32_t)0x01) << 0)
#define DCDC_STATUS_WARM          (((uint32_t)0x01) << 1)
#define DCDC_STATUS_RUNNING       (((uint32_t)0x01) << 2)
#define DCDC_STATUS_VREGIN        (((uint32_t)0x01) << 3)
#define DCDC_STATUS_BYPCMPOUT     (((uint32_t)0x01) << 4)

#define DCDC_LOCK_LOCKKEY         (((uint32_t)0xFFFF) << 0)

#define DCDC_LOCKSTATUS_LOCK      (((uint32_t)0x01) << 0)

#endif
