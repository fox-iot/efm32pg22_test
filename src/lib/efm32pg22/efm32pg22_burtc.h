#ifndef _EFM32PG22_BURTC_H_
#define _EFM32PG22_BURTC_H_

#include <stdint.h>

typedef struct{
	volatile uint32_t IPVERSION;
	volatile uint32_t EN;
	volatile uint32_t CFG;
	volatile uint32_t CMD;
	volatile uint32_t STATUS;
	volatile uint32_t IF;
	volatile uint32_t IEN;
	volatile uint32_t PRECNT;
	volatile uint32_t CNT;
	volatile uint32_t EM4WUEN;
	volatile uint32_t SYNCBUSY;
	volatile uint32_t LOCK;
	volatile uint32_t COMP;

}__attribute__((packed)) BURTC_TypeDef;

#define BURTC                          ((BURTC_TypeDef *)0x40064000)
#define BURTC_SET                      ((BURTC_TypeDef *)0x40065000)
#define BURTC_CLR                      ((BURTC_TypeDef *)0x40066000)
#define BURTC_TGL                      ((BURTC_TypeDef *)0x40067000)

#define BURTC_IPVERSION_IPVERSION      (((uint32_t)0xFFFFFFFF) << 0)

#define BURTC_EN_EN                    (((uint32_t)0x01) << 0)

#define BURTC_CFG_DEBUGRUN             (((uint32_t)0x01) << 0)
#define BURTC_CFG_COMPTOP              (((uint32_t)0x01) << 1)
#define BURTC_CFG_CNTPRESC             (((uint32_t)0x0F) << 4)

#define BURTC_CMD_START                (((uint32_t)0x01) << 0)
#define BURTC_CMD_STOP                 (((uint32_t)0x01) << 1)

#define BURTC_STATUS_RUNNING           (((uint32_t)0x01) << 0)
#define BURTC_STATUS_LOCK              (((uint32_t)0x01) << 1)

#define BURTC_IF_OF                    (((uint32_t)0x01) << 0)
#define BURTC_IF_COMP                  (((uint32_t)0x01) << 1)

#define BURTC_IEN_OF                   (((uint32_t)0x01) << 0)
#define BURTC_IEN_COMP                 (((uint32_t)0x01) << 1)

#define BURTC_PRECNT_PRECNT            (((uint32_t)0x7FFF) << 0)

#define BURTC_CNT_CNT                  (((uint32_t)0xFFFFFFFF) << 0)

#define BURTC_EM4WUEN_OFEM4WUEN        (((uint32_t)0x01) << 0)
#define BURTC_EM4WUEN_COMPEM4WUEN      (((uint32_t)0x01) << 1)

#define BURTC_SYNCBUSY_START           (((uint32_t)0x01) << 0)
#define BURTC_SYNCBUSY_STOP            (((uint32_t)0x01) << 1)
#define BURTC_SYNCBUSY_PRECNT          (((uint32_t)0x01) << 2)
#define BURTC_SYNCBUSY_CNT             (((uint32_t)0x01) << 3)
#define BURTC_SYNCBUSY_COMP            (((uint32_t)0x01) << 4)
#define BURTC_SYNCBUSY_EN              (((uint32_t)0x01) << 5)

#define BURTC_LOCK_LOCKKEY             (((uint32_t)0xFFFF) << 0)

#define BURTC_COMP_COMP                (((uint32_t)0xFFFFFFFF) << 0)

#endif
