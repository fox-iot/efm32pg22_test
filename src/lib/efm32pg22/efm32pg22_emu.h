#ifndef _EFM32PG22_EMU_H_
#define _EFM32PG22_EMU_H_

#include <stdint.h>

typedef struct{
	uint32_t RESERVED1[3];
	volatile uint32_t DECBOD;
	uint32_t RESERVED2[3];
	volatile uint32_t BOD3SENSE;
	uint32_t RESERVED3[6];
	volatile uint32_t VREGVDDCMPCTRL;
	volatile uint32_t PD1PARETCTRL;
	uint32_t RESERVED4[7];
	volatile uint32_t LOCK;
	volatile uint32_t IF;
	volatile uint32_t IEN;
	volatile uint32_t EM4CTRL;
	volatile uint32_t CMD;
	volatile uint32_t CTRL;
	volatile uint32_t TEMPLIMITS;
	uint32_t RESERVED5[2];
	volatile uint32_t STATUS;
	volatile uint32_t TEMP;
	uint32_t RESERVED6[1];
	volatile uint32_t RSTCTRL;
	volatile uint32_t RSTCAUSE;
	uint32_t RESERVED7[2];
	volatile uint32_t DGIF;
	volatile uint32_t DGIEN;
	uint32_t RESERVED8[22];
	volatile uint32_t EFPIF;
	volatile uint32_t EFPIEN;
}__attribute__((packed)) EMU_TypeDef;

#define EMU                               ((EMU_TypeDef *)0x40004000)
#define EMU_SET                           ((EMU_TypeDef *)0x40005000)
#define EMU_CLR                           ((EMU_TypeDef *)0x40006000)
#define EMU_TGL                           ((EMU_TypeDef *)0x40007000)

#define EMU_DECBOD_DECBODEN               (((uint32_t)0x01) << 0)
#define EMU_DECBOD_DECBODMASK             (((uint32_t)0x01) << 1)
#define EMU_DECBOD_DECOVMBODEN            (((uint32_t)0x01) << 4)
#define EMU_DECBOD_DECOVMBODMASK          (((uint32_t)0x01) << 5)

#define EMU_BOD3SENSE_AVDDBODEN           (((uint32_t)0x01) << 0)
#define EMU_BOD3SENSE_VDDIO0BODEN         (((uint32_t)0x01) << 1)
#define EMU_BOD3SENSE_VDDIO1BODEN         (((uint32_t)0x01) << 2)

#define EMU_VREGVDDCMPCTRL_VREGINCMPEN    (((uint32_t)0x01) << 0)
#define EMU_VREGVDDCMPCTRL_THRESSEL       (((uint32_t)0x03) << 1)

#define EMU_PD1PARETCTRL_PD1PARETDIS      (((uint32_t)0xFFFF) << 0)

#define EMU_LOCK_LOCKKEY                  (((uint32_t)0xFFFF) << 0)

#define EMU_IF_AVDDBOD                    (((uint32_t)0x01) << 16)
#define EMU_IF_IOVDD0BOD                  (((uint32_t)0x01) << 17)
#define EMU_IF_EM23WAKEUP                 (((uint32_t)0x01) << 24)
#define EMU_IF_VSCALEDONE                 (((uint32_t)0x01) << 25)
#define EMU_IF_TEMPAVG                    (((uint32_t)0x01) << 27)
#define EMU_IF_TEMP                       (((uint32_t)0x01) << 29)
#define EMU_IF_TEMPLOW                    (((uint32_t)0x01) << 30)
#define EMU_IF_TEMPHIGH                   (((uint32_t)0x01) << 31)

#define EMU_IEN_AVDDBOD                    (((uint32_t)0x01) << 16)
#define EMU_IEN_IOVDD0BOD                  (((uint32_t)0x01) << 17)
#define EMU_IEN_EM23WAKEUP                 (((uint32_t)0x01) << 24)
#define EMU_IEN_VSCALEDONE                 (((uint32_t)0x01) << 25)
#define EMU_IEN_TEMPAVG                    (((uint32_t)0x01) << 27)
#define EMU_IEN_TEMP                       (((uint32_t)0x01) << 29)
#define EMU_IEN_TEMPLOW                    (((uint32_t)0x01) << 30)
#define EMU_IEN_TEMPHIGH                   (((uint32_t)0x01) << 31)

#define EMU_EM4CTRL_EM4ENTRY               (((uint32_t)0x03) << 0)
#define EMU_EM4CTRL_EM4IORETMODE           (((uint32_t)0x03) << 4)
#define EMU_EM4CTRL_BOD3SENSEEM4WU         (((uint32_t)0x01) << 8)

#define EMU_CMD_EM4UNLATCH                 (((uint32_t)0x01) << 1)
#define EMU_CMD_TEMPAVGREQ                 (((uint32_t)0x01) << 4)
#define EMU_CMD_EM01VSCALE1                (((uint32_t)0x01) << 10)
#define EMU_CMD_EM01VSCALE2                (((uint32_t)0x01) << 11)
#define EMU_CMD_RSTCAUSECLR                (((uint32_t)0x01) << 17)

#define EMU_CTRL_EM2DBGEN                  (((uint32_t)0x01) << 0)
#define EMU_CTRL_TEMPAVGNUM                (((uint32_t)0x01) << 3)
#define EMU_CTRL_EM23VSCALE                (((uint32_t)0x03) << 8)
#define EMU_CTRL_FLASHPWRUPONDEMAND        (((uint32_t)0x01) << 16)
#define EMU_CTRL_EFPDIRECTMODEEN           (((uint32_t)0x01) << 29)
#define EMU_CTRL_EFPDRVDECOUPLE            (((uint32_t)0x01) << 30)
#define EMU_CTRL_EFPDRVDVDD                (((uint32_t)0x01) << 31)

#define EMU_TEMPLIMITS_TEMPLOW             (((uint32_t)0x01FF) << 0)
#define EMU_TEMPLIMITS_TEMPHIGH            (((uint32_t)0x01FF) << 16)

#define EMU_STATUS_LOCK                    (((uint32_t)0x01) << 0)
#define EMU_STATUS_FIRSTTEMPDONE           (((uint32_t)0x01) << 1)
#define EMU_STATUS_TEMPACTIVE              (((uint32_t)0x01) << 2)
#define EMU_STATUS_TEMPAVGACTIVE           (((uint32_t)0x01) << 3)
#define EMU_STATUS_VSCALEBUSY              (((uint32_t)0x01) << 4)
#define EMU_STATUS_VSCALEFAILED            (((uint32_t)0x01) << 5)
#define EMU_STATUS_VSCALE                  (((uint32_t)0x03) << 6)
#define EMU_STATUS_EM4IORET                (((uint32_t)0x01) << 12)
#define EMU_STATUS_EM2ENTERED              (((uint32_t)0x01) << 14)

#define EMU_TEMP_TEMPLSB                   (((uint32_t)0x03) << 0)
#define EMU_TEMP_TEMP                      (((uint32_t)0x01FF) << 2) 
#define EMU_TEMP_TEMPAVG                   (((uint32_t)0x07FF) << 16)

#define EMU_RSTCTRL_WDOG0RMODE             (((uint32_t)0x01) << 0)
#define EMU_RSTCTRL_SYSRMODE               (((uint32_t)0x01) << 2)
#define EMU_RSTCTRL_LOCKUPRMODE            (((uint32_t)0x01) << 3)
#define EMU_RSTCTRL_AVDDBODRMODE           (((uint32_t)0x01) << 6)
#define EMU_RSTCTRL_IOVDD0BODRMODE         (((uint32_t)0x01) << 7)
#define EMU_RSTCTRL_DECBODRMODE            (((uint32_t)0x01) << 10)
#define EMU_RSTCTRL_DCIRMODE               (((uint32_t)0x01) << 16)

#define EMU_RSTCAUSE_POR                   (((uint32_t)0x01) << 0)
#define EMU_RSTCAUSE_PIN                   (((uint32_t)0x01) << 1)
#define EMU_RSTCAUSE_EM4                   (((uint32_t)0x01) << 2)
#define EMU_RSTCAUSE_WDOG0                 (((uint32_t)0x01) << 3)
#define EMU_RSTCAUSE_LOCKUP                (((uint32_t)0x01) << 5)
#define EMU_RSTCAUSE_SYSREQ                (((uint32_t)0x01) << 6)
#define EMU_RSTCAUSE_DVDDBOD               (((uint32_t)0x01) << 7)
#define EMU_RSTCAUSE_DVDDLEBOD             (((uint32_t)0x01) << 8)
#define EMU_RSTCAUSE_DECBOD                (((uint32_t)0x01) << 9)
#define EMU_RSTCAUSE_AVDDBOD               (((uint32_t)0x01) << 10)
#define EMU_RSTCAUSE_IOVDD0BOD             (((uint32_t)0x01) << 11)
#define EMU_RSTCAUSE_DCI                   (((uint32_t)0x01) << 16)
#define EMU_RSTCAUSE_VREGIN                (((uint32_t)0x01) << 31)

#define EMU_DGIF_EM23WAKEUPDGIF            (((uint32_t)0x01) << 24)
#define EMU_DGIF_TEMPDGIF                  (((uint32_t)0x01) << 29)
#define EMU_DGIF_TEMPLOWDGIF               (((uint32_t)0x01) << 30)
#define EMU_DGIF_TEMPHIGHDGIF              (((uint32_t)0x01) << 31)

#define EMU_DGIEN_EM23WAKEUPDGIF           (((uint32_t)0x01) << 24)
#define EMU_DGIEN_TEMPDGIF                 (((uint32_t)0x01) << 29)
#define EMU_DGIEN_TEMPLOWDGIF              (((uint32_t)0x01) << 30)
#define EMU_DGIEN_TEMPHIGHDGIF             (((uint32_t)0x01) << 31)

#define EMU_EFPIF_EFPIF                    (((uint32_t)0x01) << 0)

#define EMU_EFPIEN_EFPIEN                  (((uint32_t)0x01) << 0)

#endif
