#ifndef _EFM32PG22_CRYPTOACC_H_
#define _EFM32PG22_CRYPTOACC_H_

#include <stdint.h>

typedef struct{
	volatile uint32_t FETCHADDR;
	uint32_t RESERVED1[1];
	volatile uint32_t FETCHLEN;
	volatile uint32_t FETCHTAG;
	volatile uint32_t PUSHADDR;
	uint32_t RESERVED2[1];
	volatile uint32_t PUSHLEN;
	volatile uint32_t IEN;
	uint32_t RESERVED3[2];
	volatile uint32_t IF;
	uint32_t RESERVED4[1];
	volatile uint32_t IF_CLR;
	volatile uint32_t CTRL;
	volatile uint32_t CMD;
	volatile uint32_t STATUS;
	uint32_t RESERVED5[240];
	volatile uint32_t INCL_IPS_HW_CFG;
	volatile uint32_t BA411E_HW_CFG_1;
	volatile uint32_t BA411E_HW_CFG_2;
	volatile uint32_t BA413_HW_CFG;
	volatile uint32_t BA418_HW_CFG;
	volatile uint32_t BA419_HW_CFG;
}__attribute__((packed)) CRYPTOACC_TypeDef;

#define CRYPTOACC                      ((CRYPTOACC_TypeDef *)0x4C020000)
//#define CRYPTOACC                      ((CRYPTOACC_TypeDef *)0x4C021000)
//#define CRYPTOACC                      ((CRYPTOACC_TypeDef *)0x4C022000)
#define CRYPTOACC_SET                  ((CRYPTOACC_TypeDef *)0x4C021000)
#define CRYPTOACC_CLR                  ((CRYPTOACC_TypeDef *)0x4C022000)
#define CRYPTOACC_TGL                  ((CRYPTOACC_TypeDef *)0x4C023000)

#define CRYPTOACC_FETCHADDR_ADDR       (((uint32_t)0xFFFFFFFF) << 0)

#define CRYPTOACC_FETCHLEN_LENGTH      (((uint32_t)0x0FFFFFFF) << 0)
#define CRYPTOACC_FETCHLEN_CONSTADDR   (((uint32_t)0x01) << 28)
#define CRYPTOACC_FETCHLEN_REALIGN     (((uint32_t)0x01) << 29)

#define CRYPTOACC_FETCHTAG_TAG         (((uint32_t)0xFFFFFFFF) << 0)

#define CRYPTOACC_PUSHADDR_ADDR        (((uint32_t)0xFFFFFFFF) << 0)

#define CRYPTOACC_PUSHLEN_LENGTH       (((uint32_t)0x0FFFFFFF) << 0)
#define CRYPTOACC_PUSHLEN_CONSTADDR    (((uint32_t)0x01) << 28)
#define CRYPTOACC_PUSHLEN_REALIGN      (((uint32_t)0x01) << 29)
#define CRYPTOACC_PUSHLEN_DISCARD      (((uint32_t)0x01) << 30)

#define CRYPTOACC_IEN_FETCHERENDOFBLOCK (((uint32_t)0x01) << 0)
#define CRYPTOACC_IEN_FETCHERSTOPPED   (((uint32_t)0x01) << 1)
#define CRYPTOACC_IEN_FETCHERERROR     (((uint32_t)0x01) << 2)
#define CRYPTOACC_IEN_PUSHERENDOFBLOCK (((uint32_t)0x01) << 3)
#define CRYPTOACC_IEN_PUSHERSTOPPED    (((uint32_t)0x01) << 4)
#define CRYPTOACC_IEN_PUSHERERROR      (((uint32_t)0x01) << 5)

#define CRYPTOACC_IF_FETCHERENDOFBLOCK (((uint32_t)0x01) << 0)
#define CRYPTOACC_IF_FETCHERSTOPPED    (((uint32_t)0x01) << 1)
#define CRYPTOACC_IF_FETCHERERROR      (((uint32_t)0x01) << 2)
#define CRYPTOACC_IF_PUSHERENDOFBLOCK  (((uint32_t)0x01) << 3)
#define CRYPTOACC_IF_PUSHERSTOPPED     (((uint32_t)0x01) << 4)
#define CRYPTOACC_IF_PUSHERERROR       (((uint32_t)0x01) << 5)

#define CRYPTOACC_IF_CLR_FETCHERENDOFBLOCK (((uint32_t)0x01) << 0)
#define CRYPTOACC_IF_CLR_FETCHERSTOPPED (((uint32_t)0x01) << 1)
#define CRYPTOACC_IF_CLR_FETCHERERROR  (((uint32_t)0x01) << 2)
#define CRYPTOACC_IF_CLR_PUSHERENDOFBLOCK (((uint32_t)0x01) << 3)
#define CRYPTOACC_IF_CLR_PUSHERSTOPPED (((uint32_t)0x01) << 4)
#define CRYPTOACC_IF_CLR_PUSHERERROR   (((uint32_t)0x01) << 5)

#define CRYPTOACC_CTRL_FETCHERSCATTERGATHER (((uint32_t)0x01) << 0)
#define CRYPTOACC_CTRL_PUSHERSCATTERGATHER (((uint32_t)0x01) << 1)
#define CRYPTOACC_CTRL_STOPFETCHER     (((uint32_t)0x01) << 2)
#define CRYPTOACC_CTRL_STOPPUSHER      (((uint32_t)0x01) << 3)
#define CRYPTOACC_CTRL_SWRESET         (((uint32_t)0x01) << 4)

#define CRYPTOACC_CMD_STARTFETCHER     (((uint32_t)0x01) << 0)
#define CRYPTOACC_CMD_STARTPUSHER      (((uint32_t)0x01) << 1)

#define CRYPTOACC_STATUS_FETCHERBSY    (((uint32_t)0x01) << 0)
#define CRYPTOACC_STATUS_PUSHERBSY     (((uint32_t)0x01) << 1)
#define CRYPTOACC_STATUS_NOTEMPTY      (((uint32_t)0x01) << 4)
#define CRYPTOACC_STATUS_WAITING       (((uint32_t)0x01) << 5)
#define CRYPTOACC_STATUS_SOFTRSTBSY    (((uint32_t)0x01) << 6)
#define CRYPTOACC_STATUS_FIFODATANUM   (((uint32_t)0xFFFF) << 16)

#define CRYPTOACC_INCL_IPS_HW_CFG_g_IncludeAES (((uint32_t)0x01) << 0)
#define CRYPTOACC_INCL_IPS_HW_CFG_g_IncludeAESGCM (((uint32_t)0x01) << 1)
#define CRYPTOACC_INCL_IPS_HW_CFG_g_IncludeAESXTS (((uint32_t)0x01) << 2)
#define CRYPTOACC_INCL_IPS_HW_CFG_g_IncludeDES (((uint32_t)0x01) << 3)
#define CRYPTOACC_INCL_IPS_HW_CFG_g_IncludeHASH (((uint32_t)0x01) << 4)
#define CRYPTOACC_INCL_IPS_HW_CFG_g_IncludeChachaPoly (((uint32_t)0x01) << 5)
#define CRYPTOACC_INCL_IPS_HW_CFG_g_IncludeSHA3 (((uint32_t)0x01) << 6)
#define CRYPTOACC_INCL_IPS_HW_CFG_g_IncludeZUC (((uint32_t)0x01) << 7)
#define CRYPTOACC_INCL_IPS_HW_CFG_g_IncludeSM4 (((uint32_t)0x01) << 8)
#define CRYPTOACC_INCL_IPS_HW_CFG_g_IncludePKE (((uint32_t)0x01) << 9)
#define CRYPTOACC_INCL_IPS_HW_CFG_g_IncludeNDRNG (((uint32_t)0x01) << 10)

#define CRYPTOACC_BA411E_HW_CFG_1_g_AesModesPoss (((uint32_t)0x01FF) << 0)
#define CRYPTOACC_BA411E_HW_CFG_1_g_CS (((uint32_t)0x01) << 16)
#define CRYPTOACC_BA411E_HW_CFG_1_g_UseMasking (((uint32_t)0x01) << 17)
#define CRYPTOACC_BA411E_HW_CFG_1_g_Keysize (((uint32_t)0x07) << 24)

#define CRYPTOACC_BA411E_HW_CFG_2_g_CtrSize (((uint32_t)0xFFFF) << 0)

#define CRYPTOACC_BA413_HW_CFG_g_HashMaskFunc (((uint32_t)0x7F) << 0)
#define CRYPTOACC_BA413_HW_CFG_g_HashPadding (((uint32_t)0x01) << 16)
#define CRYPTOACC_BA413_HW_CFG_g_HMAC_enabled (((uint32_t)0x01) << 17)
#define CRYPTOACC_BA413_HW_CFG_g_HashVerifyDigest (((uint32_t)0x01) << 18)

#define CRYPTOACC_BA418_HW_CFG_g_Sha3CtxtEn (((uint32_t)0x01) << 0)

#define CRYPTOACC_BA419_HW_CFG_g_SM4ModesPoss (((uint32_t)0x7F) << 0)

#endif
