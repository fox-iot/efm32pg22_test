
#ifndef _EFM32PG22_GPCRC_H_
#define _EFM32PG22_GPCRC_H_

#include <stdint.h>

typedef struct{
	volatile uint32_t IPVERSION;
	volatile uint32_t EN;
	volatile uint32_t CTRL;
	volatile uint32_t CMD;
	volatile uint32_t INIT;
	volatile uint32_t POLY;
	volatile uint32_t INPUTDATA;
	volatile uint32_t INPUTDATAHWORD;
	volatile uint32_t INPUTDATABYTE;
	volatile uint32_t DATA;
	volatile uint32_t DATAREV;
	volatile uint32_t DATABYTEREV;
}__attribute__((packed)) GPCRC_TypeDef;

#define GPCRC                               ((GPCRC_TypeDef *)0x40088000)
#define GPCRC_SET                           ((GPCRC_TypeDef *)0x40089000)
#define GPCRC_CLR                           ((GPCRC_TypeDef *)0x4008A000)
#define GPCRC_TGL                           ((GPCRC_TypeDef *)0x4008B000)

#define GPCRC_IPVERSION_IPVERSION           (((uint32_t)0xFFFFFFFF) << 0)

#define RTCC_EN_EN                          (((uint32_t)0x01) << 0)

#define GPCRC_EN_EN                         (((uint32_t)0x01) << 0)

#define GPCRC_CTRL_POLYSEL                  (((uint32_t)0x01) << 4)
#define GPCRC_CTRL_BYTEMODE                 (((uint32_t)0x01) << 8)
#define GPCRC_CTRL_BITREVERSE               (((uint32_t)0x01) << 9)
#define GPCRC_CTRL_BYTEREVERSE              (((uint32_t)0x01) << 10)
#define GPCRC_CTRL_AUTOINIT                 (((uint32_t)0x01) << 13)

#define GPCRC_CMD_INIT                      (((uint32_t)0x01) << 0)

#define GPCRC_INIT_INIT                     (((uint32_t)0xFFFFFFFF) << 0)

#define GPCRC_POLY_POLY                     (((uint32_t)0xFFFF) << 0)

#define GPCRC_INPUTDATA_INPUTDATA           (((uint32_t)0xFFFFFFFF) << 0)

#define GPCRC_INPUTDATAHWORD_INPUTDATAHWORD (((uint32_t)0xFFFF) << 0)

#define GPCRC_INPUTDATABYTE_INPUTDATABYTE   (((uint32_t)0xFF) << 0)

#define GPCRC_DATA_DATA                     (((uint32_t)0xFFFFFFFF) << 0)

#define GPCRC_DATAREV_DATAREV               (((uint32_t)0xFFFFFFFF) << 0)

#define GPCRC_DATABYTEREV_DATABYTEREV       (((uint32_t)0xFFFFFFFF) << 0)

#endif
