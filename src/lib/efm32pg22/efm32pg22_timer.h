#ifndef _EFM32PG22_TIMER_H_
#define _EFM32PG22_TIMER_H_

#include <stdint.h>

typedef struct {
	volatile uint32_t CFG;
	volatile uint32_t CTRL;
	volatile uint32_t OC;
	uint32_t RESERVED1[1];
	volatile uint32_t OCB;
	volatile uint32_t ICF;
	volatile uint32_t ICOF;
	uint32_t RESERVED2[2];
}__attribute__((packed)) TIMER_CC_TypeDef;

typedef struct{
	volatile uint32_t IPVERSION;
	volatile uint32_t CFG;
	volatile uint32_t CTRL;
	volatile uint32_t CMD;
	volatile uint32_t STATUS;
	volatile uint32_t IF;
	volatile uint32_t IEN;
	volatile uint32_t TOP;
	volatile uint32_t TOPB;
	volatile uint32_t CNT;
	uint32_t RESERVED1[1];
	volatile uint32_t LOCK;
	volatile uint32_t EN;
	uint32_t RESERVED2[11];
	TIMER_CC_TypeDef CC[3];
	uint32_t RESERVED4[8];
	volatile uint32_t DTCFG;
	volatile uint32_t DTTIMECFG;
	volatile uint32_t DTFCFG;
	volatile uint32_t DTCTRL;
	volatile uint32_t DTOGEN;
	volatile uint32_t DTFAULT;
	volatile uint32_t DTFAULTC;
	volatile uint32_t DTLOCK;
}__attribute__((packed)) TIMER_TypeDef;

#define TIMER0                        ((TIMER_TypeDef *)0x40048000)
#define TIMER0_SET                    ((TIMER_TypeDef *)0x40049000)
#define TIMER0_CLR                    ((TIMER_TypeDef *)0x4004A000)
#define TIMER0_TGL                    ((TIMER_TypeDef *)0x4004B000)

#define TIMER1                        ((TIMER_TypeDef *)0x4004C000)
#define TIMER1_SET                    ((TIMER_TypeDef *)0x4004D000)
#define TIMER1_CLR                    ((TIMER_TypeDef *)0x4004E000)
#define TIMER1_TGL                    ((TIMER_TypeDef *)0x4004F000)

#define TIMER2                        ((TIMER_TypeDef *)0x40050000)
#define TIMER2_SET                    ((TIMER_TypeDef *)0x40051000)
#define TIMER2_CLR                    ((TIMER_TypeDef *)0x40052000)
#define TIMER2_TGL                    ((TIMER_TypeDef *)0x40053000)

#define TIMER3                        ((TIMER_TypeDef *)0x40054000)
#define TIMER3_SET                    ((TIMER_TypeDef *)0x40055000)
#define TIMER3_CLR                    ((TIMER_TypeDef *)0x40056000)
#define TIMER3_TGL                    ((TIMER_TypeDef *)0x40057000)

#define TIMER4                        ((TIMER_TypeDef *)0x40058000)
#define TIMER4_SET                    ((TIMER_TypeDef *)0x40059000)
#define TIMER4_CLR                    ((TIMER_TypeDef *)0x4005A000)
#define TIMER4_TGL                    ((TIMER_TypeDef *)0x4005B000)

#define TIMER_IPVERSION_IPVERSION     (((uint32_t)0xFFFFFFFF) << 0)

#define TIMER_CFG_MODE                (((uint32_t)0x03) << 0)
#define TIMER_CFG_SYNC                (((uint32_t)0x01) << 3)
#define TIMER_CFG_OSMEN               (((uint32_t)0x01) << 4)
#define TIMER_CFG_QDM                 (((uint32_t)0x01) << 5)
#define TIMER_CFG_DEBUGRUN            (((uint32_t)0x01) << 6)
#define TIMER_CFG_DMACLRACT           (((uint32_t)0x01) << 7)
#define TIMER_CFG_CLKSEL              (((uint32_t)0x03) << 8)
#define TIMER_CFG_RETIMEEN            (((uint32_t)0x01) << 10)
#define TIMER_CFG_DISSYNCOUT          (((uint32_t)0x01) << 11)
#define TIMER_CFG_ATI                 (((uint32_t)0x01) << 16)
#define TIMER_CFG_RSSCOIST            (((uint32_t)0x01) << 17)
#define TIMER_CFG_PRESC               (((uint32_t)0x03FF) << 18)

#define TIMER_CTRL_RISEA              (((uint32_t)0x03) << 0)
#define TIMER_CTRL_FALLA              (((uint32_t)0x03) << 2)
#define TIMER_CTRL_X2CNT              (((uint32_t)0x01) << 4)

#define TIMER_CMD_START               (((uint32_t)0x01) << 0)
#define TIMER_CMD_STOP                (((uint32_t)0x01) << 1)

#define TIMER_STATUS_RUNNING          (((uint32_t)0x01) << 0)
#define TIMER_STATUS_DIR              (((uint32_t)0x01) << 1) 
#define TIMER_STATUS_TOPBV            (((uint32_t)0x01) << 2)
#define TIMER_STATUS_TIMERLOCKSTATUS  (((uint32_t)0x01) << 4)
#define TIMER_STATUS_DTILOCKSTATUS    (((uint32_t)0x01) << 5)
#define TIMER_STATUS_SYNCBUSY         (((uint32_t)0x01) << 6)
#define TIMER_STATUS_OCBV0            (((uint32_t)0x01) << 8)
#define TIMER_STATUS_OCBV1            (((uint32_t)0x01) << 9)
#define TIMER_STATUS_OCBV2            (((uint32_t)0x01) << 10)
#define TIMER_STATUS_ICFEMPTY0        (((uint32_t)0x01) << 16) 
#define TIMER_STATUS_ICFEMPTY1        (((uint32_t)0x01) << 17)
#define TIMER_STATUS_ICFEMPTY2        (((uint32_t)0x01) << 18)
#define TIMER_STATUS_CCPOL0           (((uint32_t)0x01) << 24)
#define TIMER_STATUS_CCPOL1           (((uint32_t)0x01) << 25)
#define TIMER_STATUS_CCPOL2           (((uint32_t)0x01) << 26)

#define TIMER_IF_OF                   (((uint32_t)0x01) << 0)
#define TIMER_IF_UF                   (((uint32_t)0x01) << 1)
#define TIMER_IF_DIRCHG               (((uint32_t)0x01) << 2)
#define TIMER_IF_CC0                  (((uint32_t)0x01) << 4)
#define TIMER_IF_CC1                  (((uint32_t)0x01) << 5)
#define TIMER_IF_CC2                  (((uint32_t)0x01) << 6)
#define TIMER_IF_ICFWLFULL0           (((uint32_t)0x01) << 16)
#define TIMER_IF_ICFWLFULL1           (((uint32_t)0x01) << 17)
#define TIMER_IF_ICFWLFULL2           (((uint32_t)0x01) << 18)
#define TIMER_IF_ICFOF0               (((uint32_t)0x01) << 20)
#define TIMER_IF_ICFOF1               (((uint32_t)0x01) << 21)
#define TIMER_IF_ICFOF2               (((uint32_t)0x01) << 22)
#define TIMER_IF_ICFUF0               (((uint32_t)0x01) << 24)
#define TIMER_IF_ICFUF1               (((uint32_t)0x01) << 25)
#define TIMER_IF_ICFUF2               (((uint32_t)0x01) << 26)

#define TIMER_IEN_OF                  (((uint32_t)0x01) << 0)
#define TIMER_IEN_UF                  (((uint32_t)0x01) << 1)
#define TIMER_IEN_DIRCHG              (((uint32_t)0x01) << 2)
#define TIMER_IEN_CC0                 (((uint32_t)0x01) << 4)
#define TIMER_IEN_CC1                 (((uint32_t)0x01) << 5)
#define TIMER_IEN_CC2                 (((uint32_t)0x01) << 6)
#define TIMER_IEN_ICFWLFULL0          (((uint32_t)0x01) << 16)
#define TIMER_IEN_ICFWLFULL1          (((uint32_t)0x01) << 17)
#define TIMER_IEN_ICFWLFULL2          (((uint32_t)0x01) << 18)
#define TIMER_IEN_ICFOF0              (((uint32_t)0x01) << 20)
#define TIMER_IEN_ICFOF1              (((uint32_t)0x01) << 21)
#define TIMER_IEN_ICFOF2              (((uint32_t)0x01) << 22)
#define TIMER_IEN_ICFUF0              (((uint32_t)0x01) << 24)
#define TIMER_IEN_ICFUF1              (((uint32_t)0x01) << 25)
#define TIMER_IEN_ICFUF2              (((uint32_t)0x01) << 26)

#define TIMER_TOP_TOP                 (((uint32_t)0xFFFFFFFF) << 0)

#define TIMER_TOPB_TOPB               (((uint32_t)0xFFFFFFFF) << 0)

#define TIMER_CNT_CNT                 (((uint32_t)0xFFFFFFFF) << 0)

#define TIMER_LOCK_LOCKKEY            (((uint32_t)0xFFFF) << 0)

#define TIMER_EN_EN                   (((uint32_t)0x01) << 0)

#define TIMER_CC_CFG_MODE            (((uint32_t)0x03) << 0)
#define TIMER_CC_CFG_COIST           (((uint32_t)0x01) << 4)
#define TIMER_CC_CFG_INSEL           (((uint32_t)0x03) << 17)
#define TIMER_CC_CFG_PRSCONF         (((uint32_t)0x01) << 19)
#define TIMER_CC_CFG_FILT            (((uint32_t)0x01) << 20)
#define TIMER_CC_CFG_ICFWL           (((uint32_t)0x01) << 21)

#define TIMER_CC_CTRL_OUTINV         (((uint32_t)0x01) << 2)
#define TIMER_CC_CTRL_CMOA           (((uint32_t)0x03) << 8)
#define TIMER_CC_CTRL_COFOA          (((uint32_t)0x03) << 10)
#define TIMER_CC_CTRL_CUFOA          (((uint32_t)0x03) << 12)
#define TIMER_CC_CTRL_ICEDGE         (((uint32_t)0x03) << 24)
#define TIMER_CC_CTRL_ICEVCTRL       (((uint32_t)0x03) << 26)

#define TIMER_CC_OC_OC               (((uint32_t)0xFFFFFFFF) << 0)

#define TIMER_CC_OCB_OCB             (((uint32_t)0xFFFFFFFF) << 0)

#define TIMER_CC_ICF_ICF             (((uint32_t)0xFFFFFFFF) << 0)

#define TIMER_CC_ICOF_ICOF           (((uint32_t)0xFFFFFFFF) << 0)
  
#define TIMER_DTCFG_DTEN              (((uint32_t)0x01) << 0) 
#define TIMER_DTCFG_DTDAS             (((uint32_t)0x01) << 1) 
#define TIMER_DTCFG_DTAR              (((uint32_t)0x01) << 9) 
#define TIMER_DTCFG_DTFATS            (((uint32_t)0x01) << 10) 
#define TIMER_DTCFG_DTPRSEN           (((uint32_t)0x01) << 11) 

#define TIMER_DTTIMECFG_DTPRESC       (((uint32_t)0x03FF) << 0)
#define TIMER_DTTIMECFG_DTRISET       (((uint32_t)0x3F) << 10)
#define TIMER_DTTIMECFG_DTFALLT       (((uint32_t)0x3F) << 16)

#define TIMER_DTFCFG_DTFA             (((uint32_t)0x03) << 16)
#define TIMER_DTFCFG_DTPRS0FEN        (((uint32_t)0x01) << 24)
#define TIMER_DTFCFG_DTPRS1FEN        (((uint32_t)0x01) << 25)
#define TIMER_DTFCFG_DTDBGFEN         (((uint32_t)0x01) << 26)
#define TIMER_DTFCFG_DTLOCKUPFEN      (((uint32_t)0x01) << 27)
#define TIMER_DTFCFG_DTEM23FEN        (((uint32_t)0x01) << 28)

#define TIMER_DTCTRL_DTCINV           (((uint32_t)0x01) << 0) 
#define TIMER_DTCTRL_DTIPOL           (((uint32_t)0x01) << 1)

#define TIMER_DTOGEN_DTOGCC0EN        (((uint32_t)0x01) << 0)
#define TIMER_DTOGEN_DTOGCC1EN        (((uint32_t)0x01) << 1)  
#define TIMER_DTOGEN_DTOGCC2EN        (((uint32_t)0x01) << 2)
#define TIMER_DTOGEN_DTOGCDTI0EN      (((uint32_t)0x01) << 3)
#define TIMER_DTOGEN_DTOGCDTI1EN      (((uint32_t)0x01) << 4)
#define TIMER_DTOGEN_DTOGCDTI2EN      (((uint32_t)0x01) << 5)

#define TIMER_DTFAULT_DTPRS0F         (((uint32_t)0x01) << 0) 
#define TIMER_DTFAULT_DTPRS1F         (((uint32_t)0x01) << 1)
#define TIMER_DTFAULT_DTDBGF          (((uint32_t)0x01) << 2)
#define TIMER_DTFAULT_DTLOCKUPF       (((uint32_t)0x01) << 3)
#define TIMER_DTFAULT_DTEM23F         (((uint32_t)0x01) << 4)
 
#define TIMER_DTFAULTC_DTPRS0FC       (((uint32_t)0x01) << 0) 
#define TIMER_DTFAULTC_DTPRS1FC       (((uint32_t)0x01) << 1) 
#define TIMER_DTFAULTC_DTDBGFC        (((uint32_t)0x01) << 2) 
#define TIMER_DTFAULTC_DTLOCKUPFC     (((uint32_t)0x01) << 3) 
#define TIMER_DTFAULTC_DTEM23FC       (((uint32_t)0x01) << 4) 

#define TIMER_DTLOCK_DTILOCKKEY       (((uint32_t)0xFFFF) << 0)

#endif
