#ifndef _EFM32PG22_DPLL_H_
#define _EFM32PG22_DPLL_H_

#include <stdint.h>

typedef struct{
	volatile uint32_t IPVERSION;
	volatile uint32_t EN;
	volatile uint32_t CFG;
	volatile uint32_t CFG1;
	volatile uint32_t IF;
	volatile uint32_t IEN;
	volatile uint32_t STATUS;
	uint32_t RESERVED1[2];
	volatile uint32_t LOCK;
}__attribute__((packed)) DPLL_TypeDef;

#define DPLL0                     ((DPLL_TypeDef *)0x4001C000)
#define DPLL0_SET                 ((DPLL_TypeDef *)0x4001D000)
#define DPLL0_CLR                 ((DPLL_TypeDef *)0x4001E000)
#define DPLL0_TGL                 ((DPLL_TypeDef *)0x4001F000)

#define DPLL_IPVERSION_IPVERSION  (((uint32_t)0xFFFFFFFF) << 0)

#define DPLL_EN_EN                (((uint32_t)0x01) << 0)

#define DPLL_CFG_MODE             (((uint32_t)0x01) << 0)
#define DPLL_CFG_EDGESEL          (((uint32_t)0x01) << 1)
#define DPLL_CFG_AUTORECOVER      (((uint32_t)0x01) << 2)
#define DPLL_CFG_DITHEN           (((uint32_t)0x01) << 6)

#define DPLL_CFG1_M               (((uint32_t)0x0FFF) << 0)
#define DPLL_CFG1_N               (((uint32_t)0x0FFF) << 16)

#define DPLL_IF_LOCK              (((uint32_t)0x01) << 0)
#define DPLL_IF_LOCKFAILLOW       (((uint32_t)0x01) << 1)
#define DPLL_IF_LOCKFAILHIGH      (((uint32_t)0x01) << 2)

#define DPLL_IEN_LOCK             (((uint32_t)0x01) << 0)
#define DPLL_IEN_LOCKFAILLOW      (((uint32_t)0x01) << 1)
#define DPLL_IEN_LOCKFAILHIGH     (((uint32_t)0x01) << 2)

#define DPLL_STATUS_RDY           (((uint32_t)0x01) << 0)
#define DPLL_STATUS_ENS           (((uint32_t)0x01) << 1)
#define DPLL_STATUS_LOCK          (((uint32_t)0x01) << 31)

#define DPLL_LOCK_LOCKKEY         (((uint32_t)0x0FFFF) << 0)

#endif
