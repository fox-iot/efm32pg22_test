#ifndef _EFM32PG22_GPIO_H_
#define _EFM32PG22_GPIO_H_

#include <stdint.h>

typedef struct{
	volatile uint32_t PORTA_CTRL;
	volatile uint32_t PORTA_MODEL;
	uint32_t RESERVED1[1];
	volatile uint32_t PORTA_MODEH;
	volatile uint32_t PORTA_DOUT;
	volatile uint32_t PORTA_DIN;
	uint32_t RESERVED2[6];
	volatile uint32_t PORTB_CTRL;
	volatile uint32_t PORTB_MODEL;
	uint32_t RESERVED3[2];
	volatile uint32_t PORTB_DOUT;
	volatile uint32_t PORTB_DIN;
	uint32_t RESERVED4[6];
	volatile uint32_t PORTC_CTRL;
	volatile uint32_t PORTC_MODEL;
	uint32_t RESERVED5[2];
	volatile uint32_t PORTC_DOUT;
	volatile uint32_t PORTC_DIN;
	uint32_t RESERVED6[6];
	volatile uint32_t PORTD_CTRL;
	volatile uint32_t PORTD_MODEL;
	uint32_t RESERVED7[2];
	volatile uint32_t PORTD_DOUT;
	volatile uint32_t PORTD_DIN;
	uint32_t RESERVED8[150];
	volatile uint32_t LOCK;
	uint32_t RESERVED9[3];
	volatile uint32_t GPIOLOCKSTATUS;
	uint32_t RESERVED10[3];
	volatile uint32_t ABUSALLOC;
	volatile uint32_t BBUSALLOC;
	volatile uint32_t CDBUSALLOC;
	uint32_t RESERVED11[53];
	volatile uint32_t EXTIPSELL;
	volatile uint32_t EXTIPSELH;
	volatile uint32_t EXTIPINSELL;
	volatile uint32_t EXTIPINSELH;
	volatile uint32_t EXTIRISE;
	volatile uint32_t EXTIFALL;
	uint32_t RESERVED12[2];
	volatile uint32_t IF;
	volatile uint32_t IEN;
	uint32_t RESERVED13[1];
	volatile uint32_t EM4WUEN;
	volatile uint32_t EM4WUPOL;
	uint32_t RESERVED14[3];
	volatile uint32_t DBGROUTEPEN;
	volatile uint32_t TRACEROUTEPEN;
	uint32_t RESERVED15[2];
	volatile uint32_t CMU_ROUTEEN;
	volatile uint32_t CMU_CLKIN0ROUTE;
	volatile uint32_t CMU_CLKOUT0ROUTE;
	volatile uint32_t CMU_CLKOUT1ROUTE;
	volatile uint32_t CMU_CLKOUT2ROUTE;
	uint32_t RESERVED16[2];
	volatile uint32_t DCDC_ROUTEEN;
	uint32_t RESERVED17[8];
	volatile uint32_t I2C0_ROUTEEN;
	volatile uint32_t I2C0_SCLROUTE;
	volatile uint32_t I2C0_SDAROUTE;
	uint32_t RESERVED18[1];
	volatile uint32_t I2C1_ROUTEEN;
	volatile uint32_t I2C1_SCLROUTE;
	volatile uint32_t I2C1_SDAROUTE;
	uint32_t RESERVED19[1];
	volatile uint32_t LETIMER0_ROUTEEN;
	volatile uint32_t LETIMER0_OUT0ROUTE;
	volatile uint32_t LETIMER0_OUT1ROUTE;
	uint32_t RESERVED20[1];
	volatile uint32_t EUART0_ROUTEEN;
	volatile uint32_t EUART0_CTSROUTE;
	volatile uint32_t EUART0_RTSROUTE;
	volatile uint32_t EUART0_RXROUTE;
	volatile uint32_t EUART0_TXROUTE;
	uint32_t RESERVED21[19];
	volatile uint32_t PDM_ROUTEEN;
	volatile uint32_t PDM_CLKROUTE;
	volatile uint32_t PDM_DAT0ROUTE;
	volatile uint32_t PDM_DAT1ROUTE;
	uint32_t RESERVED22[1];
	volatile uint32_t PRS0_ROUTEEN;
	volatile uint32_t PRS0_ASYNCH0ROUTE;
	volatile uint32_t PRS0_ASYNCH1ROUTE;
	volatile uint32_t PRS0_ASYNCH2ROUTE;
	volatile uint32_t PRS0_ASYNCH3ROUTE;
	volatile uint32_t PRS0_ASYNCH4ROUTE;
	volatile uint32_t PRS0_ASYNCH5ROUTE;
	volatile uint32_t PRS0_ASYNCH6ROUTE;
	volatile uint32_t PRS0_ASYNCH7ROUTE;
	volatile uint32_t PRS0_ASYNCH8ROUTE;
	volatile uint32_t PRS0_ASYNCH9ROUTE;
	volatile uint32_t PRS0_ASYNCH10ROUTE;
	volatile uint32_t PRS0_ASYNCH11ROUTE;
	volatile uint32_t PRS0_SYNCH0ROUTE;
	volatile uint32_t PRS0_SYNCH1ROUTE;
	volatile uint32_t PRS0_SYNCH2ROUTE;
	volatile uint32_t PRS0_SYNCH3ROUTE;
	uint32_t RESERVED23[1];
	volatile uint32_t TIMER0_ROUTEEN;
	volatile uint32_t TIMER0_CC0ROUTE;
	volatile uint32_t TIMER0_CC1ROUTE;
	volatile uint32_t TIMER0_CC2ROUTE;
	volatile uint32_t TIMER0_CDTI0ROUTE;
	volatile uint32_t TIMER0_CDTI1ROUTE;
	volatile uint32_t TIMER0_CDTI2ROUTE;
	uint32_t RESERVED24[1];
	volatile uint32_t TIMER1_ROUTEEN;
	volatile uint32_t TIMER1_CC0ROUTE;
	volatile uint32_t TIMER1_CC1ROUTE;
	volatile uint32_t TIMER1_CC2ROUTE;
	volatile uint32_t TIMER1_CDTI0ROUTE;
	volatile uint32_t TIMER1_CDTI1ROUTE;
	volatile uint32_t TIMER1_CDTI2ROUTE;
	uint32_t RESERVED25[1];
	volatile uint32_t TIMER2_ROUTEEN;
	volatile uint32_t TIMER2_CC0ROUTE;
	volatile uint32_t TIMER2_CC1ROUTE;
	volatile uint32_t TIMER2_CC2ROUTE;
	volatile uint32_t TIMER2_CDTI0ROUTE;
	volatile uint32_t TIMER2_CDTI1ROUTE;
	volatile uint32_t TIMER2_CDTI2ROUTE;
	uint32_t RESERVED26[1];
	volatile uint32_t TIMER3_ROUTEEN;
	volatile uint32_t TIMER3_CC0ROUTE;
	volatile uint32_t TIMER3_CC1ROUTE;
	volatile uint32_t TIMER3_CC2ROUTE;
	volatile uint32_t TIMER3_CDTI0ROUTE;
	volatile uint32_t TIMER3_CDTI1ROUTE;
	volatile uint32_t TIMER3_CDTI2ROUTE;
	uint32_t RESERVED27[1];
	volatile uint32_t TIMER4_ROUTEEN;
	volatile uint32_t TIMER4_CC0ROUTE;
	volatile uint32_t TIMER4_CC1ROUTE;
	volatile uint32_t TIMER4_CC2ROUTE;
	volatile uint32_t TIMER4_CDTI0ROUTE;
	volatile uint32_t TIMER4_CDTI1ROUTE;
	volatile uint32_t TIMER4_CDTI2ROUTE;
	uint32_t RESERVED28[1];
	volatile uint32_t USART0_ROUTEEN;
	volatile uint32_t USART0_CSROUTE;
	volatile uint32_t USART0_CTSROUTE;
	volatile uint32_t USART0_RTSROUTE;
	volatile uint32_t USART0_RXROUTE;
	volatile uint32_t USART0_CLKROUTE;
	volatile uint32_t USART0_TXROUTE;
	uint32_t RESERVED29[1];
	volatile uint32_t USART1_ROUTEEN;
	volatile uint32_t USART1_CSROUTE;
	volatile uint32_t USART1_CTSROUTE;
	volatile uint32_t USART1_RTSROUTE;
	volatile uint32_t USART1_RXROUTE;
	volatile uint32_t USART1_CLKROUTE;
	volatile uint32_t USART1_TXROUTE;
}__attribute__((packed)) GPIO_TypeDef;

#define GPIO                           ((GPIO_TypeDef *)0x4003C000)
#define GPIO_SET                       ((GPIO_TypeDef *)0x4003D000)
#define GPIO_CLR                       ((GPIO_TypeDef *)0x4003E000)
#define GPIO_TGL                       ((GPIO_TypeDef *)0x4003F000)

// GPIO_PX
#define GPIO_P0                        (((uint32_t)0x01) << 0)
#define GPIO_P1                        (((uint32_t)0x01) << 1)
#define GPIO_P2                        (((uint32_t)0x01) << 2)
#define GPIO_P3                        (((uint32_t)0x01) << 3)
#define GPIO_P4                        (((uint32_t)0x01) << 4)
#define GPIO_P5                        (((uint32_t)0x01) << 5)
#define GPIO_P6                        (((uint32_t)0x01) << 6)
#define GPIO_P7                        (((uint32_t)0x01) << 7)
#define GPIO_P8                        (((uint32_t)0x01) << 8)

#define GPIO_CTRL_SLEWRATE             (((uint32_t)0x07) << 4)
#define GPIO_CTRL_DINDIS               (((uint32_t)0x01) << 12)
#define GPIO_CTRL_SLEWRATEALT          (((uint32_t)0x07) << 20)
#define GPIO_CTRL_DINDISALT            (((uint32_t)0x01) << 28)

#define GPIO_MODEL_MODE0               (((uint32_t)0x0F) << 0)
#define GPIO_MODEL_MODE1               (((uint32_t)0x0F) << 4)
#define GPIO_MODEL_MODE2               (((uint32_t)0x0F) << 8)
#define GPIO_MODEL_MODE3               (((uint32_t)0x0F) << 12)
#define GPIO_MODEL_MODE4               (((uint32_t)0x0F) << 16)
#define GPIO_MODEL_MODE5               (((uint32_t)0x0F) << 20)
#define GPIO_MODEL_MODE6               (((uint32_t)0x0F) << 24)
#define GPIO_MODEL_MODE7               (((uint32_t)0x0F) << 28)

#define GPIO_MODEH_MODE0               (((uint32_t)0x0F) << 0)

#define GPIO_DOUT_DOUT                 (((uint32_t)0x01FF) << 0)

#define GPIO_DIN_DIN                   (((uint32_t)0x01FF) << 0)

#define GPIO_LOCK_GPIO_LOCK            (((uint32_t)0xFFFF) << 0)

#define GPIO_GPIOLOCKSTATUS_LOCK       (((uint32_t)0x01) << 0)

#define GPIO_BUSALLOC_EVEN0            (((uint32_t)0x0F) << 0)
#define GPIO_BUSALLOC_EVEN1            (((uint32_t)0x0F) << 8)
#define GPIO_BUSALLOC_ODD0             (((uint32_t)0x0F) << 16)
#define GPIO_BUSALLOC_ODD1             (((uint32_t)0x0F) << 24)

#define GPIO_EXTIPSELL_EXTIPSEL0       (((uint32_t)0x03) << 0)
#define GPIO_EXTIPSELL_EXTIPSEL1       (((uint32_t)0x03) << 4)
#define GPIO_EXTIPSELL_EXTIPSEL2       (((uint32_t)0x03) << 8)
#define GPIO_EXTIPSELL_EXTIPSEL3       (((uint32_t)0x03) << 12)
#define GPIO_EXTIPSELL_EXTIPSEL4       (((uint32_t)0x03) << 16)
#define GPIO_EXTIPSELL_EXTIPSEL5       (((uint32_t)0x03) << 20)
#define GPIO_EXTIPSELL_EXTIPSEL6       (((uint32_t)0x03) << 24)
#define GPIO_EXTIPSELL_EXTIPSEL7       (((uint32_t)0x03) << 28)

#define GPIO_EXTIPSELH_EXTIPSEL0       (((uint32_t)0x03) << 0)
#define GPIO_EXTIPSELH_EXTIPSEL1       (((uint32_t)0x03) << 4)
#define GPIO_EXTIPSELH_EXTIPSEL2       (((uint32_t)0x03) << 8)
#define GPIO_EXTIPSELH_EXTIPSEL3      (((uint32_t)0x03) << 12)

#define GPIO_EXTIPINSELL_EXTIPINSEL0   (((uint32_t)0x03) << 0)
#define GPIO_EXTIPINSELL_EXTIPINSEL1   (((uint32_t)0x03) << 4)
#define GPIO_EXTIPINSELL_EXTIPINSEL2   (((uint32_t)0x03) << 8)
#define GPIO_EXTIPINSELL_EXTIPINSEL3   (((uint32_t)0x03) << 12)
#define GPIO_EXTIPINSELL_EXTIPINSEL4   (((uint32_t)0x03) << 16)
#define GPIO_EXTIPINSELL_EXTIPINSEL5   (((uint32_t)0x03) << 20)
#define GPIO_EXTIPINSELL_EXTIPINSEL6   (((uint32_t)0x03) << 24)
#define GPIO_EXTIPINSELL_EXTIPINSEL7   (((uint32_t)0x03) << 28)

#define GPIO_EXTIPINSELH_EXTIPINSEL0   (((uint32_t)0x03) << 0)
#define GPIO_EXTIPINSELH_EXTIPINSEL1   (((uint32_t)0x03) << 4)
#define GPIO_EXTIPINSELH_EXTIPINSEL2   (((uint32_t)0x03) << 8)
#define GPIO_EXTIPINSELH_EXTIPINSEL3   (((uint32_t)0x03) << 12)

#define GPIO_EXTIRISE_EXTIRISE         (((uint32_t)0x0FFF) << 0)

#define GPIO_EXTIFALL_EXTIFALL         (((uint32_t)0x0FFF) << 0)

#define GPIO_IF_EXTIF0                 (((uint32_t)0x01) << 0)
#define GPIO_IF_EXTIF1                 (((uint32_t)0x01) << 1)
#define GPIO_IF_EXTIF2                 (((uint32_t)0x01) << 2)
#define GPIO_IF_EXTIF3                 (((uint32_t)0x01) << 3)
#define GPIO_IF_EXTIF4                 (((uint32_t)0x01) << 4)
#define GPIO_IF_EXTIF5                 (((uint32_t)0x01) << 5)
#define GPIO_IF_EXTIF6                 (((uint32_t)0x01) << 6)
#define GPIO_IF_EXTIF7                 (((uint32_t)0x01) << 7)
#define GPIO_IF_EXTIF8                 (((uint32_t)0x01) << 8)
#define GPIO_IF_EXTIF9                 (((uint32_t)0x01) << 9)
#define GPIO_IF_EXTIF10                (((uint32_t)0x01) << 10)
#define GPIO_IF_EXTIF11                (((uint32_t)0x01) << 11)
#define GPIO_IF_EM4WU                  (((uint32_t)0x0FFF) << 16)

#define GPIO_IEN_EXTIEN0               (((uint32_t)0x01) << 0)
#define GPIO_IEN_EXTIEN1               (((uint32_t)0x01) << 1)
#define GPIO_IEN_EXTIEN2               (((uint32_t)0x01) << 2)
#define GPIO_IEN_EXTIEN3               (((uint32_t)0x01) << 3)
#define GPIO_IEN_EXTIEN4               (((uint32_t)0x01) << 4)
#define GPIO_IEN_EXTIEN5               (((uint32_t)0x01) << 5)
#define GPIO_IEN_EXTIEN6               (((uint32_t)0x01) << 6)
#define GPIO_IEN_EXTIEN7               (((uint32_t)0x01) << 7)
#define GPIO_IEN_EXTIEN8               (((uint32_t)0x01) << 8)
#define GPIO_IEN_EXTIEN9               (((uint32_t)0x01) << 9)
#define GPIO_IEN_EXTIEN10              (((uint32_t)0x01) << 10)
#define GPIO_IEN_EXTIEN11              (((uint32_t)0x01) << 11)
#define GPIO_IEN_EM4WUIEN0             (((uint32_t)0x01) << 16)
#define GPIO_IEN_EM4WUIEN1             (((uint32_t)0x01) << 17)
#define GPIO_IEN_EM4WUIEN2             (((uint32_t)0x01) << 18)
#define GPIO_IEN_EM4WUIEN3             (((uint32_t)0x01) << 19)
#define GPIO_IEN_EM4WUIEN4             (((uint32_t)0x01) << 20)
#define GPIO_IEN_EM4WUIEN5             (((uint32_t)0x01) << 21)
#define GPIO_IEN_EM4WUIEN6             (((uint32_t)0x01) << 22)
#define GPIO_IEN_EM4WUIEN7             (((uint32_t)0x01) << 23)
#define GPIO_IEN_EM4WUIEN8             (((uint32_t)0x01) << 24)
#define GPIO_IEN_EM4WUIEN9             (((uint32_t)0x01) << 25)
#define GPIO_IEN_EM4WUIEN10            (((uint32_t)0x01) << 26)
#define GPIO_IEN_EM4WUIEN11            (((uint32_t)0x01) << 27)

#define GPIO_EM4WUEN_EM4WUEN           (((uint32_t)0x0FFF) << 16)

#define GPIO_EM4WUPOL_EM4WUPOL         (((uint32_t)0x0FFF) << 16)

#define GPIO_DBGROUTEPEN_SWCLKTCKPEN   (((uint32_t)0x01) << 0)
#define GPIO_DBGROUTEPEN_SWDIOTMSPEN   (((uint32_t)0x01) << 1)
#define GPIO_DBGROUTEPEN_TDOPEN        (((uint32_t)0x01) << 2)
#define GPIO_DBGROUTEPEN_TDIPEN        (((uint32_t)0x01) << 3)

#define GPIO_TRACEROUTEPEN_SWVPEN      (((uint32_t)0x01) << 0)
#define GPIO_TRACEROUTEPEN_TRACECLKPEN (((uint32_t)0x01) << 1)
#define GPIO_TRACEROUTEPEN_TRACEDATA0PEN (((uint32_t)0x01) << 2)

#define GPIO_CMU_ROUTEEN_CLKOUT0PEN    (((uint32_t)0x01) << 0)
#define GPIO_CMU_ROUTEEN_CLKOUT1PEN    (((uint32_t)0x01) << 1)
#define GPIO_CMU_ROUTEEN_CLKOUT2PEN    (((uint32_t)0x01) << 2)

#define GPIO_CMU_CLKIN0ROUTE_PORT      (((uint32_t)0x03) << 0)
#define GPIO_CMU_CLKIN0ROUTE_PIN       (((uint32_t)0x0F) << 16)

#define GPIO_CMU_CLKOUT0ROUTE_PORT     (((uint32_t)0x03) << 0)
#define GPIO_CMU_CLKOUT0ROUTE_PIN      (((uint32_t)0x0F) << 16)

#define GPIO_CMU_CLKOUT1ROUTE_PORT     (((uint32_t)0x03) << 0)
#define GPIO_CMU_CLKOUT1ROUTE_PIN      (((uint32_t)0x0F) << 16)

#define GPIO_CMU_CLKOUT2ROUTE_PORT     (((uint32_t)0x03) << 0)
#define GPIO_CMU_CLKOUT2ROUTE_PIN      (((uint32_t)0x0F) << 16)

#define GPIO_DCDC_ROUTEEN_DCDCCOREHIDDENPEN (((uint32_t)0x01) << 0)

#define GPIO_I2C_ROUTEEN_SCLPEN       (((uint32_t)0x01) << 0)
#define GPIO_I2C_ROUTEEN_SDAPEN       (((uint32_t)0x01) << 1)

#define GPIO_I2C_SCLROUTE_PORT        (((uint32_t)0x03) << 0)
#define GPIO_I2C_SCLROUTE_PIN         (((uint32_t)0x0F) << 16)

#define GPIO_I2C_SDAROUTE_PORT        (((uint32_t)0x03) << 0)
#define GPIO_I2C_SDAROUTE_PIN         (((uint32_t)0x0F) << 16)

#define GPIO_LETIMER0_ROUTEEN_OUT0PEN  (((uint32_t)0x01) << 0)
#define GPIO_LETIMER0_ROUTEEN_OUT1PEN  (((uint32_t)0x01) << 1)

#define GPIO_LETIMER0_OUT0ROUTE_PORT   (((uint32_t)0x03) << 0)
#define GPIO_LETIMER0_OUT0ROUTE_PIN    (((uint32_t)0x0F) << 16)

#define GPIO_LETIMER0_OUT1ROUTE_PORT   (((uint32_t)0x03) << 0)
#define GPIO_LETIMER0_OUT1ROUTE_PIN    (((uint32_t)0x0F) << 16)

#define GPIO_EUART0_ROUTEEN_RTSPEN     (((uint32_t)0x01) << 0)
#define GPIO_EUART0_ROUTEEN_TXPEN      (((uint32_t)0x01) << 1)

#define GPIO_EUART0_CTSROUTE_PORT      (((uint32_t)0x03) << 0)
#define GPIO_EUART0_CTSROUTE_PIN       (((uint32_t)0x0F) << 16)

#define GPIO_EUART0_RTSROUTE_PORT      (((uint32_t)0x03) << 0)
#define GPIO_EUART0_RTSROUTE_PIN       (((uint32_t)0x0F) << 16)

#define GPIO_EUART0_RXROUTE_PORT       (((uint32_t)0x03) << 0)
#define GPIO_EUART0_RXROUTE_PIN        (((uint32_t)0x0F) << 16)

#define GPIO_EUART0_TXROUTE_PORT       (((uint32_t)0x03) << 0)
#define GPIO_EUART0_TXROUTE_PIN        (((uint32_t)0x0F) << 16)

#define GPIO_PDM_ROUTEEN_CLKPEN        (((uint32_t)0x01) << 0)

#define GPIO_PDM_CLKROUTE_PORT         (((uint32_t)0x03) << 0)
#define GPIO_PDM_CLKROUTE_PIN          (((uint32_t)0x0F) << 16)

#define GPIO_PDM_DAT0ROUTE_PORT        (((uint32_t)0x03) << 0)
#define GPIO_PDM_DAT0ROUTE_PIN         (((uint32_t)0x0F) << 16)

#define GPIO_PDM_DAT1ROUTE_PORT        (((uint32_t)0x03) << 0)
#define GPIO_PDM_DAT1ROUTE_PIN         (((uint32_t)0x0F) << 16)

#define GPIO_PRS0_ROUTEEN_ASYNCH0PEN   (((uint32_t)0x01) << 0)
#define GPIO_PRS0_ROUTEEN_ASYNCH1PEN   (((uint32_t)0x01) << 1)
#define GPIO_PRS0_ROUTEEN_ASYNCH2PEN   (((uint32_t)0x01) << 2)
#define GPIO_PRS0_ROUTEEN_ASYNCH3PEN   (((uint32_t)0x01) << 3)
#define GPIO_PRS0_ROUTEEN_ASYNCH4PEN   (((uint32_t)0x01) << 4)
#define GPIO_PRS0_ROUTEEN_ASYNCH5PEN   (((uint32_t)0x01) << 5)
#define GPIO_PRS0_ROUTEEN_ASYNCH6PEN   (((uint32_t)0x01) << 6)
#define GPIO_PRS0_ROUTEEN_ASYNCH7PEN   (((uint32_t)0x01) << 7)
#define GPIO_PRS0_ROUTEEN_ASYNCH8PEN   (((uint32_t)0x01) << 8)
#define GPIO_PRS0_ROUTEEN_ASYNCH9PEN   (((uint32_t)0x01) << 9)
#define GPIO_PRS0_ROUTEEN_ASYNCH10PEN  (((uint32_t)0x01) << 10)
#define GPIO_PRS0_ROUTEEN_ASYNCH11PEN  (((uint32_t)0x01) << 11)
#define GPIO_PRS0_ROUTEEN_SYNCH0PEN    (((uint32_t)0x01) << 12)
#define GPIO_PRS0_ROUTEEN_SYNCH1PEN    (((uint32_t)0x01) << 13)
#define GPIO_PRS0_ROUTEEN_SYNCH2PEN    (((uint32_t)0x01) << 14)
#define GPIO_PRS0_ROUTEEN_SYNCH3PEN    (((uint32_t)0x01) << 15)

#define GPIO_PRS0_ASYNCH0ROUTE_PORT    (((uint32_t)0x03) << 0)
#define GPIO_PRS0_ASYNCH0ROUTE_PIN     (((uint32_t)0x0F) << 16)

#define GPIO_PRS0_ASYNCH1ROUTE_PORT    (((uint32_t)0x03) << 0)
#define GPIO_PRS0_ASYNCH1ROUTE_PIN     (((uint32_t)0x0F) << 16)

#define GPIO_PRS0_ASYNCH2ROUTE_PORT    (((uint32_t)0x03) << 0)
#define GPIO_PRS0_ASYNCH2ROUTE_PIN     (((uint32_t)0x0F) << 16)

#define GPIO_PRS0_ASYNCH3ROUTE_PORT    (((uint32_t)0x03) << 0)
#define GPIO_PRS0_ASYNCH3ROUTE_PIN     (((uint32_t)0x0F) << 16)

#define GPIO_PRS0_ASYNCH4ROUTE_PORT    (((uint32_t)0x03) << 0)
#define GPIO_PRS0_ASYNCH4ROUTE_PIN     (((uint32_t)0x0F) << 16)

#define GPIO_PRS0_ASYNCH5ROUTE_PORT    (((uint32_t)0x03) << 0)
#define GPIO_PRS0_ASYNCH5ROUTE_PIN     (((uint32_t)0x0F) << 16)

#define GPIO_PRS0_ASYNCH6ROUTE_PORT    (((uint32_t)0x03) << 0)
#define GPIO_PRS0_ASYNCH6ROUTE_PIN     (((uint32_t)0x0F) << 16)

#define GPIO_PRS0_ASYNCH7ROUTE_PORT    (((uint32_t)0x03) << 0)
#define GPIO_PRS0_ASYNCH7ROUTE_PIN     (((uint32_t)0x0F) << 16)

#define GPIO_PRS0_ASYNCH8ROUTE_PORT    (((uint32_t)0x03) << 0)
#define GPIO_PRS0_ASYNCH8ROUTE_PIN     (((uint32_t)0x0F) << 16)

#define GPIO_PRS0_ASYNCH9ROUTE_PORT    (((uint32_t)0x03) << 0)
#define GPIO_PRS0_ASYNCH9ROUTE_PIN     (((uint32_t)0x0F) << 16)

#define GPIO_PRS0_ASYNCH10ROUTE_PORT   (((uint32_t)0x03) << 0)
#define GPIO_PRS0_ASYNCH10ROUTE_PIN    (((uint32_t)0x0F) << 16)

#define GPIO_PRS0_ASYNCH11ROUTE_PORT   (((uint32_t)0x03) << 0)
#define GPIO_PRS0_ASYNCH11ROUTE_PIN    (((uint32_t)0x0F) << 16)

#define GPIO_PRS0_SYNCH0ROUTE_PORT     (((uint32_t)0x03) << 0)
#define GPIO_PRS0_SYNCH0ROUTE_PIN      (((uint32_t)0x0F) << 16)

#define GPIO_PRS0_SYNCH0ROUTE_PORT     (((uint32_t)0x03) << 0)
#define GPIO_PRS0_SYNCH0ROUTE_PIN      (((uint32_t)0x0F) << 16)

#define GPIO_PRS0_SYNCH1ROUTE_PORT     (((uint32_t)0x03) << 0)
#define GPIO_PRS0_SYNCH1ROUTE_PIN      (((uint32_t)0x0F) << 16)

#define GPIO_PRS0_SYNCH2ROUTE_PORT     (((uint32_t)0x03) << 0)
#define GPIO_PRS0_SYNCH2ROUTE_PIN      (((uint32_t)0x0F) << 16)

#define GPIO_PRS0_SYNCH3ROUTE_PORT     (((uint32_t)0x03) << 0)
#define GPIO_PRS0_SYNCH3ROUTE_PIN      (((uint32_t)0x0F) << 16)

#define GPIO_TIMER_ROUTEEN_CC0PEN      (((uint32_t)0x01) << 0)
#define GPIO_TIMER_ROUTEEN_CC1PEN      (((uint32_t)0x01) << 1)
#define GPIO_TIMER_ROUTEEN_CC2PEN      (((uint32_t)0x01) << 2)
#define GPIO_TIMER_ROUTEEN_CCC0PEN     (((uint32_t)0x01) << 3)
#define GPIO_TIMER_ROUTEEN_CCC1PEN     (((uint32_t)0x01) << 4)
#define GPIO_TIMER_ROUTEEN_CCC2PEN     (((uint32_t)0x01) << 5)

#define GPIO_TIMER_CC0ROUTE_PORT       (((uint32_t)0x03) << 0)
#define GPIO_TIMER_CC0ROUTE_PIN        (((uint32_t)0x0F) << 16)

#define GPIO_TIMER_CC1ROUTE_PORT       (((uint32_t)0x03) << 0)
#define GPIO_TIMER_CC1ROUTE_PIN        (((uint32_t)0x0F) << 16)

#define GPIO_TIMER_CC2ROUTE_PORT       (((uint32_t)0x03) << 0)
#define GPIO_TIMER_CC2ROUTE_PIN        (((uint32_t)0x0F) << 16)

#define GPIO_TIMER_CDTI0ROUTE_PORT     (((uint32_t)0x03) << 0)
#define GPIO_TIMER_CDTI0ROUTE_PIN      (((uint32_t)0x0F) << 16)

#define GPIO_TIMER_CDTI1ROUTE_PORT     (((uint32_t)0x03) << 0)
#define GPIO_TIMER_CDTI1ROUTE_PIN      (((uint32_t)0x0F) << 16)

#define GPIO_TIMER_CDTI2ROUTE_PORT     (((uint32_t)0x03) << 0)
#define GPIO_TIMER_CDTI2ROUTE_PIN      (((uint32_t)0x0F) << 16)

#define GPIO_USART_ROUTEEN_CSPEN       (((uint32_t)0x01) << 0)
#define GPIO_USART_ROUTEEN_RTSPEN      (((uint32_t)0x01) << 1)
#define GPIO_USART_ROUTEEN_RXPEN       (((uint32_t)0x01) << 2)
#define GPIO_USART_ROUTEEN_CLKPEN      (((uint32_t)0x01) << 3)
#define GPIO_USART_ROUTEEN_TXPEN       (((uint32_t)0x01) << 4)

#define GPIO_USART_CSROUTE_PORT       (((uint32_t)0x03) << 0)
#define GPIO_USART_CSROUTE_PIN        (((uint32_t)0x0F) << 16)

#define GPIO_USART_CTSROUTE_PORT       (((uint32_t)0x03) << 0)
#define GPIO_USART_CTSROUTE_PIN        (((uint32_t)0x0F) << 16)

#define GPIO_USART_RTSROUTE_PORT       (((uint32_t)0x03) << 0)
#define GPIO_USART_RTSROUTE_PIN        (((uint32_t)0x0F) << 16)

#define GPIO_USART_RXROUTE_PORT        (((uint32_t)0x03) << 0)
#define GPIO_USART_RXROUTE_PIN         (((uint32_t)0x0F) << 16)

#define GPIO_USART_CLKROUTE_PORT       (((uint32_t)0x03) << 0)
#define GPIO_USART_CLKROUTE_PIN        (((uint32_t)0x0F) << 16)

#define GPIO_USART_TXROUTE_PORT        (((uint32_t)0x03) << 0)
#define GPIO_USART_TXROUTE_PIN         (((uint32_t)0x0F) << 16)

#endif
