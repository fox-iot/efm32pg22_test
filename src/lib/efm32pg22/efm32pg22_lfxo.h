
#ifndef _EFM32PG22_LFXO_H_
#define _EFM32PG22_LFXO_H_

#include <stdint.h>

typedef struct{
	volatile uint32_t IPVERSION;
	volatile uint32_t CTRL;
	volatile uint32_t CFG;
	uint32_t RESERVED1[1];
	volatile uint32_t STATUS;
	volatile uint32_t CAL;
	volatile uint32_t IF;
	volatile uint32_t IEN;
	volatile uint32_t SYNCBUSY;
	volatile uint32_t LOCK;

}__attribute__((packed)) LFXO_TypeDef;

#define LFXO                           ((LFXO_TypeDef *)0x40020000)
#define LFXO_SET                       ((LFXO_TypeDef *)0x40021000)
#define LFXO_CLR                       ((LFXO_TypeDef *)0x40022000)
#define LFXO_TGL                       ((LFXO_TypeDef *)0x40023000)

#define LFXO_IPVERSION_IPVERSION       (((uint32_t)0xFFFFFFFF) << 0)

#define LFXO_CTRL_FORCEEN              (((uint32_t)0x01) << 0)
#define LFXO_CTRL_DISONDEMAND          (((uint32_t)0x01) << 1)
#define LFXO_CTRL_FAILDETEN            (((uint32_t)0x01) << 4)
#define LFXO_CTRL_FAILDETEM4WUEN       (((uint32_t)0x01) << 5)

#define LFXO_CFG_AGC                   (((uint32_t)0x01) << 0)
#define LFXO_CFG_HIGHAMPL              (((uint32_t)0x01) << 1)
#define LFXO_CFG_MODE                  (((uint32_t)0x03) << 4)
#define LFXO_CFG_TIMEOUT               (((uint32_t)0x07) << 8)

#define LFXO_STATUS_RDY                (((uint32_t)0x01) << 0)
#define LFXO_STATUS_ENS                (((uint32_t)0x01) << 16)
#define LFXO_STATUS_LOCK               (((uint32_t)0x01) << 31)

#define LFXO_CAL_CAPTUNE               (((uint32_t)0x7F) << 0)
#define LFXO_CAL_GAIN                  (((uint32_t)0x03) << 8)

#define LFXO_IF_RDY                    (((uint32_t)0x01) << 0)
#define LFXO_IF_POSEDGE                (((uint32_t)0x01) << 1)
#define LFXO_IF_NEGEDGE                (((uint32_t)0x01) << 2)
#define LFXO_IF_FAIL                   (((uint32_t)0x01) << 3)

#define LFXO_IEN_RDY                   (((uint32_t)0x01) << 0)
#define LFXO_IEN_POSEDGE               (((uint32_t)0x01) << 1)
#define LFXO_IEN_NEGEDGE               (((uint32_t)0x01) << 2)
#define LFXO_IEN_FAIL                  (((uint32_t)0x01) << 3)

#define LFXO_SYNCBUSY_CAL              (((uint32_t)0x01) << 0)

#define LFXO_LOCK_LOCKKEY              (((uint32_t)0xFFFF) << 0)

#endif
