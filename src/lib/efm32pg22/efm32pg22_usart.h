#ifndef _EFM32PG22_USART_H_
#define _EFM32PG22_USART_H_

#include <stdint.h>

typedef struct{
        volatile uint32_t IPVERSION;
        volatile uint32_t EN;
        volatile uint32_t CTRL;
        volatile uint32_t FRAME;
        volatile uint32_t TRIGCTRL;
        volatile uint32_t CMD;
        volatile uint32_t STATUS;
        volatile uint32_t CLKDIV;
        volatile uint32_t RXDATAX;
        volatile uint32_t RXDATA;
        volatile uint32_t RXDOUBLEX;
        volatile uint32_t RXDOUBLE;
        volatile uint32_t RXDATAXP;
        volatile uint32_t RXDOUBLEXP;
        volatile uint32_t TXDATAX;
        volatile uint32_t TXDATA;
        volatile uint32_t TXDOUBLEX;
        volatile uint32_t TXDOUBLE;
        volatile uint32_t IF;
        volatile uint32_t IEN;
        volatile uint32_t IRCTRL;
        volatile uint32_t I2SCTRL;
        volatile uint32_t TIMING;
        volatile uint32_t CTRLX;
        volatile uint32_t TIMECMP0;
        volatile uint32_t TIMECMP1;
        volatile uint32_t TIMECMP2;
}__attribute__((packed)) USART_TypeDef;

#define USART0                    ((USART_TypeDef *)0x4005C000)
#define USART0_SET                ((USART_TypeDef *)0x4005D000)
#define USART0_CLR                ((USART_TypeDef *)0x4005E000)
#define USART0_TGL                ((USART_TypeDef *)0x4005F000)

#define USART1                    ((USART_TypeDef *)0x40060000)
#define USART1_SET                ((USART_TypeDef *)0x40061000)
#define USART1_CLR                ((USART_TypeDef *)0x40062000)
#define USART1_TGL                ((USART_TypeDef *)0x40063000)

#define USART_IPVERSION_IPVERSION (((uint32_t)0xFFFFFFFF) << 0)

#define USART_EN_EN               (((uint32_t)0x01) << 0)
 
#define USART_CTRL_SYNC           (((uint32_t)0x01) << 0)
#define USART_CTRL_LOOPBK         (((uint32_t)0x01) << 1)
#define USART_CTRL_CCEN           (((uint32_t)0x01) << 2)
#define USART_CTRL_MPM            (((uint32_t)0x01) << 3)
#define USART_CTRL_MPAB           (((uint32_t)0x01) << 4)
#define USART_CTRL_OVS            (((uint32_t)0x03) << 5)
#define USART_CTRL_CLKPOL         (((uint32_t)0x01) << 8)
#define USART_CTRL_CLKPHA         (((uint32_t)0x01) << 9)
#define USART_CTRL_MSBF           (((uint32_t)0x01) << 10)
#define USART_CTRL_CSMA           (((uint32_t)0x01) << 11)
#define USART_CTRL_TXBIL          (((uint32_t)0x01) << 12)
#define USART_CTRL_RXINV          (((uint32_t)0x01) << 13)
#define USART_CTRL_TXINV          (((uint32_t)0x01) << 14)
#define USART_CTRL_CSINV          (((uint32_t)0x01) << 15)
#define USART_CTRL_AUTOCS         (((uint32_t)0x01) << 16)
#define USART_CTRL_AUTOTRI        (((uint32_t)0x01) << 17)
#define USART_CTRL_SCMODE         (((uint32_t)0x01) << 18)
#define USART_CTRL_SCRETRANS      (((uint32_t)0x01) << 19)
#define USART_CTRL_SKIPPERRF      (((uint32_t)0x01) << 20)
#define USART_CTRL_BIT8DV         (((uint32_t)0x01) << 21)
#define USART_CTRL_ERRSDMA        (((uint32_t)0x01) << 22)
#define USART_CTRL_ERRSRX         (((uint32_t)0x01) << 23)
#define USART_CTRL_ERRSTX         (((uint32_t)0x01) << 24)
#define USART_CTRL_SSSEARLY       (((uint32_t)0x01) << 25)
#define USART_CTRL_BYTESWAP       (((uint32_t)0x01) << 28)
#define USART_CTRL_AUTOTX         (((uint32_t)0x01) << 29)
#define USART_CTRL_MVDIS          (((uint32_t)0x01) << 30)
#define USART_CTRL_SMSDELAY       (((uint32_t)0x01) << 31)

#define USART_FRAME_DATABITS      (((uint32_t)0x0F) << 0)
#define USART_FRAME_PARITY        (((uint32_t)0x03) << 8)
#define USART_FRAME_STOPBITS      (((uint32_t)0x03) << 12)

#define USART_TRIGCTRL_RXTEN      (((uint32_t)0x01) << 4)
#define USART_TRIGCTRL_TXTEN      (((uint32_t)0x01) << 5)
#define USART_TRIGCTRL_AUTOTXTEN  (((uint32_t)0x01) << 6)
#define USART_TRIGCTRL_TXARX0EN   (((uint32_t)0x01) << 7)
#define USART_TRIGCTRL_TXARX1EN   (((uint32_t)0x01) << 8)
#define USART_TRIGCTRL_TXARX2EN   (((uint32_t)0x01) << 9)
#define USART_TRIGCTRL_RXATX0EN   (((uint32_t)0x01) << 10)
#define USART_TRIGCTRL_RXATX1EN   (((uint32_t)0x01) << 11)
#define USART_TRIGCTRL_RXATX2EN   (((uint32_t)0x01) << 12)

#define USART_CMD_RXEN            (((uint32_t)0x01) << 0)
#define USART_CMD_RXDIS           (((uint32_t)0x01) << 1)
#define USART_CMD_TXEN            (((uint32_t)0x01) << 2)
#define USART_CMD_TXDIS           (((uint32_t)0x01) << 3)
#define USART_CMD_MASTEREN        (((uint32_t)0x01) << 4)
#define USART_CMD_MASTERDIS       (((uint32_t)0x01) << 5)
#define USART_CMD_RXBLOCKEN       (((uint32_t)0x01) << 6)
#define USART_CMD_RXBLOCKDIS      (((uint32_t)0x01) << 7)
#define USART_CMD_TXTRIEN         (((uint32_t)0x01) << 8)
#define USART_CMD_TXTRIDIS        (((uint32_t)0x01) << 9)
#define USART_CMD_CLEARTX         (((uint32_t)0x01) << 10)
#define USART_CMD_CLEARRX         (((uint32_t)0x01) << 11)

#define USART_STATUS_RXENS        (((uint32_t)0x01) << 0)
#define USART_STATUS_TXENS        (((uint32_t)0x01) << 1)
#define USART_STATUS_MASTER       (((uint32_t)0x01) << 2)
#define USART_STATUS_RXBLOCK      (((uint32_t)0x01) << 3)
#define USART_STATUS_TXTRI        (((uint32_t)0x01) << 4)
#define USART_STATUS_TXC          (((uint32_t)0x01) << 5)
#define USART_STATUS_TXBL         (((uint32_t)0x01) << 6)
#define USART_STATUS_RXDATAV      (((uint32_t)0x01) << 7)
#define USART_STATUS_RXFULL       (((uint32_t)0x01) << 8)
#define USART_STATUS_TXBDRIGHT    (((uint32_t)0x01) << 9)
#define USART_STATUS_TXBSRIGHT    (((uint32_t)0x01) << 10)
#define USART_STATUS_RXDATAVRIGHT (((uint32_t)0x01) << 11)
#define USART_STATUS_RXFULLRIGHT  (((uint32_t)0x01) << 12)
#define USART_STATUS_TXIDLE       (((uint32_t)0x01) << 13)
#define USART_STATUS_TIMERRESTARTED (((uint32_t)0x01) << 14)
#define USART_STATUS_TXBUFCNT     (((uint32_t)0x03) << 16)

#define USART_CLKDIV_DIV          (((uint32_t)0xFFFFF) << 3)
#define USART_CLKDIV_AUTOBAUDEN   (((uint32_t)0x01) << 31)

#define USART_RXDATAX_RXDATA      (((uint32_t)0x01FF) << 0)
#define USART_RXDATAX_PERR        (((uint32_t)0x01) << 14)
#define USART_RXDATAX_FERR        (((uint32_t)0x01) << 15)

#define USART_RXDATA_RXDATA       (((uint32_t)0xFF) << 0)

#define USART_RXDOUBLEX_RXDATA0   (((uint32_t)0x01FF) << 0)
#define USART_RXDOUBLEX_PERR0     (((uint32_t)0x01) << 14)
#define USART_RXDOUBLEX_FERR0     (((uint32_t)0x01) << 15)
#define USART_RXDOUBLEX_RXDATA1   (((uint32_t)0x01FF) << 16)
#define USART_RXDOUBLEX_PERR1     (((uint32_t)0x01) << 30)
#define USART_RXDOUBLEX_FERR1     (((uint32_t)0x01) << 31)

#define USART_RXDOUBLE_RXDATA0    (((uint32_t)0xFF) << 0)
#define USART_RXDOUBLE_RXDATA1    (((uint32_t)0xFF) << 8)

#define USART_RXDATAXP_RXDATAP    (((uint32_t)0x01FF) << 0)
#define USART_RXDATAXP_PERRP      (((uint32_t)0x01) << 14)
#define USART_RXDATAXP_FERRP      (((uint32_t)0x01) << 15)

#define USART_RXDOUBLEXP_RXDATAP0 (((uint32_t)0x01FF) << 0)
#define USART_RXDOUBLEXP_PERRP0   (((uint32_t)0x01) << 14)
#define USART_RXDOUBLEXP_FERRP0   (((uint32_t)0x01) << 15)
#define USART_RXDOUBLEXP_RXDATAP1 (((uint32_t)0x01FF) << 16)
#define USART_RXDOUBLEXP_PERRP1   (((uint32_t)0x01) << 30)
#define USART_RXDOUBLEXP_FERRP1   (((uint32_t)0x01) << 31)

#define USART_TXDATAX_TXDATAX     (((uint32_t)0x01FF) << 0)
#define USART_TXDATAX_UBRXAT      (((uint32_t)0x01) << 11)
#define USART_TXDATAX_TXTRIAT     (((uint32_t)0x01) << 12)
#define USART_TXDATAX_TXBREAK     (((uint32_t)0x01) << 13)
#define USART_TXDATAX_TXDISAT     (((uint32_t)0x01) << 14)
#define USART_TXDATAX_RXENAT      (((uint32_t)0x01) << 15)

#define USART_TXDATA_TXDATA       (((uint32_t)0xFF) << 0)

#define USART_TXDOUBLEX_TXDATA0   (((uint32_t)0x01FF) << 0)
#define USART_TXDOUBLEX_UBRXAT0   (((uint32_t)0x01) << 11)
#define USART_TXDOUBLEX_TXTRIAT0  (((uint32_t)0x01) << 12)
#define USART_TXDOUBLEX_TXBREAK0  (((uint32_t)0x01) << 13)
#define USART_TXDOUBLEX_TXDISAT0  (((uint32_t)0x01) << 14)
#define USART_TXDOUBLEX_RXENAT0   (((uint32_t)0x01) << 15)
#define USART_TXDOUBLEX_TXDATA1   (((uint32_t)0x01FF) << 16)
#define USART_TXDOUBLEX_UBRXAT1   (((uint32_t)0x01) << 27)
#define USART_TXDOUBLEX_TXTRIAT1  (((uint32_t)0x01) << 28)
#define USART_TXDOUBLEX_TXBREAK1  (((uint32_t)0x01) << 29)
#define USART_TXDOUBLEX_TXDISAT1  (((uint32_t)0x01) << 30)
#define USART_TXDOUBLEX_RXENAT1   (((uint32_t)0x01) << 31)

#define USART_TXDOUBLE_TXDATA0    (((uint32_t)0xFF) << 0)
#define USART_TXDOUBLE_TXDATA1    (((uint32_t)0xFF) << 8)

#define USART_IF_TXC              (((uint32_t)0x01) << 0)
#define USART_IF_TXBL             (((uint32_t)0x01) << 1)
#define USART_IF_RXDATAV          (((uint32_t)0x01) << 2)
#define USART_IF_RXFULL           (((uint32_t)0x01) << 3)
#define USART_IF_RXOF             (((uint32_t)0x01) << 4)
#define USART_IF_RXUF             (((uint32_t)0x01) << 5)
#define USART_IF_TXOF             (((uint32_t)0x01) << 6)
#define USART_IF_TXUF             (((uint32_t)0x01) << 7)
#define USART_IF_PERR             (((uint32_t)0x01) << 8)
#define USART_IF_FERR             (((uint32_t)0x01) << 9)
#define USART_IF_MPAF             (((uint32_t)0x01) << 10)
#define USART_IF_SSM              (((uint32_t)0x01) << 11)
#define USART_IF_CCF              (((uint32_t)0x01) << 12)
#define USART_IF_TXIDLE           (((uint32_t)0x01) << 13)
#define USART_IF_TCMP0            (((uint32_t)0x01) << 14)
#define USART_IF_TCMP1            (((uint32_t)0x01) << 15)
#define USART_IF_TCMP2            (((uint32_t)0x01) << 16)

#define USART_IEN_TXC             (((uint32_t)0x01) << 0)
#define USART_IEN_TXBL            (((uint32_t)0x01) << 1)
#define USART_IEN_RXDATAV         (((uint32_t)0x01) << 2)
#define USART_IEN_RXFULL          (((uint32_t)0x01) << 3)
#define USART_IEN_RXOF            (((uint32_t)0x01) << 4)
#define USART_IEN_RXUF            (((uint32_t)0x01) << 5)
#define USART_IEN_TXOF            (((uint32_t)0x01) << 6)
#define USART_IEN_TXUF            (((uint32_t)0x01) << 7)
#define USART_IEN_PERR            (((uint32_t)0x01) << 8)
#define USART_IEN_FERR            (((uint32_t)0x01) << 9)
#define USART_IEN_MPAF            (((uint32_t)0x01) << 10)
#define USART_IEN_SSM             (((uint32_t)0x01) << 11)
#define USART_IEN_CCF             (((uint32_t)0x01) << 12)
#define USART_IEN_TXIDLE          (((uint32_t)0x01) << 13)
#define USART_IEN_TCMP0           (((uint32_t)0x01) << 14)
#define USART_IEN_TCMP1           (((uint32_t)0x01) << 15)
#define USART_IEN_TCMP2           (((uint32_t)0x01) << 16)

#define USART_IRCTRL_IREN         (((uint32_t)0x01) << 0)
#define USART_IRCTRL_IRPW         (((uint32_t)0x03) << 1)
#define USART_IRCTRL_IRFILT       (((uint32_t)0x01) << 3)

#define USART_I2SCTRL_EN          (((uint32_t)0x01) << 0)
#define USART_I2SCTRL_MONO        (((uint32_t)0x01) << 1)
#define USART_I2SCTRL_JUSTIFY     (((uint32_t)0x01) << 2)
#define USART_I2SCTRL_DMASPLIT    (((uint32_t)0x01) << 3)
#define USART_I2SCTRL_DELAY       (((uint32_t)0x01) << 4)
#define USART_I2SCTRL_FORMAT      (((uint32_t)0x07) << 8)

#define USART_TIMING_TXDELAY      (((uint32_t)0x07) << 16)
#define USART_TIMING_CSSETUP      (((uint32_t)0x07) << 20)
#define USART_TIMING_ICS          (((uint32_t)0x07) << 24)
#define USART_TIMING_CSHOLD       (((uint32_t)0x07) << 28)

#define USART_CTRLX_DBGHALT       (((uint32_t)0x01) << 0)
#define USART_CTRLX_CTSINV        (((uint32_t)0x01) << 1) 
#define USART_CTRLX_CTSEN         (((uint32_t)0x01) << 2)
#define USART_CTRLX_RTSINV        (((uint32_t)0x01) << 3)
#define USART_CTRLX_RXPRSEN       (((uint32_t)0x01) << 7)
#define USART_CTRLX_CLKPRSEN      (((uint32_t)0x01) << 15)

#define USART_TIMECMP0_TCMPVAL    (((uint32_t)0xFF) << 0)
#define USART_TIMECMP0_TSTART     (((uint32_t)0x07) << 16)
#define USART_TIMECMP0_TSTOP      (((uint32_t)0x07) << 20)
#define USART_TIMECMP0_RESTARTEN  (((uint32_t)0x01) << 24)

#define USART_TIMECMP1_TCMPVAL    (((uint32_t)0xFF) << 0)
#define USART_TIMECMP1_TSTART     (((uint32_t)0x07) << 16)
#define USART_TIMECMP1_TSTOP      (((uint32_t)0x07) << 20)
#define USART_TIMECMP1_RESTARTEN  (((uint32_t)0x01) << 24)

#define USART_TIMECMP2_TCMPVAL    (((uint32_t)0xFF) << 0)
#define USART_TIMECMP2_TSTART     (((uint32_t)0x07) << 16)
#define USART_TIMECMP2_TSTOP      (((uint32_t)0x07) << 20)
#define USART_TIMECMP2_RESTARTEN  (((uint32_t)0x01) << 24)

#endif
