#include "platform.h"
#include "platform_0x20.h"
#include "efm32pg22.h"
#include "mask.h"

void platform_init_target(void)
{
	/** Test controller pins
	 * LEDs
	 *   GREEN  PC0
	 *   RED    PD0
	 * 
	 * Analog inputs
	 *   Input voltage       PC01
	 *   IN1                 PC05
	 *   IN2                 PB02
	 *   OUT1 current consmp PA04
	 * 
	 * Digital inputs
	 *   IN1   PC06
	 *   IN2   PB03
	 *   IN3   PB00
	 * 
	 * Input port pull-ups - Activates the internal input resistor with 
	 * controlled voltage (7,9171V)
	 *   PU1   PC07
	 *   PU2   PB04
	 *   PU3   PB01
	 * 
	 * Digital output, type: open-drain, max current: 1.5A
	 *   OUT   PA05
	 *   OVC...PA03
	 */

	CMU->CLKEN0 |= CMU_CLKEN0_GPIO;

	/* LEDS */
	mask32_set(GPIO->PORTC_MODEL, GPIO_MODEL_MODE0, 4);
	GPIO_CLR->PORTC_DOUT = GPIO_P0;
	
	mask32_set(GPIO->PORTD_MODEL, GPIO_MODEL_MODE0, 4);
	GPIO_CLR->PORTD_DOUT = GPIO_P0;

	/* Digital Inputs */
	mask32_set(GPIO->PORTC_MODEL, GPIO_MODEL_MODE6, 1);
	GPIO_SET->PORTC_DOUT = GPIO_P6; /** activate glitch filter */
	
	mask32_set(GPIO->PORTB_MODEL, GPIO_MODEL_MODE3, 1);
	GPIO_SET->PORTB_DOUT = GPIO_P3; /** activate glitch filter */
	
	mask32_set(GPIO->PORTB_MODEL, GPIO_MODEL_MODE0, 2);
	GPIO_CLR->PORTB_DOUT = GPIO_P0; /** activate glitch filter */
	
	/* external pull-ups */
	mask32_set(GPIO->PORTC_MODEL, GPIO_MODEL_MODE7, 4);
	GPIO_CLR->PORTC_DOUT = GPIO_P7;
	
	mask32_set(GPIO->PORTB_MODEL, GPIO_MODEL_MODE4, 4);
	GPIO_CLR->PORTB_DOUT = GPIO_P4;
	
	mask32_set(GPIO->PORTB_MODEL, GPIO_MODEL_MODE1, 4);
	GPIO_CLR->PORTB_DOUT = GPIO_P1;

	/* Digital output, max current 1.5A, open-drain*/
	mask32_set(GPIO->PORTA_MODEL, GPIO_MODEL_MODE5, 4);
	GPIO_CLR->PORTA_DOUT = GPIO_P5;
}

void platform_led_set(platform_led_t led, uint8_t val)
{
	/*
	 * LED TASK
	 * Fill that function with the approriate code
	 * See the platform_0x20.h for more information 
	 */
}

void platform_led_toggle(platform_led_t led)
{
	/*
	 * LED TASK
	 * Fill that function with the approriate code
	 * See the platform_0x20.h for more information 
	 */
}

int platform_input_get(uint8_t ch)
{
	if (ch == 0) {
		return(!(GPIO->PORTC_DIN & GPIO_P6));
	} else if (ch == 1) {
		return(!(GPIO->PORTB_DIN & GPIO_P3));
	} else if (ch == 2) {
		return(!(GPIO->PORTB_DIN & GPIO_P0));
	}
	
	return(-1);
}

void platform_pullup_set(uint8_t pu, uint8_t val)
{
	if (pu == 0) {
		if (val) {
			GPIO_SET->PORTC_DOUT = GPIO_P7;
		} else {
			GPIO_CLR->PORTC_DOUT = GPIO_P7;
		}
	} else if(pu == 1) {
		if (val) {
			GPIO_SET->PORTB_DOUT = GPIO_P4;
		}else {
			GPIO_CLR->PORTB_DOUT = GPIO_P4;
		}
	} else if(pu == 2) {
		if (val) {
			GPIO_SET->PORTB_DOUT = GPIO_P1;
		}else {
			GPIO_CLR->PORTB_DOUT = GPIO_P1;
		}
	} 
}
	
void platform_output_set(uint8_t val)
{
	if (val) {
		GPIO_SET->PORTA_DOUT = GPIO_P5;
	} else {
		GPIO_CLR->PORTA_DOUT = GPIO_P5;
	}
}	

uint64_t platform_get_eui64 (void)
{
	return((uint64_t)DEVINFO->EUI64L | ((uint64_t)DEVINFO->EUI64H << 32));
}
