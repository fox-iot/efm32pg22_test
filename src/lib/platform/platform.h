#ifndef _PLATFORM_H_
#define _PLATFORM_H_

#include <stdint.h>
#include "platform_type.h"

#if TARGET_PLATFORM == 0x20
#include "platform_0x20.h"
#endif
extern uint32_t PLATFORM_HFXO, PLATFORM_SYSCLK, PLATFORM_HCLK, PLATFORM_PCLK, PLATFORM_LSCLK;

void platform_init();

#endif
