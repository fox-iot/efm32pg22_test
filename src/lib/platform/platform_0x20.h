#ifndef _PLATFORM_0X0100_H_
#define _PLATFORM_0X0100_H_

/** 
 * \brief Initialize the target board
 * This function initializes target product-specific peripherals, such as
 * LEDs, inputs, outputs, etc.
 */
void platform_init_target(void);

typedef enum {
	PLATFORM_LED_GREEN = 0,
	PLATFORM_LED_RED
} platform_led_t;

/**
 * \brief Sets the LED state ON or OFF
 * \param led LED color \see platform_led_t
 * \param val Desired LED state true/false
 */ 
void platform_led_set(platform_led_t led, uint8_t val);

/**
 * \brief Toggles the active state of the LED
 * \param led Desired LED \see platform_led_t
 */ 
void platform_led_toggle(platform_led_t led);

/**
 * \brief Get digital value from input 
 * \param ch Input number. The first channel starts 0 
 * \return Returns the current input value. If the value is negative, then error occured
 */ 

int platform_input_get(uint8_t ch);

/**
 * \brief Activates external pull-up resistors with controlled voltage (7,9171V)
 * \param pu Pull-up channel number. The first channel starts at 0.
 * \param val activate or disable the pull-up. 0 - disable; 1 - enable
 */
void platform_pullup_set(uint8_t pu, uint8_t val);

/**
 * \brief Activates open-drain output, max current 1A, limiter ~1.5A
 * \param val activate or disable the output. 0 - disable; 1 - enable
 */
void platform_output_set(uint8_t val);

/**
 * \brief returns 64bit Unique ID
 */ 
uint64_t platform_get_eui64 (void);

#endif
