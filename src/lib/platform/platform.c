#include "platform.h"
#include "cortex.h"
#include "efm32pg22.h"
#include "mask.h"

uint32_t PLATFORM_HFXO, PLATFORM_SYSCLK, PLATFORM_HCLK, PLATFORM_PCLK, PLATFORM_LSCLK;

void platform_init()
{
	// enable HFRCO
	CMU->CLKEN0 |= CMU_CLKEN0_HFRCO0;
	
	// Unlock HFRCO0 registers
	mask32_set(HFRCO0->LOCK, HFRCO_LOCK_LOCKKEY, 33173);

	HFRCO0->CTRL = HFRCO_CTRL_FORCEEN;	

	// Set the target frequency to 38MHz using the 
	// manufacturer's calibration data
	HFRCO0->CAL = DEVINFO->HFRCODPLLCAL[HFRCO_FREQRANGE_38MHz];	
	PLATFORM_SYSCLK = 38000000;
	
	// Wait until oscillator is ready
	while (!(HFRCO0->STATUS & HFRCO_STATUS_ENS));
	while (!(HFRCO0->STATUS & HFRCO_STATUS_RDY));

	// Use HFRCODPLL for SYSCLK
	mask32_set(CMU->SYSCLKCTRL, CMU_SYSCLKCTRL_CLKSEL, 2);

	// HCLK divider /1
	mask32_set(CMU->SYSCLKCTRL, CMU_SYSCLKCTRL_HCLKPRESC, 0);
	PLATFORM_HCLK = PLATFORM_SYSCLK / 1;

	// PCLK divider /1
	mask32_set(CMU->SYSCLKCTRL, CMU_SYSCLKCTRL_PCLKPRESC, 0);
	PLATFORM_PCLK = PLATFORM_HCLK / 1;

	// LSCLK divider /2
	PLATFORM_LSCLK = PLATFORM_PCLK / 2;

	platform_init_target();
}
