#ifndef _TIMER_H_
#define _TIMER_H_

#include <stdint.h>

typedef struct {
	/**
	 * \brief The callback function is executed every 100 microsecond
	 * If not used, set to NULL
	*/
	void (*timer_100us_cb)(void);
}timer_t;

void timer_init(timer_t *conf);
uint32_t timer_get(uint16_t *msec);

#endif

