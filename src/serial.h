#ifndef _SERIAL_H_
#define _SERIAL_H_

#include <stdint.h>


/**
 * \brief Initialize serial
 */
void serial_init();

/**
 * \brief This function shall attempt to read bytes from the serial into the buffer
 * \param buf Buffer provided by the user
 * \param count The desired number of bytes to read bytes from the serial.
 * \return Returns the number of bytes read 
 */ 
int16_t serial_read(void *buf, int16_t count);


/**
 * \brief This function writes up bytes from the buffer into the serial
 * \param buf Buffer with desidred data provided by the user
 * \param count The desired number of bytes to write to the serial.
 * \return Returns the number of bytes written
 */ 
int16_t serial_write(const void *buf, int16_t count);

#endif

