#include <stdio.h>
#include "cortex.h"
#include "efm32pg22.h"
#include "mask.h"
#include "platform.h"
#include "serial.h"
#include "mtimer.h"
#include "timer.h"
#include "debug.h"
#include "serial.h"

static timer_t t_conf = { 
	.timer_100us_cb = NULL
};

static mtimer_t uptime_timer;

int main(void) 
{
	platform_init();
	timer_init(&t_conf);
	mtimer_init(timer_get);
	serial_init();
	interrupts_enable();

	printf("\n\n\rEFM32 Test starting..\r\n");
	while(1) {
		if (mtimer_timeout(&uptime_timer)) {
			mtimer_timeout_set(&uptime_timer, 5000);
			DEBUG_PRINTF("uptime: %u\r\n", (unsigned int)mtimer_get(NULL));
		}
	}

	return(1);
}

ssize_t write_stdout(const void *buf, size_t count)
{
	size_t i;

	for (i = 0; i < count; i++)
		while (serial_write(&(((uint8_t *)buf)[i]), 1) != 1);

	return count;
}

