///
/// @file
/// @brief     debug enable in header
///

#ifndef _DEBUG_CONFIG_H_
#define _DEBUG_CONFIG_H_
#include <stdio.h>


#define ENABLE_DEBUG

#ifdef ENABLE_DEBUG
#define DEBUG_PRINTF(...) \
        printf("[DEBUG] %s: %s:%d >> ", __FILE__ , __func__, __LINE__); \
        printf(__VA_ARGS__)
#else
#define DEBUG_PRINTF(...)
#endif

#endif

