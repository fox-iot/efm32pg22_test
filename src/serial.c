#include "serial.h"
#include "cortex.h"
#include "efm32pg22.h"
#include "mask.h"
#include "platform.h"

// TX: PA05
// RX: PA06

#define SERIAL_SEND_FIFO_LEN 256
#define SERIAL_RECV_FIFO_LEN 256

static volatile uint16_t serial_send_fifo_in, serial_send_fifo_out, serial_send_fifo_len;
static volatile uint16_t serial_recv_fifo_in, serial_recv_fifo_out, serial_recv_fifo_len;

static volatile uint8_t serial_send_start;
static volatile uint8_t serial_send_fifo[SERIAL_SEND_FIFO_LEN], serial_recv_fifo[SERIAL_RECV_FIFO_LEN];

void serial_init()
{
	cortex_interrupt_disable(USART1_RX_INT);
	cortex_interrupt_disable(USART1_TX_INT);

	serial_send_start = 0;
	serial_send_fifo_in = serial_send_fifo_out = serial_send_fifo_len = 0;
	serial_recv_fifo_in = serial_recv_fifo_out = serial_recv_fifo_len = 0;

	CMU->CLKEN0 |= CMU_CLKEN0_USART1;
	USART1->EN &= ~USART_EN_EN;

	USART1->CTRL = 0;
	USART1->FRAME = mask32(USART_FRAME_DATABITS, 5) | mask32(USART_FRAME_PARITY, 0) | mask32(USART_FRAME_STOPBITS, 1);
	USART1->CLKDIV = 256LLU * ((PLATFORM_PCLK / (16LLU * 115200LLU)) - 1LLU);

	CMU->CLKEN0 |= CMU_CLKEN0_GPIO;
	//mask32_set(GPIO->USART1_RXROUTE, GPIO_USART_RXROUTE_PORT, 0);
	//mask32_set(GPIO->USART1_RXROUTE, GPIO_USART_RXROUTE_PIN, 6);
	mask32_set(GPIO->USART1_TXROUTE, GPIO_USART_TXROUTE_PORT, 0);
	mask32_set(GPIO->USART1_TXROUTE, GPIO_USART_TXROUTE_PIN, 0);
	mask32_set(GPIO->PORTA_MODEL, GPIO_MODEL_MODE0, 5);
	//mask32_set(GPIO->PORTA_MODEL, GPIO_MODEL_MODE6, 1);
	//GPIO->USART1_ROUTEEN = GPIO_USART_ROUTEEN_RXPEN | GPIO_USART_ROUTEEN_TXPEN;
	GPIO->USART1_ROUTEEN = GPIO_USART_ROUTEEN_TXPEN;

	USART1->EN |= USART_EN_EN;

	USART1->CMD = USART_CMD_CLEARRX;
	USART1->IEN = USART_IEN_RXDATAV;
	//USART1->CMD = USART_CMD_RXEN;

	USART1->CMD = USART_CMD_CLEARTX;
	USART1->CMD = USART_CMD_TXEN;

	cortex_interrupt_set_priority(USART1_RX_INT, 0);
	cortex_interrupt_clear(USART1_RX_INT);
	cortex_interrupt_enable(USART1_RX_INT);

	cortex_interrupt_set_priority(USART1_TX_INT, 0);
	cortex_interrupt_clear(USART1_TX_INT);
	cortex_interrupt_enable(USART1_TX_INT);
}

int16_t serial_read(void *buf, int16_t count)
{
	uint16_t len;

	if (!count || !serial_recv_fifo_len)
		return 0;

	USART1->IEN &= ~USART_IEN_RXDATAV;
	for (len = 0; serial_recv_fifo_len && (len < count) && (len < 32767); len++) {
		((uint8_t *)buf)[len] = serial_recv_fifo[serial_recv_fifo_out];
		serial_recv_fifo_len--;
		serial_recv_fifo_out++;
		if (serial_recv_fifo_out == SERIAL_RECV_FIFO_LEN)
			serial_recv_fifo_out = 0;
	}
	USART1->IEN |= USART_IEN_RXDATAV;
	return len;
}

int16_t serial_write(const void *buf, int16_t count)
{
	uint16_t len;

	if (!count)
		return 0;

	USART1->IEN &= ~USART_IEN_TXC;
	for (len = 0; (serial_send_fifo_len < SERIAL_SEND_FIFO_LEN) && (len < count) && (len < 32767); len++) {
		serial_send_fifo[serial_send_fifo_in] = ((uint8_t *)buf)[len];
		serial_send_fifo_len++;
		serial_send_fifo_in++;
		if (serial_send_fifo_in == SERIAL_SEND_FIFO_LEN)
			serial_send_fifo_in = 0;
	}
	if (serial_send_fifo_len) {
		if (!serial_send_start) {
			USART1->TXDATAX = serial_send_fifo[serial_send_fifo_out];
			serial_send_fifo_len--;
			serial_send_fifo_out++;
			if (serial_send_fifo_out == SERIAL_SEND_FIFO_LEN)
				serial_send_fifo_out = 0;

			serial_send_start = 1;
		}
		USART1->IEN |= USART_IEN_TXC;
	}
	return len;
}

CORTEX_ISR(USART1_RX_INT)
{
	volatile uint32_t c;

	if (USART1->STATUS & USART_STATUS_RXDATAV) {
		c = USART1->RXDATAX;
		if (serial_recv_fifo_len < SERIAL_RECV_FIFO_LEN) {
			serial_recv_fifo[serial_recv_fifo_in] = (c & 0xFF);
			serial_recv_fifo_len++;
			serial_recv_fifo_in++;
			if (serial_recv_fifo_in == SERIAL_RECV_FIFO_LEN)
				serial_recv_fifo_in = 0;
		}
	}
	cortex_interrupt_clear(USART1_RX_INT);
}

CORTEX_ISR(USART1_TX_INT)
{
	if (USART1->STATUS & USART_STATUS_TXC) {
		USART1_CLR->IF = USART_IEN_TXC;
		if (serial_send_fifo_len) {
			USART1->TXDATAX = serial_send_fifo[serial_send_fifo_out];
			serial_send_fifo_len--;
			serial_send_fifo_out++;
			if (serial_send_fifo_out == SERIAL_SEND_FIFO_LEN)
				serial_send_fifo_out = 0;
		} else {
			serial_send_start = 0;
			USART1->IEN &= ~USART_IEN_TXC;
		}
	}
	cortex_interrupt_clear(USART1_TX_INT);
}

