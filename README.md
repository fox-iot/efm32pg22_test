## **WE'RE OUTSOURCING **

Complete tasks successfully, and let's start work together!   
For more details about the tasks, please look at our [Wiki page](https://bitbucket.org/fox-iot/efm32pg22_test/wiki/Home).

## Your or Your Team Profile

- Experience in development under Linux/Unix systems
- Good at embedded development in C
- Good at Shell scripts
- Excellent at microcontrollers (especially ARM Cortex m0, m0+, m3, m4, m33 processors, for example, Silabs, STM32, Nordic nRF chipsets)
- Able to read electronic schematics
- Knowledge of the Rust language is beneficial 
